package com.apcfss.exception;

public class DAOException extends Exception
{


/**
*
*/
private static final long serialVersionUID = 1L;

public DAOException()
{
super();
}

public DAOException(String aMessage)
{
super(aMessage);
}

public DAOException(String aMessage, Throwable aException)
{
super(aMessage, aException);
}

public DAOException(Throwable aException)
{
super(aException);
}
}
