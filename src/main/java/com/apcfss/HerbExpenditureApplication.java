package com.apcfss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "com.apcfss")
public class HerbExpenditureApplication  extends SpringBootServletInitializer  {

	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(HerbExpenditureApplication.class);
    }
	
	public static void main(String[] args) {
		SpringApplication.run(HerbExpenditureApplication.class, args);
	}

}
