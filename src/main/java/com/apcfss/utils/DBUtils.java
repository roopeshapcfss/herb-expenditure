package com.apcfss.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DBUtils {
	
	@Autowired
	static DataSource dataSource;
	
	
	/*@Autowired
	private SessionFactory factory;*/
	
	//Session session = null;
	String sql = null;
	//SQLQuery sqlQuery = null;
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	} 
	
	
	public static Connection getConnection()
    {
		Connection con = null;
		try
        {
             do
             {
            	 con= dataSource.getConnection();
            	 System.out.println("At Utils---"+con);
             }
             while(con==null || con.isClosed());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        } 
        return con;
    }
	
	public static String getFinYear() {
		
		Calendar cal=Calendar.getInstance();
		int year=cal.get(Calendar.YEAR);
		int month =cal.get(Calendar.MONTH);
		if(month<=3) {
			year=year-1;
		}
		String strYear=String.valueOf(year);
		return strYear;
	}

	public static String getValue(Statement st , String sql)
	{
		String result ="";
		
	    ResultSet rs = null;
		try 
		{
			rs = st.executeQuery(sql);
			if (rs!=null && rs.next())
			{	
				if (rs.getString(1)!=null)
					result = rs.getString(1);	
			}
			
			if(rs.next())
				result = rs.getString(1);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static boolean recordExists(Connection con, String query) {
		boolean flag = false;
		ResultSet rs = null;
		Statement st = null;
		try {
			st = con.createStatement();
			//System.out.println("650==="+query);
			rs = st.executeQuery(query);
			if (rs != null && rs.next()) {
				if (rs.getString(1).equals("0"))
					flag = false;
				else
					flag = true;
			} else
				flag = false;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeStatement(st);
			closeResultSet(rs);

		}
		return flag;
	}
	
	
	public static void closeStatement(Statement _stmt) 
	{
		try
		{
			if(_stmt!=null) {
				_stmt.close();
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void closePreparedStatement(PreparedStatement _stmt) 
	{
		try
		{
			if(_stmt!=null) {
				_stmt.close();
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void closeResultSet(ResultSet _rs) 
	{
		try
		{
			if(_rs!=null) {
				_rs.close();
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void closeConnection(Connection con)
    {
		try 
		{
			if (con != null) 
			{
				if (!con.isClosed()) 
				{
					con.close();
					con = null;
				} 
				else
					con = null;
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static ArrayList<HashMap<String, Object>> getListMapData(ResultSet rs)
	{
		ArrayList<HashMap<String, Object>> reportData = new ArrayList<HashMap<String, Object>>();
		ResultSetMetaData rm = null;
		HashMap<String, Object> rowData = null;
		int noOfCols = 0;
		try{
			rm = rs.getMetaData();
			noOfCols = rm.getColumnCount();
			if(rs!=null && rs.next())
			{
				do {
					rowData = new HashMap<String, Object>(noOfCols);
					for(int i=1;i<=noOfCols;i++)
					{
						rowData.put(rm.getColumnName(i).toLowerCase(), ((rs.getObject(i)!=null && !rs.getObject(i).equals(""))?rs.getObject(i):""));
					} 
					reportData.add(rowData);
				} while(rs.next());
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null) {
					rs.close();
				}
			} catch (SQLException se) {
				// TODO: handle exception
				se.printStackTrace();
			}
		}
		
		return reportData;
	}
	
	public static ArrayList<HashMap<String,Object>> getListMap(String sql,Connection con,Object[] columnvalues) throws SQLException
	{       
		ArrayList<HashMap<String,Object>> records=new ArrayList<HashMap<String,Object>>();
		PreparedStatement ps = con.prepareStatement(sql);
		if(columnvalues!=null)
		{
			for(int ci=0;ci<columnvalues.length;ci++)
			{
				ps.setObject(ci+1, columnvalues[ci]);
			}
		}
		ResultSet rs=ps.executeQuery();
		ResultSetMetaData rm = rs.getMetaData();
		int cols = rm.getColumnCount();
		if (rs.next()) 
		{
			 do {
				 HashMap<String, Object> row = new java.util.HashMap<String, Object>(cols);
				 for (int i=1; i<=cols; i++) {
					 String columnName = rm.getColumnName(i);
					 Object columnValue = rs.getObject(i);
					 row.put(columnName.trim(), columnValue);
				 } 
				 records.add(row);
				 row = null;
			 } 
			 while (rs.next());
		 }
		closeResultSet(rs);
		closeStatement(ps);
		return records;
	}
	
	public static Map<String, Object> getMapData(String sql, Connection con)
 	{
 		Map<String, Object> reportData = new HashMap<String, Object>();
 		Statement stmt = null;
 		ResultSet rs = null;
 		try {
 			stmt = con.createStatement();
 			rs = stmt.executeQuery(sql);
 			reportData = getMapData(rs);
 		} catch (Exception e) {
 			e.printStackTrace();// TODO: handle exception
 		} finally {
 			try {
 				if (rs != null) {
 					rs.close();
 				}
 				if (stmt != null) {
 					stmt.close();
 				}
 			} catch (SQLException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
 		}
 		return reportData;
 	}
	
	public static Map<String, Object> getMapData(ResultSet rs)
  	{
  		ResultSetMetaData rm = null;
  		Map<String, Object> reportData = new HashMap<String, Object>();
  		int noOfCols = 0;
  		try{
  			rm = rs.getMetaData();
  			noOfCols = rm.getColumnCount();
  			if(rs.next())
  			{
  				reportData = new HashMap<String, Object>(noOfCols);
  				for(int i=1;i<=noOfCols;i++)
  				{
  					reportData.put(rm.getColumnName(i), ((rs.getObject(i)!=null && !rs.getObject(i).equals(""))?rs.getObject(i):""));
  				} 
  			}
  		} catch (Exception e) {
  			e.printStackTrace();
  		} finally {
  			try {
  				if(rs!=null) {
  					rs.close();
  				}
  			} catch (SQLException se) {
  				se.printStackTrace();
  			}
  		}

  		return reportData;
  	}
	
		public static int columnCount(ResultSet rs) {

		int columns=0;
		try {
			ResultSetMetaData rm;

			if (rs!=null) {
				rm = rs.getMetaData();
				columns=rm.getColumnCount();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return columns;
	}
	
	public static ArrayList<ArrayList<String>> doubleArrayList(String sql,Connection con) {
	    ArrayList<ArrayList<String>> matrix = new ArrayList<ArrayList<String>>();
	        ResultSet rs=null;
	        Statement st=null;
	    try {
	        st=con.createStatement();
	        rs=st.executeQuery(sql);
	        int columns=columnCount(rs);
	        ArrayList<String> subMatrix = null;

	        while(rs.next()) {
	            subMatrix = new ArrayList<String>();

	            for (int i=1;i<=columns;i++) {
	                subMatrix.add(rs.getString(i));
	            }

	            matrix.add(subMatrix);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return matrix;
	}

	public static StringBuffer lvb3(String sql,Connection con){
		StringBuffer val = new StringBuffer();
		ArrayList li=doubleArrayList(sql, con);
		for(int i=0 ; i <li.size() ;i++){
			val.append("<option value='"+((ArrayList)li.get(i)).get(0)+"'>"+((ArrayList)li.get(i)).get(1)+"</option>");
		}
		return val;
	}
	 public static List executeQuery(Connection conn, String query, Object[] objArray)
			    throws SQLException
			  {
			    PreparedStatement ps = conn.prepareStatement(query);
			    //System.out.println(" qry :: "+query);
			    if(null!=objArray){
				    for (int i = 0; i < objArray.length; i++) {
				      ps.setObject(i + 1, objArray[i]);
				      System.out.println("\t o :: "+objArray[i]);
				    }
			    }
			    ResultSet rs = ps.executeQuery();
			    List result = processResultSet(rs);
			    close(null, ps, rs);
			    return result;
			  }
			  @SuppressWarnings({ "rawtypes", "unchecked" })
				private static List processResultSet(ResultSet rs) throws SQLException
				{
					List result = null;
					Map row = null;
					Object columnValue = null;
					String columnName = null;
					int cols=0;
					ResultSetMetaData rm=(ResultSetMetaData) rs.getMetaData();
					cols = rm.getColumnCount();
					if(rs!=null)
					{
						result = new ArrayList();
						while(rs.next())
						{
							row = new HashMap(cols);
							for(int i=1;i<=cols;i++)
							{
								columnName = rm.getColumnName(i);
								columnValue = rs.getObject(i);
								if(columnValue==null || columnValue.equals(""))
									columnValue=" ";
								if(columnName==null || columnName.equals(""))
									columnName=" ";
								row.put(columnName,columnValue);

							} 
							result.add(row);
						} 
					}
					//System.out.println("result "+result);
					return result;
				}
			  public static void close(Connection conn, Statement stm, ResultSet rs) {
					if (rs != null) {
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
						rs = null;
					}
					if (stm != null) {
						try {
							stm.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
						stm = null;
					}
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
						conn = null;
					}
				}
			  @SuppressWarnings("rawtypes")
				public static List executeQuery(String sql,Connection con) throws SQLException
				{
					List result=null;
					PreparedStatement pstmt = null;
					ResultSet rs=null;
					try {
						////System.out.println("sql::"+sql);
						pstmt = con.prepareStatement(sql);
						rs = pstmt.executeQuery();
						result = processResultSet(rs);
					}catch (Exception e) {
						e.printStackTrace();// TODO: handle exception
					}finally
					{
						try
						{
							pstmt.close();
							rs.close();
							con.close();
						}catch(Exception e)
						{
							e.printStackTrace();
						}
					}
					return result;
				}
			  public static String getStringfromQuery(String sql)
			  {
			    String first = "";
			    Connection con = null;
			    Statement st = null;
			    ResultSet rs = null;
			    try
			    {
			      con = getConnection();
			      st = con.createStatement();
			      rs = st.executeQuery(sql);
			      if (rs.next())
			      {
			        first = rs.getString(1);
			      }
			    } catch (SQLException se) {
			      first = "0";
			      se.printStackTrace();
			    } finally {
			      close(con, st, rs);
			    }
			    return first;
			  }

			  public static String getStringfromQuery(Connection con, String sql) throws SQLException
			  {
			    String str = "";
			    
			    Statement st = con.createStatement();
			    ResultSet rs = st.executeQuery(sql);
			    if (rs.next()) {
			      str = rs.getString(1);
			    }
			    close(null, st, rs);
			    return str;
			  }

			  public static String getStringfromQuery(Connection conn, String query, Object[] objArray) throws SQLException
			  {
			    String str = "";
			    PreparedStatement ps = conn.prepareStatement(query);
			    System.out.println("query :: "+query);
			    for (int i = 0; i < objArray.length; i++) {
			      ps.setObject(i + 1, objArray[i]);
			      System.out.println("objArray[i]:: "+objArray[i]);
			    }

			    ResultSet rs = ps.executeQuery();
			    if (rs.next()) {
			      str = rs.getString(1);
			    }
			    System.out.println("str :: "+str);
			    return str;
			  }
			  
			  public static ArrayList<ArrayList<String>> doubleArrayList(ResultSet rs) {
					ArrayList<ArrayList<String>> matrix = new ArrayList<ArrayList<String>>();

					try {
						int columns=columnCount(rs);
						ArrayList<String> subMatrix = null;

						while(rs.next()) {
							subMatrix = new ArrayList<String>();

							for (int i=1;i<=columns;i++) {
								subMatrix.add(rs.getString(i));
							}

							matrix.add(subMatrix);
						}
					} catch (Exception e) {
						System.out.println("Error while copying the double dimensional ArrayList");
						e.printStackTrace();
					}
					return matrix;
				}
			  @SuppressWarnings("rawtypes")
				public static List execut234eQuery(String sql,Connection con) throws SQLException
				{
					List result=null;
					PreparedStatement pstmt = null;
					ResultSet rs=null;
					try {
						////System.out.println("sql::"+sql);
						pstmt = con.prepareStatement(sql);
						rs = pstmt.executeQuery();
						result = processResultSet(rs);
					}catch (Exception e) {
						e.printStackTrace();// TODO: handle exception
					}finally
					{
						try
						{
							pstmt.close();
							rs.close();
							con.close();
						}catch(Exception e)
						{
							e.printStackTrace();
						}
					}
					return result;
				}
			
			

			  public StringBuffer getLabelValueBean(Connection con,String sql) {
				  StringBuffer selectDropDownList = new StringBuffer();
					PreparedStatement pstmt =null;
					ResultSet rs = null;
					try {
						pstmt = con.prepareStatement(sql);
						rs = pstmt.executeQuery();
						/*
						 * if (combo) selectDropDownList.append("No Options Found");
						 */
						while(rs.next()) {
							selectDropDownList.append("<option value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>");
						}
						System.out.println("selectDropDownList--"+selectDropDownList);
					} catch (Exception e) {
						e.printStackTrace();
					}
					finally
					{
						DBUtils.closeResultSet(rs);
						DBUtils.closePreparedStatement(pstmt);
						DBUtils.closeConnection(con);
					}
					return selectDropDownList;
				}


}