package com.apcfss.utils;
import javax.sql.DataSource;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
@Aspect
public class ConnectionsCount  
{

	JdbcTemplate jdbcTemplate;	
	 
	@Autowired
	public ConnectionsCount(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@After("execution(* com.apcfss.DAO.MasterDAOImpl.*(..))")
	 public void logBeforeV1(JoinPoint joinPoint)
	    {
		System.out.println("inside method "+joinPoint.getSignature());
		//com.zaxxer.hikari.HikariDataSource hikariDataSource =(com.zaxxer.hikari.HikariDataSource)jdbcTemplate.getDataSource();
		//  System.out.println("-------active---"+hikariDataSource.getHikariPoolMXBean().getActiveConnections()+" ---idle ---"+hikariDataSource.getHikariPoolMXBean().getIdleConnections()+"--total--"+hikariDataSource.getHikariPoolMXBean().getTotalConnections() ); 
		 
		
	    }

}
