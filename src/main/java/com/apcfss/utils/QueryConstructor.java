package com.apcfss.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;



public class QueryConstructor 
{

	public static String insertQuery( String tableName,  Map<String , Object>  fieldAndValue)
	{

		if(tableName==null || fieldAndValue==null || fieldAndValue.size()==0 ) 
		{
			return null;	

		}

		StringBuffer queryBuffer=new StringBuffer(" insert into  "+tableName +"("  );


		Set<String> keys =fieldAndValue.keySet();

		Collection<Object> values =fieldAndValue.values();

		Iterator<String> it = keys.iterator();

		while (it.hasNext()) 
		{
			queryBuffer.append(it.next()+"," );

		} 

		queryBuffer.replace(queryBuffer.length()-1, queryBuffer.length(), ")");
		queryBuffer.append("values(");

		Iterator<Object> valueIterator=values.iterator();

		while (valueIterator.hasNext()) 
		{
			queryBuffer.append("'"+valueIterator.next()+"'"+"," );

		} 

		queryBuffer.replace(queryBuffer.length()-1, queryBuffer.length(), ")");

		return queryBuffer.toString();
	}



	public static String  selectSingeTable( String tableName,  List <String>  columnsList, HashMap<String,String> conditionsMap,List<String> orderByFieldsList ,boolean aliasAsFields)
	{

		if(tableName==null || columnsList==null || columnsList.size()==0 ) 
		{
			return null;	

		}


		StringBuffer queryBuffer=new StringBuffer(" select " );


		StringBuffer aliasName=new StringBuffer("");	
		for(int i=0; i<columnsList.size();i++)
		{
			if(aliasAsFields)
			{
				aliasName.append(" as field"+(i+1) );	
			}
			queryBuffer.append(columnsList.get(i)+aliasName.toString()  );	
			
			if((i+1)!= columnsList.size())
			{
				queryBuffer.append("," );	
			}
			
			if(aliasAsFields)
			{
				aliasName.setLength(0);	
			}
			

		}


		queryBuffer.append(" from " +tableName);

		if(conditionsMap!=null && conditionsMap.size()>0 )
		{

			queryBuffer.append(" where ");

			for (Map.Entry<String, String> entry : conditionsMap.entrySet()) 
			{


				queryBuffer.append("  "+entry.getKey()+"= '" +entry.getValue()+"' and" );

			}


			queryBuffer.replace(queryBuffer.length()-3, queryBuffer.length(), "  ");
		}


		if(orderByFieldsList!=null && orderByFieldsList.size()>0 )
		{

			queryBuffer.append(" order by ");

			for(int i=0; i<orderByFieldsList.size();i++)
			{
				if((i+1)!= orderByFieldsList.size())
				{
					queryBuffer.append(orderByFieldsList.get(i) +"," );	
				}

				else
				{
					queryBuffer.append(orderByFieldsList.get(i) );	

				}

			}

		}

		return queryBuffer.toString();
	}




	public static String deleteQuery( String tableName,  HashMap<String , String>  conditionsMap)
	{

		if(tableName==null  ) 
		{
			return null;	

		}

		StringBuffer queryBuffer=new StringBuffer(" delete from  "+tableName +""  );

		
		if(conditionsMap!=null && conditionsMap.size()>0 )
		{

			queryBuffer.append(" where ");

			for (Map.Entry<String, String> entry : conditionsMap.entrySet()) 
			{


				queryBuffer.append("  "+entry.getKey()+"= '" +entry.getValue()+"' and" );

			}


			queryBuffer.replace(queryBuffer.length()-3, queryBuffer.length(), "  ");
		}
		
		

		return queryBuffer.toString();
	}



	public static String updateQuery( String tableName,  HashMap<String , String>  updateValuesMap, HashMap<String , String>  conditionsMap)
	{

		if(tableName==null || updateValuesMap==null || updateValuesMap.size()==0 ) 
		{
			return null;	

		}

		StringBuffer queryBuffer=new StringBuffer(" update  "+tableName +""  );

		

			queryBuffer.append(" set ");

			for (Map.Entry<String, String> entry : updateValuesMap.entrySet()) 
			{

				queryBuffer.append(entry.getKey()+"= '" +entry.getValue()+"' ," );

			}

			queryBuffer.replace(queryBuffer.length()-1, queryBuffer.length(), "  ");
		
			if(conditionsMap!=null && conditionsMap.size()>0 )
			{

				queryBuffer.append(" where ");

				for (Map.Entry<String, String> entry : conditionsMap.entrySet()) 
				{


					queryBuffer.append("  "+entry.getKey()+"= '" +entry.getValue()+"' and" );

				}


				queryBuffer.replace(queryBuffer.length()-3, queryBuffer.length(), "  ");
			}

		return queryBuffer.toString();
	}




}
