package com.apcfss.utils;

public class DatabaseTables {

	// Masters
	final public static String mdistrict = "mdistrict"; // Districts
	final public static String dept_mst = "dept_mst"; // Departments
	final public static String service_list = "service_list"; // Services

	final public static String zgoap_hoa = "zgoap_hoa"; // HOA Master
	final public static String hcm_memp = "hcm_memp";
	final public static String audit_work="expenditure_workflow_bill_submission_works_master";
	
//	ExpenditureBillSubmissionWorkflow
	final public static String expenditure_workflow_bill_submission = "expenditure_workflow_bill_submission";
	
}
