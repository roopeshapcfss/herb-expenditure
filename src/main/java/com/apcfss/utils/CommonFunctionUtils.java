// 
// Decompiled by Procyon v0.5.36
// 

package com.apcfss.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


@Component("commonutils")
public class CommonFunctionUtils {

	public String formatfractionNumber(final String number) {
		return number.replaceAll("\\.0*$", "");
	}

	public String validateFileName(final String fileName) {
		return fileName.replaceAll("\\s", "").replace("'", "").replace("\"", "").replace("°", "");
	}

	public String formatDateStringToDatabaseDate(final String date) {
		String returnDate = null;
		try {
			final SimpleDateFormat sdDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			final SimpleDateFormat dataBaseFormat = new SimpleDateFormat("yyyy-MM-dd");
			returnDate = dataBaseFormat.format(sdDateFormat.parse(date));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnDate;
	}

	public Date formatDate(final String date, final String format) throws Exception {
		return new SimpleDateFormat(format).parse(date);
	}

	public synchronized boolean uploadFile(final MultipartFile fileObj, final String dirPath, final String fileName,
			String reqFileName, final HttpServletRequest request) {
		try {
			final InputStream is = fileObj.getInputStream();
			String filePath = null;
			final String filename = null;
			if (isNull(fileName)) {
				filePath = String.valueOf(String.valueOf(dirPath)) + fileObj.getName();
				System.out.println(filePath);
			} else {
				if ("application/msword".equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else if ("application/vnd.ms-excel".equalsIgnoreCase(fileObj.getContentType())
						|| "application/msexcel".equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else if ("application/vnd.openxmlformats-officedocument.wordprocessingml.document"
						.equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else if ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
						.equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else if ("image/jpeg".equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else if ("image/png".equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else if ("application/pdf".equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else if ("text/xml".equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else if ("text/plain".equalsIgnoreCase(fileObj.getContentType())) {
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				} else {
					if (!"application/octet-stream".equalsIgnoreCase(fileObj.getContentType())) {
						return false;
					}
					reqFileName = reqFileName;
					filePath = String.valueOf(String.valueOf(dirPath)) + reqFileName;
				}
				if (!isNull(reqFileName)) {
					request.setAttribute(reqFileName, (Object) filename);
				}
			}
			System.out.println("filePathfilePathfilePath" + filePath);
			final OutputStream bos = new FileOutputStream(filePath);
			int bytesRead = 0;
			final byte[] buffer = new byte[18192];
			while ((bytesRead = is.read(buffer, 0, 18192)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean isNull(final Object value) {
		return value == null || "".equals(value.toString().trim());
	}

	public static boolean validateString(final String value) {
		boolean flag = false;
		if (value != null && !value.trim().equalsIgnoreCase("null") && !value.trim().equals("")) {
			flag = true;
		}
		return flag;
	}

	public static String generateOTP() {
		final Random random = new Random();
		final int numbers = 100000 + (int) (random.nextFloat() * 899900.0f);
		return new StringBuilder(String.valueOf(numbers)).toString();
	}

	public static String sendSMS(final String isSingleBulk, final String mobileNumbers, final String message) {
		String statusCode = "";
		String getResponseCode = "";
		String getResponseMessage = "";
		String returnFlag = "";
		HttpURLConnection connection = null;
		try {
			final String query = "username=" + URLEncoder.encode("aplrs") + "&password="
					+ URLEncoder.encode("aplrs@172") + "&from=APDTCP&to=" + mobileNumbers + "&type=1" + "&msg="
					+ message;
			final URL url = new URL("https://www.smsstriker.com/API/sms.php");
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			HttpURLConnection.setFollowRedirects(true);
			connection.setRequestProperty("Content-length", String.valueOf(query.length()));
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)");
			final DataOutputStream output = new DataOutputStream(connection.getOutputStream());
			final int queryLength = query.length();
			output.writeBytes(query);
			final DataInputStream input = new DataInputStream(connection.getInputStream());
			for (int c = input.read(); c != -1; c = input.read()) {
				statusCode = String.valueOf(statusCode) + (char) c;
			}
			getResponseCode = new StringBuilder(String.valueOf(connection.getResponseCode())).toString();
			getResponseMessage = connection.getResponseMessage();
			returnFlag = String.valueOf(statusCode.trim()) + "-" + getResponseCode + "-" + getResponseMessage;
			input.close();
		} catch (Exception e) {
			System.out.println("Something bad just happened.");
			System.out.println(e);
			e.printStackTrace();
			returnFlag = null;
		}
		return returnFlag;
	}

	public String sendSMSUsingCommonServies(final String isSingleBulk, final String mobileNumbers, final String message,
			final String userId, final String ipAddress) {
		String returnFlag = "";
		try {
			/*
			 * final URL url = new
			 * URL("https://cdacsms.apcfss.in/services/APCfssSmsGateWayReq/sendTextSms");
			 * final String encoding = new
			 * BASE64Encoder().encode("PRPNUSER:PRPNU$#R".getBytes()); final
			 * HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
			 * connection.setRequestMethod("POST"); connection.setDoOutput(true);
			 * connection.setRequestProperty("Authorization", "Basic " + encoding);
			 * connection.setRequestMethod("POST");
			 * connection.setRequestProperty("Content-Type", "application/json"); final
			 * String smsId = this.applicantDAO.getSMSCount(); final String input =
			 * "{\"REQUEST\" : {\"SMSID\": \"" + smsId + "\" , \"MOBNO\":\"" + mobileNumbers
			 * + "\" , \"SMSTEXT\" : \"" + message + "\", \"PROJCODE\" : \"APLRS-DTCP\"}} ";
			 * final OutputStream os = connection.getOutputStream();
			 * os.write(input.getBytes()); os.flush(); final InputStream content =
			 * connection.getInputStream(); final BufferedReader in = new BufferedReader(new
			 * InputStreamReader(content)); String smsResponse = ""; String line; while
			 * ((line = in.readLine()) != null) { smsResponse = line;
			 * System.out.println(smsResponse); }
			 */
			final String path = "message=" + message + "&contextName=LRS&singleBulk=" + isSingleBulk + "&mobileNumber="
					+ mobileNumbers + "";
			final URL url = new URL("http://test.apcfss.in/SMSIntegration/services/sms/singleSms?");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			final StringBuffer queryParam = new StringBuffer();
			queryParam.append(path);
			final OutputStream os = connection.getOutputStream();
			os.write(queryParam.toString().getBytes());
			os.flush();
			final InputStream content = (InputStream) connection.getInputStream();
			final BufferedReader in = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = in.readLine()) != null) {
				returnFlag = line;
			}
			log("returnFlag--->" + returnFlag);
			// this.applicantDAO.sentMsgStatusCreate(mobileNumbers, message, returnFlag,
			// "Scrunity", userId, ipAddress);
		} catch (Exception e) {
			System.out.println("Something bad just happened.");
			System.out.println(e);
			e.printStackTrace();
			returnFlag = null;
		}
		return returnFlag;
	}

	public boolean isNumeric(final String str) {
		if (str == null || str.length() == 0) {
			return false;
		}
		char[] charArray;
		for (int length = (charArray = str.toCharArray()).length, i = 0; i < length; ++i) {
			final char c = charArray[i];
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	public static void log(final String log) {
		System.out.println(log);
	}

	public static void main(final String[] args) {
		try {
			final URL url = new URL("https://cdacsms.apcfss.in/services/APCfssSmsGateWayReq/sendTextSms");
			
			//final String encoding = new BASE64Encoder().encode("PRPNUSER:PRPNU$#R".getBytes());
			
			final String encoding =Base64.getEncoder().encodeToString("PRPNUSER:PRPNU$#R".getBytes());
			final HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			final String input = "{\"REQUEST\" : {\"SMSID\":\"L\", \"MOBNO\":\"9490245689\" , \"SMSTEXT\" : \"SAMPLE SMS TEXT\", \"PROJCODE\" : \"APLRS-DTCP\"}} ";
			final OutputStream os = connection.getOutputStream();
			os.write(input.getBytes());
			os.flush();
			final InputStream content = connection.getInputStream();
			final BufferedReader in = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = in.readLine()) != null) {
				System.out.println(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Long getUserNameRequest(HttpServletRequest request) {

		Long userId = Long.parseLong("14246182");

		return userId;
	}

	public String getFinYear() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		if (month <= 3) {
			year = year - 1;
		}
		String strYear = String.valueOf(year);
		return strYear;
	}

	public static String getCurrentDate() {
		Date date = new Date();
		String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
		return currentDate;
	}
}
