package com.apcfss.utils;

public interface ProcessCodes {
	
	public static int hod = 3000;
	public static int financeDeptFmu = 3300;
	public static int creationOfHoaFmu = 3310;
	public static int hoaCreationOfProcessCode2 = 3320;
	public static int hoaCreationOfPdProcess = 3340;
	public static int financeDeptFmuDebtManagement = 3600;
	public static int contingencyFundFmu = 3700;
	public static int employeeAdvanceFmu = 3800;
	public static int budgetDistributionSco = 7150;
	public static int financeDeptBs = 8100;
	public static int ddo = 8812;
	public static int eapFMUInitiation = 8900;
	public static int nabardFMUInitiation = 9000;
	public static int apgliHOD = 9500;
	public static int apgliFMU = 9600;
	public static int nationalHighways = 9700;
	public static int goAmendment = 9800;
}
