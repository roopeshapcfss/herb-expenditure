package com.apcfss.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.apcfss.model.ExpenditureBillPSAuditAndWorks;
import com.apcfss.model.ExpenditureBillSubmissionWorkflow;
import com.apcfss.model.LabelValueBean;
import com.apcfss.service.BillSubmissionPSAuditAndWorksService;



@Controller
@RequestMapping("/BillSubmissionPSAuditAndWorks")
public class BillSubmissionPSAuditAndWorksController {
	@Autowired
	BillSubmissionPSAuditAndWorksService billAuditService;
	@RequestMapping(value="/ddolist")
	public ModelAndView billSubmissionAuditAndWorks(HttpServletRequest request, HttpServletResponse response)
	{
		String cfmsId="14419052";
		
		ModelAndView modelAndView = new ModelAndView("billSubmissionPSAuditAndWorksMaster");
		List<LabelValueBean> ddocodelist;
		List<Map<String, Object>> positionlist;
		try {
	    modelAndView.addObject("billSubmissionPsAuditAndWork",new ExpenditureBillPSAuditAndWorks());
		ddocodelist = billAuditService.getDDOCodesByCfmsId(Long.parseLong(cfmsId));
		positionlist=billAuditService.getPositionListByCfmsId(cfmsId);
		System.out.println("DDOList-----------"+ddocodelist);
		System.out.println("PositionList---------"+positionlist);
		//modelAndView.addObject("list", list);
		modelAndView.addObject("ddocodelist", ddocodelist);
		modelAndView.addObject("positionlist",positionlist);
		}
		catch (Exception e) {
			e.printStackTrace();
			}
		return modelAndView;	
	}
	@RequestMapping(value="/save")
	public ModelAndView saveBillSubmissionAudit(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("billSubmissionPsAuditAndWork")ExpenditureBillPSAuditAndWorks billPsAuditBean) throws Exception {
		ModelAndView mav=null;
		mav=new ModelAndView("billSubmissionPSAuditAndWorksMaster");
		mav.addObject("billSubmissionPsAuditAndWork", billPsAuditBean);
		//String username="14246182";
		boolean t=billAuditService.saveBillSubmissionAudit(billPsAuditBean,request);
		if(t==true) {
			System.out.println("Insert Status-------Record Added");
			
			}
			
		
		return null;
	}
}
