package com.apcfss.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.apcfss.DAO.LoginDao;
import com.apcfss.configurations.JwtTokenUtil;
import com.apcfss.form.LoginForm;
import com.apcfss.utils.CommonUtils;

@Controller
public class LoginController {

	@Autowired
	LoginDao loginDAO;

	@Autowired
	JwtTokenUtil jwt;

	@GetMapping("/valid/{token}")
	@ResponseBody
	public boolean tokenValid(@PathVariable("token") String token) {

		String jwtToken = jwt.generateToken(token);
		System.out.println("token --->" + jwtToken);
		return jwt.validateToken(jwtToken);
	}

	@GetMapping("/gen/{token}")
	@ResponseBody
	public String gen(@PathVariable("token") String token) {
		return jwt.generateToken(token);
	}

	@GetMapping("/validToken/{token}")
	@ResponseBody
	public boolean tokenVali(@PathVariable("token") String token) {

		return jwt.validateToken(token);
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("loginForm", new LoginForm());
		HttpSession sessObj = request.getSession(true);
		String resposneAttr = CommonUtils.getCaptchaImage();
		System.out.println("resposneAttr--login-" + resposneAttr);
		String resposne = resposneAttr.split("@@@@@@@@")[0];
		String captchaStr = resposneAttr.split("@@@@@@@@")[1];
		mav.addObject("captchaCode", resposne);
		mav.addObject("captchaStr", captchaStr);
		sessObj.setAttribute("captcha", captchaStr);
		return mav;
	}

	@RequestMapping(value = "/loginCheck", method = RequestMethod.POST)
	// public ModelAndView loginValidate(HttpServletRequest request,
	// HttpServletResponse response,@Valid @ModelAttribute("loginForm")LoginForm
	// loginBean,@ModelAttribute("workFlowConfigForm")WorkFlowConfigForm
	// workFlowConfigForm, BindingResult result)
	public ModelAndView loginValidate(HttpServletRequest request, HttpServletResponse response,
			@Valid @ModelAttribute("loginForm") LoginForm loginBean, BindingResult result) {
		String page = null, msg = null;
		ModelAndView mav = null;
		HttpSession sessObj = request.getSession(true);
		List services = new ArrayList();
		List childServices = new ArrayList();
		List allServices = new ArrayList();
		List reportservices = new ArrayList();
		List budgetServices = new ArrayList();
		List expenditureServices = new ArrayList();
		List workFlowServices = new ArrayList();
		int count = 0;
		String typeofUsr = "";
		String moduleType = "";
		try {
			if (result.hasErrors()) {
				mav = new ModelAndView("login");
				String resposneAttr = CommonUtils.getCaptchaImage();
				String resposne = resposneAttr.split("@@@@@@@@")[0];
				String captchaStr = resposneAttr.split("@@@@@@@@")[1];
				mav.addObject("captchaCode", resposne);
				mav.addObject("captchaStr", captchaStr);
				sessObj.setAttribute("captcha", captchaStr);
			} else {
				msg = loginDAO.checkLogginDetails(loginBean.getUserName(), loginBean.getPassword());

				if (msg != "" && msg != null) {
					mav = new ModelAndView("login");
					String resposneAttr = CommonUtils.getCaptchaImage();
					String resposne = resposneAttr.split("@@@@@@@@")[0];
					String captchaStr = resposneAttr.split("@@@@@@@@")[1];
					mav.addObject("captchaCode", resposne);
					mav.addObject("captchaStr", captchaStr);
					sessObj.setAttribute("captcha", captchaStr);
					request.setAttribute("errorMessage", msg);
					page = "login";
					mav.setViewName(page);
				}
//	    			 else if(!loginDAO.checkCreatedDate(loginBean.getUserName(),loginBean.getPassword())) { 
//						  mav = new ModelAndView("forgotPassword");
//						  mav.addObject("forgotPasswordForm",new ForgotPasswordForm());
//						  request.setAttribute("errorMessage","Password Expired. Please change your Password"); 
//						  page="forgotPassword";
//						  mav.addObject("userName", loginBean.getUserName());
//						  mav.addObject("otpValidation","true");
//						  request.setAttribute("loggedin","N");
//						  mav.setViewName(page); 
//					} 
				else {
					List budgetDistributiontrnDetails = new ArrayList();
					mav = new ModelAndView("home");
					sessObj.setAttribute("welcomeUser", loginBean.getUserName());
					sessObj.setAttribute("user", loginBean.getUserName());
					loginDAO.insertUserLogDetails(loginBean.getUserName(), request);
					services = loginDAO.getParentServices();
					childServices = loginDAO.getServicesbyParentId(moduleType);
					typeofUsr = loginDAO.getTypeofUser(loginBean.getUserName());
					System.out.println("typeofUsr------------" + typeofUsr);
					Optional<String> checkNull = Optional.ofNullable(typeofUsr);
					if (checkNull.isPresent()) {
						workFlowServices = loginDAO.getWorkFlowDetailsforFMU(loginBean.getUserName());
					} else {
						workFlowServices = loginDAO.getWorkFlowDetails(loginBean.getUserName());
					}
					count = loginDAO.getUserType(loginBean.getUserName());
					System.out.println("count--@@@@-" + count);
					if (count > 0) {
						mav.addObject("userType", "HOD");
					}
					budgetDistributiontrnDetails = loginDAO.getBudgetTransactionDetails(loginBean.getUserName());
					mav.addObject("budgettransactionList", budgetDistributiontrnDetails);
					allServices = loginDAO.getAllServices();
					reportservices = loginDAO.getReportServices();
					budgetServices = loginDAO.getBudgetServices(loginBean.getUserName());
					expenditureServices = loginDAO.getExpenditureServices();
					mav.addObject("servicesList", services);
					mav.addObject("childservicesList", childServices);
					mav.addObject("allServiceList", allServices);
					mav.addObject("reportservicesList", reportservices);
					mav.addObject("budgetservicesList", budgetServices);
					mav.addObject("expenditureServiceList", expenditureServices);
					mav.addObject("workFlowServiceList", workFlowServices);
					sessObj.setMaxInactiveInterval(2 * 60 * 60); // 2 hours
					page = "home";
					mav.setViewName(page);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return mav;
	}

	@RequestMapping(value = "/secure/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		// `String page="login";
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				request.setAttribute("sessionLogout", "Yes");
				request.getSession().invalidate();
			}
			// response.sendRedirect("/");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/login";
	}

	@RequestMapping(value = "loginHome", method = RequestMethod.POST)
	public ModelAndView home(HttpServletRequest request, HttpServletResponse response, LoginForm loginForm,
			BindingResult bindingResult) {
		ModelAndView mav = null;
		String errorMessage = null;
		List services = new ArrayList();
		List childServices = new ArrayList();
		List allServices = new ArrayList();
		List reportservices = new ArrayList();
		List budgetServices = new ArrayList();
		List expenditureServices = new ArrayList();
		List workFlowServices = new ArrayList();
		String typeofUsr = null;
		HttpSession httpSession = null;
		int count = 0;
		List budgetDistributiontrnDetails = new ArrayList();
		try {
			httpSession = request.getSession(false);
			String user = (String) httpSession.getAttribute("user");
			Optional<HttpSession> checkNull = Optional.ofNullable(httpSession);
			if (checkNull.isPresent()) {
				mav = new ModelAndView("home");
				typeofUsr = loginDAO.getTypeofUser(user);
				Optional<String> checkNull1 = Optional.ofNullable(typeofUsr);
				if (checkNull1.isPresent()) {
					workFlowServices = loginDAO.getWorkFlowDetailsforFMU(user);
				} else {
					workFlowServices = loginDAO.getWorkFlowDetails(user);
				}
				count = loginDAO.getUserType(user);
				if (count > 0) {
					mav.addObject("userType", "HOD");
				}
				budgetDistributiontrnDetails = loginDAO.getBudgetTransactionDetails(user);
				mav.addObject("budgettransactionList", budgetDistributiontrnDetails);
				allServices = loginDAO.getAllServices();
				reportservices = loginDAO.getReportServices();
				budgetServices = loginDAO.getBudgetServices(user);
				expenditureServices = loginDAO.getExpenditureServices();
				mav.addObject("servicesList", services);
				mav.addObject("childservicesList", childServices);
				mav.addObject("allServiceList", allServices);
				mav.addObject("reportservicesList", reportservices);
				mav.addObject("budgetservicesList", budgetServices);
				mav.addObject("expenditureServiceList", expenditureServices);
				mav.addObject("workFlowServiceList", workFlowServices);
			}
		} catch (Exception e) {
			e.printStackTrace();
			errorMessage = "Unable to Process Your request. Please Try Again.";
			mav.addObject("errorMessage", errorMessage);
		}
		return mav;
	}

}