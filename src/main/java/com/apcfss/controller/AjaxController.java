package com.apcfss.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.apcfss.DAO.ForgotPasswordDao;
import com.apcfss.DAO.LoginDao;
import com.apcfss.exception.DAOException;
import com.apcfss.model.ServiceMenu;

public class AjaxController {
	
	@Autowired
	ForgotPasswordDao forgotPasswordDao;
	
	@Autowired
	LoginDao loginDao;
	
	@Autowired
	private HttpSession session;
	

	@RequestMapping(value="/checkPassword",method = RequestMethod.GET)
	@ResponseBody
	public String checkPass(HttpServletRequest request, HttpServletResponse response) throws DAOException, Exception 
	{
			String password=request.getParameter("pass");
			String userName=request.getParameter("userName");
			String result=forgotPasswordDao.checkPassword(userName, password);
			return result;
	}
	@RequestMapping(value="/checkMobileNo",method = RequestMethod.GET)
	@ResponseBody
	public String checkMob(HttpServletRequest request, HttpServletResponse response) throws DAOException, Exception 
	{
			String userName=request.getParameter("userName");
			String mobileNo=request.getParameter("mobileNo");
			String result=forgotPasswordDao.getPhoneNumberFromDB(userName,mobileNo);
			return result;
	}
	@RequestMapping(value="/childServices",method = RequestMethod.GET)
	@ResponseBody
	public List<ServiceMenu> getChildServices(HttpServletRequest request, HttpServletResponse response) throws DAOException, Exception 
	{
			String moduleType=request.getParameter("moduleType");
			List<ServiceMenu> result=loginDao.getServicesbyParentId(moduleType);
			System.out.println("In ajax Controller----------"+result);
			return result;
	}


}
