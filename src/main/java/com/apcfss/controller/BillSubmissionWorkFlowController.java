package com.apcfss.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.apcfss.model.ExpenditureBillSubmissionWorkflow;
import com.apcfss.model.LabelValueBean;
import com.apcfss.service.BillSubmissionWorkFlowService;

@Controller
@RequestMapping("/BillSubmission")
public class BillSubmissionWorkFlowController {

	@Autowired
	BillSubmissionWorkFlowService billSubmissionWorkFlowservice;
	@Autowired
	private HttpSession session;

	
	@RequestMapping("/Maker/Show")
	public ModelAndView createSubmissionWorkFlow(HttpServletRequest request, ExpenditureBillSubmissionWorkflow expenditureBillSubmissionWorkflow) {
		String username = (session.getAttribute("user") != null
				&& !session.getAttribute("user").toString().trim().equals(""))
						? session.getAttribute("user").toString()
						: null;
//		String cfmsId = "14419052";
		String ddocode = "27001701038";
		
		ModelAndView mav = new ModelAndView("billSubmissionWorkFlow");
		mav.addObject("type","M");
		mav.addObject("submitPath","./BillSubmission/Maker/Save");
		
		
		List<LabelValueBean> ddoList;
		List<Map<String, Object>> positionList;
		List<LabelValueBean> hoaList;
		List<LabelValueBean> orgList;
		List<Map<String, Object>> workFlowList;
		try {
			mav.addObject("expenditureBillSubmissionWorkflow", new ExpenditureBillSubmissionWorkflow());
			ddoList = billSubmissionWorkFlowservice.getDDOCodesByCFfmsId(Long.parseLong(username));
			positionList = billSubmissionWorkFlowservice.getPostionsInOrgIdbycfmsId(username);
			hoaList = billSubmissionWorkFlowservice.getHoaByDDO(ddocode);
			orgList = billSubmissionWorkFlowservice.getOrganisationByCfmsId(Long.parseLong(username));
			
			workFlowList = billSubmissionWorkFlowservice.getWorflowBillSubmissionMaker();
			
			
			mav.addObject("ddoList", ddoList);
			mav.addObject("positionList", positionList);
			mav.addObject("hoaList", hoaList);
			mav.addObject("orgList", orgList);
			mav.addObject("workFlowList", workFlowList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mav;
	}

	@RequestMapping(value = "/Maker/Save", method = RequestMethod.POST)
	public ModelAndView saveSubmissionWorkFlow(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("expenditureBillSubmissionWorkflow") ExpenditureBillSubmissionWorkflow expenditureBillSubmissionWorkflow)
	{

		String ddocode = "27001701038";
		expenditureBillSubmissionWorkflow.setDdocode(ddocode);
		try {
			billSubmissionWorkFlowservice.saveSubmissionWorkFlow(expenditureBillSubmissionWorkflow, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(expenditureBillSubmissionWorkflow.getHoa());		
		System.out.println(expenditureBillSubmissionWorkflow.getPositionId());		
		System.out.println(expenditureBillSubmissionWorkflow.getOrgId());		
		System.out.println(expenditureBillSubmissionWorkflow.getStartDate());		
		System.out.println(expenditureBillSubmissionWorkflow.getEndDate());		
		return createSubmissionWorkFlow(request, expenditureBillSubmissionWorkflow);
		
	}
	
	@RequestMapping("/Checker/Show")
	public ModelAndView checkerBillSubmissionWorkFlow(HttpServletRequest request, ExpenditureBillSubmissionWorkflow expenditureBillSubmissionWorkflow) {
		String username = (session.getAttribute("user") != null
				&& !session.getAttribute("user").toString().trim().equals(""))
						? session.getAttribute("user").toString()
						: null;
//		String cfmsId = "14419052";
		String ddocode = "27001701038";
		ModelAndView mav = new ModelAndView("billSubmissionWorkFlow");
		
		
		List<LabelValueBean> ddoList;
		List<Map<String, Object>> positionList;
		List<LabelValueBean> hoaList;
		List<LabelValueBean> orgList;
		try {
			mav.addObject("expenditureBillSubmissionWorkflow", new ExpenditureBillSubmissionWorkflow());
			mav.addObject("type","C");
			mav.addObject("submitPath","./BillSubmission/Checker/Save");
			
			ddoList = billSubmissionWorkFlowservice.getDDOCodesByCFfmsId(Long.parseLong(username));
			positionList = billSubmissionWorkFlowservice.getPostionsInOrgIdbycfmsId(username);
			hoaList = billSubmissionWorkFlowservice.getHoaByDDO(ddocode);
			
			
			orgList = billSubmissionWorkFlowservice.getOrganisationByCfmsId(Long.parseLong(username));
			
			
			mav.addObject("ddoList", ddoList);
			mav.addObject("positionList", positionList);
			mav.addObject("hoaList", hoaList);
			mav.addObject("orgList", orgList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mav;
	}
}
