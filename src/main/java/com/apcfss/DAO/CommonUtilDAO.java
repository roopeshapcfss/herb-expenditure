package com.apcfss.DAO;

import java.util.List;
import java.util.Map;

public interface CommonUtilDAO 
{
	/*
	 *  generic insert without auto generation key 
	 */
  public int genericInsert(Map<String, Object> genericMap, String tableName);
  /*
   * generic insert with autokey 
   */
  public int genericInsertWithAutoKey(Map<String, Object> genericMap, String tableName,String keyName);
    /*
   * generic insert with batch transaction 
   */
  public boolean genericInsertWithTransaction(List<Map<String, Object>> genericMap, List<String> tableNames);
 
  /*  update details 
   * 
   */
	/*
	 * public int genericUpdate(Map<String, Object> updateValuesMap, String
	 * tableName, Map<String, Object> whereConditionMap);
	 */ 	
  /*  
   * it converts pojo properties to database columns fields 
   *   
   */
  public Map<String,Object> pojotoMapConverter(Object pojo);
 
  public Map<String, Object> mapToDatabaseFiledsConverter(Map<String, Object> hashMap);
  
  
  
}
