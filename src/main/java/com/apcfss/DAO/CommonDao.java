package com.apcfss.DAO;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
public interface CommonDao {
	public int genericInsert(Map<String, Object> genericMap, String tableName);
	public boolean genericInsertWithTransaction(List<Map<String, Object>> genericMap, List<String> tableNames);
	public int genericInsertWithColumns(Map<String, Object> genericMap,String tableNames) throws Exception;
	
	String getChallanNo() throws SQLException;
	
}
