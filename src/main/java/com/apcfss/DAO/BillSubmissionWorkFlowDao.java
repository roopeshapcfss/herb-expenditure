package com.apcfss.DAO;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.apcfss.exception.DAOException;
import com.apcfss.model.ExpenditureBillSubmissionWorkflow;
import com.apcfss.model.LabelValueBean;

public interface BillSubmissionWorkFlowDao {

	List<LabelValueBean> getOrganisationByCfmsId(Long cfmsId) throws DAOException;

	List<LabelValueBean> getDDOCodesByCFfmsId(Long cfmsId) throws DAOException;

	List<Map<String, Object>> getPostionsInOrgIdbycfmsId(String cfmsId) throws DAOException;

	List<LabelValueBean> getHoaByDDO(String ddocode) throws DAOException;

	boolean saveSubmissionWorkFlow(Map<String,Object> map) throws DAOException;

	List<Map<String, Object>> getWorflowBillSubmissionMaker() throws DAOException;
	
}
