package com.apcfss.DAO;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.apcfss.utils.DBUtils;
import com.apcfss.utils.QueryConstructor;
@Repository("commonDAO")
public class CommonDaoImpl implements CommonDao{
	
	@Autowired
	private DataSource dataSource;
	
	JdbcTemplate jdbcTemplate;

	@Autowired
	 private PlatformTransactionManager transactionManager;

		@Autowired
		public CommonDaoImpl(DataSource dataSource) {
			jdbcTemplate = new JdbcTemplate(dataSource);
		}

		public int genericInsert(Map<String, Object> genericMap, String tableName) 
		{	
			int count = 0;
			try{
				System.out.println(genericMap);
				SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName(tableName).usingColumns(String.join(", ", genericMap.keySet())) ;
				System.out.println(QueryConstructor.insertQuery(tableName, genericMap));
				count = simpleJdbcInsert.execute(genericMap);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return count;
		}
		
	
		public boolean genericInsertWithTransaction(List<Map<String, Object>> genericMap, List<String> tableNames) 
	{
		boolean resultStatus=false;
		TransactionDefinition def = new DefaultTransactionDefinition();
	      TransactionStatus status = transactionManager.getTransaction(def);
	      int count=0;
	      try{
	    	  for(int i=0;i<tableNames.size();i++)
	    	  {
	    		  count=genericInsert(genericMap.get(i), tableNames.get(i));
	    		  if(count==0)
	    		  {
	    			  transactionManager.rollback(status) ;
	    			  resultStatus=false;
	    			  break;
	    		  }
	    	  }
	    	  
	    		  transactionManager.commit(status);  
		    	  resultStatus=true;	  
	    	  
	    	 
	      }
	      catch (Exception e) {
	    	  e.printStackTrace();
	    	  resultStatus=false;
	    	  transactionManager.rollback(status) ;
		}
		return resultStatus;
	}

		public int genericInsertWithColumns(Map<String, Object> genericMap,String tableName)  throws Exception
		{
			int count = 0;
			try{
				SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName(tableName).usingColumns(genericMap.keySet().stream().toArray(String[]::new));
				System.out.println(QueryConstructor.insertQuery(tableName, genericMap));
				count = simpleJdbcInsert.execute(genericMap);
			}catch (Exception e) {
				e.printStackTrace();
				count=0;
				throw e;
			}
				return count;
		}
		
		@Override
		/* Added to generate ChallanNo */
		public String getChallanNo() throws SQLException {
			String getChallanStr = "select max(transid)+1 as challanno  from treceipts";
			//String getChallanStr = "select nextval('herb_test_challan_seq') as challanno ";
			String challanNo=DBUtils.getStringfromQuery(dataSource.getConnection(), getChallanStr);
			return challanNo;
		}
			
		
		
		
}
