package com.apcfss.DAO;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.apcfss.beanmappers.LabelValueBeanMapper;
import com.apcfss.model.LabelValueBean;

public class UserServicesMappingDAOImpl implements UserServicesMappingDAO 
{
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public UserServicesMappingDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public List<LabelValueBean> getServices(String serviceType) 
	{
		String sql="select service_id as value,service_desc as label from  services_mst  where service_type=? order by service_desc ";
		return jdbcTemplate.query(sql, new Object[]{serviceType},new LabelValueBeanMapper());
	}


	
	
	
}
