package com.apcfss.DAO;

import javax.servlet.http.HttpServletRequest;

import com.apcfss.exception.DAOException;
import com.apcfss.form.ForgotPasswordForm;

public interface ForgotPasswordDao {
	
	String getUserName(String userName) throws DAOException;
	
	String insertOtp(ForgotPasswordForm forgotPasswordForm,HttpServletRequest request) throws DAOException;
	
	String validOtp(String userName, String enterOtp)throws DAOException;
	
	String getPhoneNumberFromDB(String userName) throws DAOException ;
	
	String updatePassword(ForgotPasswordForm forgotPasswordForm,HttpServletRequest request)throws DAOException;
	
	String checkPassword(String userName, String enterOtp)throws DAOException, Exception;

	String getPhoneNumberFromDB(String userName, String mobileNo)throws DAOException, Exception;

}
