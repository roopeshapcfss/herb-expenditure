package com.apcfss.DAO;  
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.apcfss.utils.CommonFunctionUtils;
import com.apcfss.utils.QueryConstructor;

@Repository("commonUtilDAO")
public class CommonUtilDAOImpl implements CommonUtilDAO 
{
	
JdbcTemplate jdbcTemplate;

@Autowired
 private PlatformTransactionManager transactionManager;

	@Autowired
	public CommonUtilDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public int genericInsert(Map<String, Object> genericMap, String tableName) 
	{	
		int count = 0;
		try{
			System.out.println(genericMap);
			SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName(tableName);
			System.out.println(QueryConstructor.insertQuery(tableName, genericMap));
			count = simpleJdbcInsert.execute(genericMap);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public boolean genericInsertWithTransaction(List<Map<String, Object>> genericMap, List<String> tableNames) 
	{
		boolean resultStatus=false;
		TransactionDefinition def = new DefaultTransactionDefinition();
	      TransactionStatus status = transactionManager.getTransaction(def);
	      int count=0;
	      try{
	    	  for(int i=0;i<tableNames.size();i++)
	    	  {
	    		  count=genericInsert(genericMap.get(i), tableNames.get(i));
	    		  if(count==0)
	    		  {
	    			  transactionManager.rollback(status) ;
	    			  resultStatus=false;
	    			  break;
	    		  }
	    	  }
	    	  transactionManager.commit(status);  
	    	  resultStatus=true;
	      }
	      catch (Exception e) {
	    	  e.printStackTrace();
	    	  resultStatus=false;
	    	  transactionManager.rollback(status) ;
		}
		return resultStatus;
	}
	
	public int genericInsertWithAutoKey(Map<String, Object> genericMap,String tableName,String keyName) 
	{
		int count = 0;
		try{
			SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName(tableName).usingGeneratedKeyColumns(keyName);
			System.out.println(QueryConstructor.insertQuery(tableName, genericMap));
			count = simpleJdbcInsert.execute(genericMap);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return count;
		}
	
	
		/*
		 * public int genericUpdate(Map<String, Object> updateValuesMap,String
		 * tableName, Map<String, Object> whereConditionMap) { int count=0; try{
		 * SimpleJdbcUpdate simpleJdbcUpdate = new
		 * SimpleJdbcUpdate(jdbcTemplate).withTableName(tableName);
		 * simpleJdbcUpdate.setTableName(tableName);
		 * count=simpleJdbcUpdate.execute(updateValuesMap, whereConditionMap); } catch
		 * (Exception e) { e.printStackTrace(); } return count; }
		 */
	
	 
	 public Map<String, Object> pojotoMapConverter(Object pojo) 
	 {
		 Map<String, Object> hashMap = new LinkedHashMap<String, Object>();
		 Map<String, Object> resultMap = new LinkedHashMap<String, Object>();	
		    try {
		        Class<? extends Object> c = pojo.getClass();
		        Method m[] = c.getMethods();
		        for (int i = 0; i < m.length; i++) {
		            if (m[i].getName().indexOf("get") == 0) {
		                String name = m[i].getName().substring(3, 4) + m[i].getName().substring(4);
		                System.out.println("namenamename"+name );
		                if(!name.equalsIgnoreCase("class"))
		                hashMap.put(name,m[i].invoke(pojo, new Object[0]));
		            }
		        }
		        for (Map.Entry<String,Object> entry : hashMap.entrySet())  
		        {
		        	if(CommonFunctionUtils.validateString(entry.getValue()+""))
		        	{
		        		resultMap.put(uppertoUnderScore( entry.getKey()).toLowerCase(), entry.getValue());	
		        	}
		        	 
		        }
		        
		        
		        
		    } catch (Throwable e) {
		    	e.printStackTrace();
		        //log error
		    }
		    
		    return resultMap;
	}
	 
	
	 public Map<String, Object> mapToDatabaseFiledsConverter(Map<String, Object> hashMap) 
	 {
		 Map<String, Object> resultMap = new HashMap<String, Object>();	
		    try {
		        for (Map.Entry<String,Object> entry : hashMap.entrySet())  
		        {
		        	resultMap.put(uppertoUnderScore( entry.getKey()).toLowerCase(), entry.getValue()); 
		        }
		    } catch (Throwable e) {
		    	e.printStackTrace();
		    }
		    return resultMap;
	}
	 
	 
	 String uppertoUnderScore(String name)
	 {
		  StringBuffer sb=new StringBuffer();
		  
		  name= name.substring(0, 1).toLowerCase()+ name.substring(1);
		  
		  for (int i = 0 ; i <= name.length()-1 ; i++) {
			  
			  if( Character.isUpperCase(name.charAt(i)) )
			  {System.out.println("inside xxx "+sb.toString()+"i"+i);
				  sb.append("_"+name.charAt(i)); 
			  }
			  else{
				  sb.append(name.charAt(i)); 
			  }
		  }
		  return sb.toString();
		 
	 }
	 
	/* public static void main(String[] args) 
	 {
		 LandConversionCharges landConversionCharges=new LandConversionCharges();
		 landConversionCharges.setAmount(11);
		 landConversionCharges.setMuncipalityId(20);
		 landConversionCharges.setMunicipalLimits("Inside");
		System.out.println(pojotoMapConverter(landConversionCharges)); 
	}*/
	
}