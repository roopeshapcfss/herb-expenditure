package com.apcfss.DAO;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.apcfss.exception.DAOException;
import com.apcfss.form.ForgotPasswordForm;
import com.apcfss.service.ServiceUtil;

@Repository
public class ForgotPasswordDaoImpl implements ForgotPasswordDao {
	JdbcTemplate jdbcTemplate;
	@Autowired
	DataSource dataSource;
	
	@Autowired
	ServiceUtil serviceUtil;
	
	@Autowired
	public ForgotPasswordDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	@Autowired
	CommonDao commonDAO;
	@Autowired
	 private PlatformTransactionManager transactionManager;
	
	
	public String getUserName(String userId) throws DAOException {
	    try {
	        final String sql = "select userid from user02 where userid = ?";
	        userId = (String)this.jdbcTemplate.queryForObject(sql, new Object[] { userId }, (Class)String.class);
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
		return userId;
	}

	/* Added for Inserting OTP sent to Mobile ----START */
	public String insertOtp(ForgotPasswordForm forgotPasswordForm,HttpServletRequest request) throws DAOException {
		int count=0;
		TransactionDefinition def = new DefaultTransactionDefinition();
		  TransactionStatus status = transactionManager.getTransaction(def);
		  String successornot="";
		try {
		Map<String, Object> userDetailsMap=null;
		userDetailsMap=new LinkedHashMap<String, Object>();
		userDetailsMap.put("user_id", forgotPasswordForm.getUserName());
		userDetailsMap.put("new_password", forgotPasswordForm.getGenPassword());
		userDetailsMap.put("changed_ip", request.getRemoteAddr());
		userDetailsMap.put("encrypted_password", serviceUtil.md5Encrypt(forgotPasswordForm.getGenPassword()));
		userDetailsMap.put("changed_by", forgotPasswordForm.getUserName());
		count=commonDAO.genericInsertWithColumns(userDetailsMap, "change_password"); 
		if(count>0)
		{
			transactionManager.commit(status);
			successornot="Success";
		}else {
			successornot="Failure";
		}
		}catch (Exception e) {
			// TODO: handle exception
			 transactionManager.rollback(status) ;
		}
		return count>0?successornot:successornot; 
		
	}
	/* Added for Inserting OTP sent to Mobile ----END */
	
	
	/* Added for Validating  OTP entered while changing Password ----START */
	public String validOtp(String userId, String enterOtp) throws DAOException {
		String optFromDB = null;
		String status="";
	    try {
	        final String sql = "select encrypted_password from change_password where user_id = ? order by changed_on desc limit 1";
	        optFromDB = (String)this.jdbcTemplate.queryForObject(sql, new Object[] { userId }, (Class)String.class);
	        if(enterOtp.equals(optFromDB))
            {
            	status="Success";
            }
            else
            {
            	status="Failure";
            }
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
		return status;
	}
	/* Added for Validating  OTP entered while changing Password ----END */
	
	
	/* Method  for Validating  Registered mobile Number ----START */
	public String getPhoneNumberFromDB(String userId) {
		
		String phoneNumber = null;
	    try {
	        final String sql = "select mobile_no from user02 where userid=?";
	        phoneNumber = (String)this.jdbcTemplate.queryForObject(sql, new Object[] { userId }, (Class)String.class);
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	    return phoneNumber;
	}
	/* Method  for Validating  Registered mobile Number ----END */
	
	
	
	/* Method  for Updating/inserting changed Password ----START */
	public String updatePassword(ForgotPasswordForm forgotPasswordForm,HttpServletRequest request)
			throws DAOException {
		String optFromDB = null,statuss=null;
		int count=0,count1=0;
		  String successornot="";
		TransactionDefinition def = new DefaultTransactionDefinition();
		  TransactionStatus status = transactionManager.getTransaction(def);
		try {
			final String sql = "select new_password from change_password where user_id= ? order by changed_on desc limit 1";
			optFromDB = (String)this.jdbcTemplate.queryForObject(sql, new Object[] { forgotPasswordForm.getUserName() }, (Class)String.class);
			String enterdOtp=forgotPasswordForm.getEnterOtp();
			System.out.println("forgotPasswordForm.getLoggedin() in update password----"+forgotPasswordForm.getLoggedin());
			if("N".equalsIgnoreCase(forgotPasswordForm.getLoggedin())) {
				enterdOtp=serviceUtil.md5Encrypt(optFromDB);
				Map<String, Object> userDetailsMap=null;
				userDetailsMap=new LinkedHashMap<String, Object>();
				userDetailsMap.put("user_id", forgotPasswordForm.getUserName());
				userDetailsMap.put("new_password", enterdOtp);
				userDetailsMap.put("changed_ip", request.getRemoteAddr());
				userDetailsMap.put("encrypted_password", serviceUtil.md5Encrypt(forgotPasswordForm.getGenPassword()));
				userDetailsMap.put("changed_by", forgotPasswordForm.getUserName());
				count=commonDAO.genericInsertWithColumns(userDetailsMap, "change_password"); 
				if(count>0)
				{
					transactionManager.commit(status);
					successornot="Success";
				}else {
					successornot="Failure";
				}
			}
			if(enterdOtp.equals(serviceUtil.md5Encrypt(optFromDB)))
			{
				List<Object> userLogInfoList=new ArrayList<Object>();
				userLogInfoList.add(serviceUtil.md5Encrypt(forgotPasswordForm.getOrigpassword()));
				userLogInfoList.add(request.getRemoteAddr());
				userLogInfoList.add(forgotPasswordForm.getUserName());
				userLogInfoList.add(forgotPasswordForm.getUserName());
		   		String sql1 = "update user02 set iniit_password=?,ip_address=?, date_of_last_change = now(), created_by = ? ,created_on=now() where userid = ?";
				count=jdbcTemplate.update (sql1, userLogInfoList.toArray());
				List<Object> changeLogInfoList=new ArrayList<Object>();
				changeLogInfoList.add(forgotPasswordForm.getOrigpassword());
				changeLogInfoList.add(serviceUtil.md5Encrypt(forgotPasswordForm.getOrigpassword()));
				changeLogInfoList.add(forgotPasswordForm.getUserName());
				changeLogInfoList.add(optFromDB);
				String sql2="update change_password set updated_password=?,encrypted_password=? where user_id = ? and new_password=?";
				count1=jdbcTemplate.update (sql2, changeLogInfoList.toArray());
				if(count>0 && count1>0) {
					statuss="Success";
					 transactionManager.commit(status);
				}else {
					statuss="Failure"; 
				}
				 } 
		} catch (Exception e) {
	        e.printStackTrace();
	        transactionManager.rollback(status) ;
	    }
		
				return statuss;
		
	}
	
	/* Method  for Updating/inserting changed Password ----END */
	
	/* Method  for  Password validation for changing Password should not be last 3 passwords ----START */
	public String checkPassword(String userName, String password) throws DAOException, Exception {
		String sql="";
		String msg="";
		int count=0;
		try {
			sql=" select count(*) as pcount from " + 
					" (select updated_password as oldpassword from change_password where user_id=? order by changed_on desc limit 3)a " + 
					" where oldpassword=?";
			count=jdbcTemplate.queryForObject(sql,new Object[] {userName,password},Integer.class);
			if(count<=0) {
				msg="";
			}else {
				msg="Users should not reuse last three passwords";
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return msg;

	}
	/* Method  for  Password validation for changing Password should not be last 3 passwords ----END */

	/* Method  for Validating  Registered mobile Number ----START */
	@Override
	public String getPhoneNumberFromDB(String userName, String mobileNo) throws DAOException, Exception {
		
		String msg = null;
		int count=0;
	    try {
	        final String sql = "select count(*) from user02 where userid=? and mobile_no=?";
	        count=jdbcTemplate.queryForObject(sql,new Object[] {userName,mobileNo},Integer.class);
	        if(count<=0) {
	        	msg="User Should enter Registered Mobile Number";
			}else {
				msg="";
			}
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	    return msg;
	}
	/* Method  for Validating  Registered mobile Number ----END */
}
