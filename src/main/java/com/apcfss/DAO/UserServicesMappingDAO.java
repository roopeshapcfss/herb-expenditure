package com.apcfss.DAO;

import java.util.List;

import com.apcfss.model.LabelValueBean;

public interface UserServicesMappingDAO
{
	List<LabelValueBean> getServices(String serviceType);
	
	
}
