package com.apcfss.DAO;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.apcfss.model.ServiceMenu;
import com.apcfss.model.WorkFlowMenu;

public interface LoginDao {
		
	String  checkLogginDetails(String username, String password) throws Exception ;
	boolean checkCreatedDate(String username,String password) throws Exception ;
	public String insertUserLogDetails(String username,HttpServletRequest request) throws Exception;
	public List<ServiceMenu>  getParentServices() ;
	public List<ServiceMenu>  getAllServices() ;
	public List<ServiceMenu>  getReportServices() ;
	public List<ServiceMenu>  getBudgetServices(String cfmsId) ;
	public List<ServiceMenu>  getExpenditureServices() ;
	public List<ServiceMenu>  getServicesbyParentId(String moduleType);
	public List<WorkFlowMenu>  getWorkFlowDetails(String userName) ;
	public List<WorkFlowMenu>  getWorkFlowDetailsforFMU(String userName) ;
	public int  getUserType(String username);
	public String getTypeofUser(String username);
	public List<Map<String, Object>> getBudgetTransactionDetails(String userName);
}
