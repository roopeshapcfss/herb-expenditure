
package com.apcfss.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.apcfss.beanmappers.LabelValueBeanMapper;
import com.apcfss.model.LabelValueBean;
import com.apcfss.utils.CommonFunctionUtils;
import com.apcfss.utils.DBUtils;
import com.apcfss.utils.DatabaseTables;

@Repository("masterDAO")
public class MasterDAOImpl implements MasterDAO {

	@Autowired
	private DataSource dataSource;

	JdbcTemplate jdbcTemplate;

	@Autowired
	public MasterDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Map<String, Object>> getDepartments() {
		String// MVCConfigurations
		sql = " select deptcode,dept_desc from " + DatabaseTables.mdept + " order by deptcode  ";
		return jdbcTemplate.queryForList(sql);
	}

	@Override
	public List<Map<String, Object>> getSpecialServices(String serviceCode) {
		String sql = " select service_id,service_desc_short from " + DatabaseTables.service_list
				+ " where service_id=? order by service_id  ";
		return jdbcTemplate.queryForList(sql, new Object[] { serviceCode });
	}

	@Override
	public List<Map<String, Object>> getTreasuryPao(String distCode, String treasuryPaoCode) {
		String subCon = "";
		if (CommonFunctionUtils.validateString(treasuryPaoCode)) {
			subCon = " and stocode=? ";
		}
		String sql = " select stocode as stocode,stoname st from " + DatabaseTables.msto + " where dist=? " + subCon
				+ " order by stocode ";

		if (CommonFunctionUtils.validateString(treasuryPaoCode)) {
			return jdbcTemplate.queryForList(sql, new Object[] { distCode, treasuryPaoCode });
		} else {
			return jdbcTemplate.queryForList(sql, new Object[] { distCode });
		}
	}

	@Override
	public String getServicesAsJson(String departmentId, Integer challanType) {
		String subCon = "";
		if (challanType == 1) {
			subCon = " b.pd_challan_flag='X' ";
		} else if (challanType == 3) {
			subCon = " b.employee_challan_flag='X' ";
		} else if (challanType == 4) {
			subCon = " b.citizen_challan_flag='X' ";
		} else if (challanType == 5) {
			subCon = " b.civil_dep_flag='X' ";
		} else if (challanType == 7) {
			subCon = " b.jud_remitt_flag='X' ";
		} else if (challanType == 8) {
			subCon = " b.cap_dev_fund_flag='X' ";
		} else if (challanType == 9) {
			subCon = " b.escort_charges_flag='X' ";
		} else if (challanType == 10) {
			subCon = " b.guarcharges_flag='X' ";
		} else if (challanType == 17) {
			subCon = " b.pen_rec_challan_flag='X' ";
		} else if (challanType == 19) {
			subCon = " b.cps_contrirb_flag='X' ";
		}
		String sql = " select '[' || string_agg(json::varchar,',') || ']' from (select row_to_json(a) as json from (select a.service_id as servicecode,service_desc_short from "
				+ DatabaseTables.service_list + " a " + " inner join " + DatabaseTables.challan_services
				+ " b on(a.service_id=b.service_id) where (b.deptcode=trim(?) or b.deptcode='ALL') and " + subCon
				+ " order by a.service_id ) a) a ";
		String json = (String) jdbcTemplate.queryForObject(sql, new Object[] { departmentId }, String.class);
		System.out.println("sql---" + sql + "--dept--" + departmentId);
		return json;
	}

	@Override
	public String getHOADetailsAsJson(String departmentId, String serviceCode, Integer challanType) {
		String subCon = "";
		if (challanType == 1) {
			subCon = " b.pd_challan_flag='X' ";
		} else if (challanType == 3) {
			subCon = " b.employee_challan_flag='X' ";
		} else if (challanType == 4) {
			subCon = " b.citizen_challan_flag='X' ";
		} else if (challanType == 5) {
			subCon = " b.civil_dep_flag='X' ";
		} else if (challanType == 7) {
			subCon = " b.jud_remitt_flag='X' ";
		} else if (challanType == 8) {
			subCon = " b.cap_dev_fund_flag='X' ";
		} else if (challanType == 9) {
			subCon = " b.escort_charges_flag='X' ";
		} else if (challanType == 10) {
			subCon = " b.guarcharges_flag='X' ";
		} else if (challanType == 17) {
			subCon = " b.pen_rec_challan_flag='X' ";
		} else if (challanType == 19) {
			subCon = " b.cps_contrirb_flag='X' ";
		}
		String sql = " select '[' || string_agg(json::varchar,',') || ']' from (select row_to_json(a) as json from ("
				+ " select b.hoa as hoa from " + DatabaseTables.challan_services
				+ " b where (b.deptcode=trim(?) or b.deptcode='ALL') and " + subCon
				+ " and b.service_id=trim(?) order by b.hoa  " + ") a) a ";
		String json = (String) jdbcTemplate.queryForObject(sql, new Object[] { departmentId, serviceCode },
				String.class);
		System.out.println("sql---" + sql + "--service--" + serviceCode);
		return json;
	}

	@Override
	public List<Map<String, Object>> getDistricts(String distId) {
		String subCon = "";
		if (CommonFunctionUtils.validateString(distId)) {
			subCon = " where district_id=? ";
		}
		String sql = " select district_id,distname from " + DatabaseTables.mdistrict + " " + subCon
				+ " order by district_id ";

		if (CommonFunctionUtils.validateString(distId)) {
			return jdbcTemplate.queryForList(sql, new Object[] { distId });
		} else {
			return jdbcTemplate.queryForList(sql);
		}

	}

	@Override
	public String getTreasuryPaoAsJson(String distId) {
		String sql = " select '[' || string_agg(json::varchar,',') || ']' from (select row_to_json(a) as json from ("
				+ " select stocode as stocode,stoname st from " + DatabaseTables.msto
				+ " where dist=? order by stocode  " + ") a) a ";
		String json = (String) jdbcTemplate.queryForObject(sql, new Object[] { distId }, String.class);
		System.out.println("sql---" + sql + "--distId--" + distId);
		return json;
	}

	@Override
	public String getDDOCodeJson(String treasurypao) {
		String sql = " select '[' || string_agg(json::varchar,',') || ']' from (select row_to_json(a) as json from ("
				+ " select ddocode,ddodesg as ddo from mddo where stocode=trim(?)  " + ") a) a ";
		String json = (String) jdbcTemplate.queryForObject(sql, new Object[] { treasurypao }, String.class);
		System.out.println("sql---" + sql + "--treasurypao--" + treasurypao);
		return json;
	}

	@Override
	public List<LabelValueBean> getDistrictsAsOptions() {
		String sql = " select district_id as value, distname as label from " + DatabaseTables.mdistrict
				+ " order by district_id ";
		return jdbcTemplate.query(sql, new LabelValueBeanMapper());
	}

	@Override
	public List<LabelValueBean> getDepartmentsAsOptions() {
		String sql = " select deptcode as value,dept_desc as label from " + DatabaseTables.mdept
				+ "  order by dept_desc ";
		return jdbcTemplate.query(sql, new LabelValueBeanMapper());
	}

	@Override
	public List<Map<String, Object>> getSpecialDepartments(String deptCode) {
		String sql = " select deptcode,dept_desc from " + DatabaseTables.mdept
				+ " where deptcode=? order by deptcode  ";
		System.out.println("--sql--" + sql + "---deptcode---" + deptCode);
		return jdbcTemplate.queryForList(sql, new Object[] { deptCode });
	}

	@Override
	public String getchallanType(String challan) {
		String sql = " select challan_type from " + DatabaseTables.challantype + " where challan_desc=trim(?)";
		return jdbcTemplate.queryForObject(sql, new Object[] { challan }, String.class);
	}

	@Override
	public List<Map<String, Object>> getDistForEscortGuardCharges() {
		String sql = " select district_id,distname from " + DatabaseTables.mdistrict
				+ " where district_id in ('03','05','11','27') order by district_id  ";
		return jdbcTemplate.queryForList(sql);
	}

	@Override
	public String getEmpDetailsJson(String cfmsId) {
		String sql = " select '[' || string_agg(json::varchar,',') || ']' from (select row_to_json(a) as json from ("
				+ " select cfmsid,name from cfmsdata where cfmsid=? " + ") a) a ";
		String json = (String) jdbcTemplate.queryForObject(sql, new Object[] { cfmsId }, String.class);
		System.out.println("sql---" + sql + "--cfmsId--" + cfmsId);
		return json;
	}

	@Override
	public List<Map<String, Object>> getServices(String departmentId, Integer challanType) {

		String subCon = "";
		if (challanType == 1) {
			subCon = " b.pd_challan_flag='X' ";
		}

		String sql = " select a.service_id as servicecode,service_desc_short from service_list a left join challan_services b on b.service_id=a.service_id where  (b.deptcode=trim(?) or b.deptcode='ALL') and "
				+ subCon + "  order by a.service_id ";
		return jdbcTemplate.queryForList(sql, new Object[] { departmentId });
	}

	@Override
	public List<Map<String, Object>> getChallanStatusDetails(String transactionId) {
		String sql = " select to_char(a.challandate,'dd/mm/yyyy') as challandate,e.txn_id,e.first_name,e.sent_amount,a.transid,service_desc_short,a.hoa as hoa,distname,stoname,ddocode,chall_tot_amount from treceipts a "
				+ " left join service_list b on b.service_id=a.serviceid "
				+ " left join mdistrict c on c.district_id=a.distcode " + " left join msto d on d.stocode=a.stocode "
				+ " left join herb_test_payments e on e.transid=a.transid where a.transid=? order by transid   ";

		sql = " select 'Rupees ' || replace(cash_words(chall_tot_amount::money),'dollars and zero cents','only') as amount_words,remittername,transid,chall_tot_amount,b.service_desc_short,hoa,d.distname,e.stoname,f.ddocode,to_char(a.challandate,'dd/mm/yyyy') as challandate,a.bank_refno from treceipts a "
				+ " left join service_list b on b.service_id=a.serviceid "
				+ " left join mdistrict d on d.district_id=a.distcode " + " left join msto e on e.stocode=a.stocode "
				+ " left join mddo f on f.ddocode=a.ddocode  where transid=?  ";
		return jdbcTemplate.queryForList(sql, new Object[] { Integer.parseInt(transactionId) });
	}

	@Override
	public List<Map<String, Object>> getPaymentModes(String challanType) {
		String sql = " select a.challan_type,c.payment_way,c.column_id,c.column_value from "
				+ DatabaseTables.payment_modes_mapping + " a " + " left join " + DatabaseTables.challantype
				+ " b on a.challan_type=b.challan_type " + " left join " + DatabaseTables.payment_modes
				+ " c on a.payment_mode=c.payment_id where a.challan_type=? and a.is_delete is false order by c.payment_id ";
		CommonFunctionUtils.log("sql--" + sql + "--challanType--" + challanType);
		return jdbcTemplate.queryForList(sql, new Object[] { challanType });
	}

	@Override
	public List<Map<String, Object>> knowDeptList() {
		String sql = " select a.deptcode as deptcode ,dept_desc,service_desc_short,hoa from " + DatabaseTables.mdept
				+ " a left join " + DatabaseTables.challan_services + " b on a.deptcode=b.deptcode " + "left join "
				+ DatabaseTables.service_list
				+ " c on b.service_id=c.service_id order by dept_desc,service_desc_short,hoa ";
		return jdbcTemplate.queryForList(sql);
	}

}
