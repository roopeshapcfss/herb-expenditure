package com.apcfss.DAO;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.apcfss.beanmappers.LabelValueBeanMapper;
import com.apcfss.exception.DAOException;
import com.apcfss.model.ExpenditureBillSubmissionWorkflow;
import com.apcfss.model.LabelValueBean;
import com.apcfss.utils.CommonFunctionUtils;
import com.apcfss.utils.DatabaseTables;

@Repository("billSubmissionWorkFlowDao")
public class BillSubmissionWorkFlowDaoImpl implements BillSubmissionWorkFlowDao {

	@Autowired
	private PlatformTransactionManager transactionManager;
	
	@Autowired
	private ServletContext context;
	
	@Autowired
	CommonDao commonDAO;
	
	@Autowired
	CommonUtilDAO commonUtilDAO ;
	
	@Autowired
	 CommonFunctionUtils commonutils;
	
	
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	public BillSubmissionWorkFlowDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<LabelValueBean> getOrganisationByCfmsId(Long cfmsId)  throws DAOException
	{
		String sql="select org_unit_id as label,org_unit_id as value from org_unit_position_map where cfmsid=?";
		return jdbcTemplate.query(sql,new Object[] {cfmsId},new LabelValueBeanMapper());
	}
	
	@Override
	public List<LabelValueBean> getDDOCodesByCFfmsId(Long cfmsId) throws DAOException{
		
		String sql="select a.ddocode as value, description_long || '( '|| a.ddocode || ' )' as label from org_unit_ddocode_map as a" + 
				" left join ddo_mst as b on a.ddocode=b.ddocode" + 
				" where org_id::numeric in(" + 
				" select org_unit_id from org_unit_position_map where cfmsid= ?) and service_type='01'" + 
				" group by a.ddocode,a.ddocode,description_long";
		return jdbcTemplate.query(sql,new Object[] {cfmsId},new LabelValueBeanMapper());
	}

	@Override
	public List<Map<String, Object>> getPostionsInOrgIdbycfmsId(String cfmsId) throws DAOException{	
		String sql="select position_id,position_name,a.cfmsid,name from org_unit_position_map as a" + 
				" left join hcm_memp as b on a.cfmsid=b.cfmsid" + 
				" where org_id::numeric in(" + 
				"select org_unit_id from org_unit_position_map where cfmsid=?) and a.cfmsid is not null and name is not null";
		return jdbcTemplate.queryForList(sql,new Object[] {Long.parseLong(cfmsId)});
		
	}

	@Override
	public List<LabelValueBean> getHoaByDDO(String ddocode) throws DAOException{
		String sql = "select hoa as label,hoa as value from zgoap_ddo_hoa where ddocode=?";
		return jdbcTemplate.query(sql,new Object[] {ddocode},new LabelValueBeanMapper());
	}
	
	
	@Override
	public boolean saveSubmissionWorkFlow(Map<String, Object> map) throws DAOException {
		
		boolean flag=false;
		try {
			
		if(commonDAO.genericInsertWithColumns(map, DatabaseTables.expenditure_workflow_bill_submission)>0) {	
			flag=true;	
		}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return flag;
	}
	
	@Override
	public List<Map<String, Object>> getWorflowBillSubmissionMaker() throws DAOException{
		String sql="select a.position_id,String_agg(distinct(a.org_id::varchar),',')"
				+ " as org,String_agg(hoa,',') as hoa,position_name,name,b.cfmsid,to_char(start_date,'dd/mm/yyyy') as startdate," + 
				" to_char(end_date,'dd/mm/yyyy') as endate from  expenditure_workflow_bill_submission as a" + 
				" left join org_unit_position_map as b on a.position_id=b.position_id" + 
				" left join hcm_memp as c on c.cfmsid=b.cfmsid" + 
				" group by a.position_id,position_name,name,b.cfmsid,start_date, end_date";
		return jdbcTemplate.queryForList(sql);
		}
	
}
