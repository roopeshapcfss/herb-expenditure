package com.apcfss.DAO;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.apcfss.beanmappers.LabelValueBeanMapper;
import com.apcfss.exception.DAOException;
import com.apcfss.model.ExpenditureBillPSAuditAndWorks;
import com.apcfss.model.LabelValueBean;
import com.apcfss.utils.CommonFunctionUtils;
import com.apcfss.utils.DatabaseTables;

@Repository("billAuditDao")
public class BillSubmissionPSAuditAndWorksDaoImpl implements BillSubmissionPSAuditAndWorksDao {

	JdbcTemplate jdbcTemplate;

	@Autowired
	CommonDao commonDAO;

	@Autowired
	CommonUtilDAO commonUtilDAO;

	@Autowired
	CommonFunctionUtils commonutils;

	@Autowired
	CommonDao commonDao;

	@Autowired
	public BillSubmissionPSAuditAndWorksDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<LabelValueBean> getDDOCodesByCfmsId(Long cfmsId) throws DAOException {

		String sql = "select a.ddocode as value, description_long || '( '|| a.ddocode || ' )' as label from org_unit_ddocode_map as a"
				+ " left join ddo_mst as b on a.ddocode=b.ddocode" + " where org_id::numeric in("
				+ " select org_unit_id from org_unit_position_map where cfmsid= ?) and service_type='01'"
				+ " group by a.ddocode,a.ddocode,description_long";
		return jdbcTemplate.query(sql, new Object[] { cfmsId }, new LabelValueBeanMapper());
	}

	public List<Map<String, Object>> getPositionListByCfmsId(String cfmsId) throws DAOException {
		String sql = "select position_id,position_name,a.cfmsid,name from org_unit_position_map as a"
				+ " left join hcm_memp as b on a.cfmsid=b.cfmsid" + " where org_id::numeric in("
				+ "select org_unit_id from org_unit_position_map where cfmsid=?) and a.cfmsid is not null and name is not null";
		return jdbcTemplate.queryForList(sql, new Object[] { Long.parseLong(cfmsId) });

	}

	@Override
	public boolean saveBillSubmissionAudit(ExpenditureBillPSAuditAndWorks billPsAuditBean, HttpServletRequest request) throws Exception 
	{
		boolean status = false;
		Map<String, Object> map = commonUtilDAO.pojotoMapConverter(billPsAuditBean);
		map.put("ip_address", request.getRemoteAddr());
		if (commonDAO.genericInsertWithColumns(map, DatabaseTables.audit_work) > 0) {
			status = true;
		}
		return status;
	}

}
