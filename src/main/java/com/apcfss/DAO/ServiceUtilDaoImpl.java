package com.apcfss.DAO;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Repository;

import com.apcfss.exception.DAOException;

/*import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.anu.spring.exception.DAOException;
import com.anu.spring.forms.LoginForm;
import com.anu.spring.model.BatchType;
import com.anu.spring.model.CourseTypeList;
import com.anu.spring.model.EligliblitSubjects;
import com.anu.spring.model.EligliblityDegree;
import com.anu.spring.model.ExamTimings;
import com.anu.spring.model.ReasonsForPendingList;
import com.anu.spring.model.ReasonsToRejectList;
import com.anu.spring.model.StateCodes;
import com.anu.spring.pojo.CourseData;
@Repository*/

@Repository
public class ServiceUtilDaoImpl  implements ServiceUtilDao{

	@Override
	public String md5Encrypt(String password) throws DAOException {
		// TODO Auto-generated method stub
		String md5Password = null;
		try {
			MessageDigest md=MessageDigest.getInstance("MD5");
			byte[] messageDigest=md.digest(password.getBytes());
			BigInteger number=new BigInteger(1,messageDigest);
			md5Password=number.toString(16);
			while(md5Password.length()<32)
			{
				md5Password="0"+md5Password;
			}
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return md5Password;
	}
	
	
	
	/*
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<CourseTypeList> getCourseType() throws DAOException {
		List<CourseTypeList> courseType = new ArrayList<CourseTypeList>();
		Session session = null;
			try
			{
				session = sessionFactory.openSession();
				//String hql = "from CourseTypeList where isActive='Y'  order by courseTypeCode ";
				String hql = "from CourseTypeList where isActive='Y' and courseTypeCode not in (3, 4) order by courseTypeCode ";
				Query query = session.createQuery(hql);
				courseType = query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return courseType;
		
	}
	@Override
	public List getCoursesList() throws DAOException {
		List coursesList = new ArrayList();
		Session session = null;
		try
		{
			session = sessionFactory.openSession();
			String sql = "select course_id,case when length(course_code)<=1 then '0'||''||course_code||'-'||course_name else course_code||'-'||course_name end  as course_name  from anu_mst_course where is_active='Y' and is_exists='Y'   order by cast(course_code as int)";
			SQLQuery query = session.createSQLQuery(sql);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List coursesData = query.list();
			for(Object object : coursesData) {
	            Map row = (Map)object;
	            CourseData course = new CourseData();
	            course.setCourseId((Integer)row.get("course_id"));
	            course.setCourseName((String) row.get("course_name"));
	            coursesList.add(course);
	         }
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new DAOException();
		}
		finally
		{
			session.close();
		}
		return coursesList;
		
	}
	


	@Override
	public List<StateCodes> getStates() throws DAOException {
		List<StateCodes> stateCodesList = new ArrayList<StateCodes>();
		Session session = null;
		try
		{
			session = sessionFactory.openSession();
			String hql = "from StateCodes order by lscStateCode ";
			Query query = session.createQuery(hql);
			stateCodesList = query.list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new DAOException();
		}
		finally
		{
			session.close();
		}
		return stateCodesList;
	

	}



	//Generate md5 Password
		@Override
		public String md5Encrypt(String password) throws DAOException {
			// TODO Auto-generated method stub
			String md5Password = null;
			try {
				MessageDigest md=MessageDigest.getInstance("MD5");
				byte[] messageDigest=md.digest(password.getBytes());
				BigInteger number=new BigInteger(1,messageDigest);
				md5Password=number.toString(16);
				while(md5Password.length()<32)
				{
					md5Password="0"+md5Password;
				}
				
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return md5Password;
		}
		@Override
		public List getCorseApplied(String courseType) throws DAOException {
			List courseAppliedList = new ArrayList();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				//String sql = "select course_id,course_name from anu_mst_course where course_type='"+courseType+"'  and is_active='Y' and is_exists='Y' order by course_name";
				String sql = "select course_id, course_code||'-'||course_name as course_name from anu_mst_course where course_type='"+courseType+"' and is_active='Y' and is_exists='Y' order by course_name";
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List courseAppliedData = query.list();
				for(Object object : courseAppliedData) {
		            Map row = (Map)object;
		            CourseData course = new CourseData();
		            course.setCourseId((Integer) row.get("course_id"));
		            course.setCourseName((String) row.get("course_name"));
		            courseAppliedList.add(course);
		         }
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return courseAppliedList;
		}
		@Override
		public List getMediumNames(int courseId) throws DAOException {
			List mediumNamesList = new ArrayList();
			Session session = null;
			String sql1 = null;
			SQLQuery sqlQuery2 = null;
			CourseData course1 = null;
			CourseData course2 = null;
			try
			{
				session = sessionFactory.openSession();
				String sql = "select medium, (case when medium='T' then 'Telugu' when medium='E' then 'English' when medium='H' then 'Hindi' " +
							" when medium='TE' then 'Telugu' else '-' end) as medium_name  from anu_mst_course where course_id="+courseId;
				String sql = "select medium, (case when medium='T' then 'Telugu' when medium='E' then 'English' when medium='H' then 'Hindi' end" +
						" ) as medium_name  from anu_mst_course where course_id="+courseId;
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List courseAppliedData = query.list();
				for(Object object : courseAppliedData) {
					Map row = (Map)object;
					if("TE".equals((String) row.get("medium")))
					{
						sql1=" select (case when medium='TE' then 'E' else '-' end) as medium, (case when medium='TE' then 'English' else '-' end) as medium_name,  " +
								" (case when medium='TE' then 'T' else '-' end) as medium1, (case when medium='TE' then 'Telugu' else '-' end) as medium_name2 from anu_mst_course where course_id="+courseId;
						sqlQuery2 = session.createSQLQuery(sql1);
						sqlQuery2.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
						List teList = sqlQuery2.list();
						for(Object obj : teList) {
							Map row1 = (Map)obj;
							course1 = new CourseData();
							course1.setMedium((String) row1.get("medium"));
							course1.setMediumName((String) row1.get("medium_name"));
							course2 = new CourseData();
							course2.setMedium((String) row1.get("medium1"));
							course2.setMediumName((String) row1.get("medium_name2"));
							
						}
						mediumNamesList.add(course1);
						mediumNamesList.add(course2);
					}
					else
					{
						CourseData course = new CourseData();
						course.setMedium((String) row.get("medium"));
						course.setMediumName((String) row.get("medium_name"));
						mediumNamesList.add(course);
					}
					if("TE".equals(course.getMedium()))
					{
						sql1=" select (case when medium='TE' then 'T' else '-' end) as medium, (case when medium='TE' then 'Telugu' else '-' end) as medium_name  from anu_mst_course where course_id="+courseId;
						sqlQuery2 = session.createSQLQuery(sql1);
						sqlQuery2.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
						List teList = sqlQuery2.list();
						for(Object obj : teList) {
							Map row1 = (Map)obj;
							course2 = new CourseData();
							course2.setMedium((String) row1.get("medium"));
							course2.setMediumName((String) row1.get("medium_name"));
						}
						mediumNamesList.add(course2);
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return mediumNamesList;
		}
		@Override
		public List getDistrictsList(int stateCode) throws DAOException {
			List districtsList = new ArrayList();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String sql = "select district_code,district_name from district_mst where state_code = "+ stateCode;
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List districtData = query.list();
				for(Object object : districtData) {
					Map row = (Map)object;
					CourseData district = new CourseData();
					district.setLscDistrict((Integer)row.get("district_code"));
					district.setLscDistrictName((String) row.get("district_name"));
					districtsList.add(district);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return districtsList;
		}
		@Override
		public List getLearnerSupportCentersList(int stateCode,
				int districtCode, int courseId) throws DAOException {
			List learnerSupportCentersList = new ArrayList();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String sql="select alm.lsc_id, alm.lsc_code||'-'||lsc_name as lsc_name from anu_lsc_master alm join anu_lsc_courses_offered amc on(alm.lsc_id=amc.lsc_id)" +
						" where state='"+stateCode+"' and district='"+districtCode+"' and amc.course_id="+courseId;
				//String sql="select alm.lsc_code, lsc_name from anu_lsc_master alm ";
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List learnerSupportCentersData = query.list();
				for(Object object : learnerSupportCentersData) {
					Map row = (Map)object;
					CourseData learnerSupportCenters = new CourseData();
					learnerSupportCenters.setLscId(((Integer) row.get("lsc_id")));
					learnerSupportCenters.setStudyCenterName((String) row.get("lsc_name"));
					learnerSupportCentersList.add(learnerSupportCenters);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return learnerSupportCentersList;
		}
		@Override
		public List getQualifyingExamsList()
				throws DAOException {
			List<EligliblityDegree> qualifyingExamsList = new ArrayList<EligliblityDegree>();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String hql = "from EligliblityDegree where course_type not in (3,2,99) order by degreeId ";
				Query query = session.createQuery(hql);
				qualifyingExamsList = query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return qualifyingExamsList;

		}
		
		@Override
		public List getQualifyingExamSubjectsList() throws DAOException {
		
			List<EligliblitSubjects> qualifyingExamSubjectsList = new ArrayList<EligliblitSubjects>();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String hql = "from EligliblitSubjects order by subjectId ";
				Query query = session.createQuery(hql);
				qualifyingExamSubjectsList = query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return qualifyingExamSubjectsList;

		}
			
		@Override
		public String getRoleIdBasedOnUserId(String userId) throws DAOException {
			String roleName = null;
			String sql = null;
			SQLQuery sqlQuery = null;
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				sql = "select rm.role_name from login_users u join roles_mst rm on(rm.role_id=u.role_id)  where user_id='"+userId+"'";
				sqlQuery = session.createSQLQuery(sql);
				roleName = (String) sqlQuery.uniqueResult();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return roleName;
		}	
		
		
		@Override
		public String generatePassword(Session session) throws DAOException {
			String password=null;
			String sql = null;
			SQLQuery sqlQuery = null;
			try
			{
				sql = "select trim(array_to_string(array(select substr('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@!#$*'," +
						" (cast((random()*(41-1)+1) as integer)),1) from generate_series(1,6)),'')) as generated_password";
				sqlQuery = session.createSQLQuery(sql);
				password = (String) sqlQuery.uniqueResult();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			
			return password;
		}	
		
		
		@Override
		public List<ReasonsToRejectList> getRejectReasons() throws DAOException {
			List<ReasonsToRejectList> rejectReasons = new ArrayList<ReasonsToRejectList>();
			Session session = null;
				try
				{
					session = sessionFactory.openSession();
					String hql = "from ReasonsToRejectList ";
					Query query = session.createQuery(hql);
					rejectReasons = query.list();
				}
				catch(Exception e)
				{
					e.printStackTrace();
					throw new DAOException();
				}
				finally
				{
					session.close();
				}
				return rejectReasons;
			
		}
		
		@Override
		public List<ReasonsForPendingList> getPendingReasons() throws DAOException {
			List<ReasonsForPendingList> pendingReasons = new ArrayList<ReasonsForPendingList>();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String hql = "from ReasonsForPendingList ";
				Query query = session.createQuery(hql);
				pendingReasons = query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return pendingReasons;
			
		}
		@Override
		public List getstudentReportByCourseWiseList() throws DAOException {
			String sql = null;
			SQLQuery sqlQuery = null;
			List list=null;
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				
				sql = "select srd.course_id,course_code as course_code,course_name as coursename,count(srd.enrollment_number)as noOfstudents from anu_mst_course amc"+
                       " left join student_registration_data srd on(srd.course_id=amc.course_id) group by course_name,course_code,srd.course_id order by course_name";
				sqlQuery = session.createSQLQuery(sql);
				sqlQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				 list = sqlQuery.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return list;
		}
		@Override
		public List getstudentsList(String courseId ) throws DAOException {
			String sql = null;
			SQLQuery sqlQuery = null;
			List list=null;
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				
				sql="select amc.course_name,srd.course_id,srd.registration_number,srd.enrollment_number,srd.student_name,srd.mobile_number" +
			        " from student_registration_data srd join anu_mst_course amc on (amc.course_id=srd.course_id) where amc.course_id='"+courseId+"'" +
			        " and srd.enrollment_number is not null";
				
				sqlQuery = session.createSQLQuery(sql);
				sqlQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				 list = sqlQuery.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return list;
		}
		
		@Override
		public List<BatchType> getBatchType() throws DAOException {
			
			
			List<BatchType> batchType = new ArrayList<BatchType>();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String hql = "from BatchType order by batchTypeCode ";
				Query query = session.createQuery(hql);
				batchType = query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return batchType;
		

		}
		@Override
		public List<ExamTimings> getExamTimings() throws DAOException {
			List<ExamTimings> examTimings = new ArrayList<ExamTimings>();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String hql = "from ExamTimings order by examTimeId ";
				Query query = session.createQuery(hql);
				examTimings = query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return examTimings;
		
		}
		@Override
		public List getYearsList() throws DAOException {
			List yearsList = null;
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				
				String sql = "select generate_series(1990,  cast(extract(year from now()) as int)  )as year_id, generate_series(1990, cast(extract(year from now())as int)) as year_name order by year_id desc  ";
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				yearsList = query.list();
				
			} 
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return yearsList;
		}
		
		@Override
		public List getUserNameAndPassword(String enrollmentNumer)
				throws DAOException {
			List userList = null;
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String sql = "select user_id, password,phone_no from login_users where user_id = '"+enrollmentNumer+"'";
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				userList = query.list();
			} 
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return userList;
		}
		@Override
		public List<CourseData> getCentersList(int courseId) throws DAOException {
			List centersList = new ArrayList();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String sql="select alco.lsc_id,alm.lsc_code||'-'||alm.lsc_name as lsc_name from anu_lsc_courses_offered alco " +
							" join anu_lsc_master alm on (alco.lsc_id=alm.lsc_id) where  course_id="+courseId+" and is_active = 'Y' and is_approved = 'Y' " +
							" order by alm.lsc_code" ;
				//String sql="select alm.lsc_code, lsc_name from anu_lsc_master alm ";
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List learnerSupportCentersData = query.list();
				for(Object object : learnerSupportCentersData) {
					Map row = (Map)object;
					CourseData learnerSupportCenters = new CourseData();
					learnerSupportCenters.setLscId(((Integer) row.get("lsc_id")));
					learnerSupportCenters.setStudyCenterName((String) row.get("lsc_name"));
					centersList.add(learnerSupportCenters);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return centersList;
		}
		
		@Override
		public List getAllDistrictsList() throws DAOException {
			List allDistrictsList = new ArrayList();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String sql = "select district_code,district_name from district_mst  order by district_name";
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				allDistrictsList= query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return allDistrictsList;
		}
		@Override
		public List getCoursesListforICET(String courseType, String registrationNumber) throws DAOException {
			Session session = null;
			String sql = null;
			SQLQuery sqlQuery = null;
			List coursesList = new ArrayList<CourseData>();
			try
			{
				session = sessionFactory.openSession();
				sql = " select amc.course_id, amc.course_code||'-'||amc.course_name as course_name from student_registration_data srd " +
						" join anu_mst_notification amn on amn.notification_number = srd.notification_number " +
						" join anu_notification_courses anc on anc.notification_id = amn.notification_id " +
						" join anu_mst_course amc on amc.course_id = anc.course_id " +
						" join anu_mst_course_type amct on amct.course_type_code = amc.course_type " +
						" where srd.registration_number = '"+registrationNumber+"' and amct.course_type_code = "+Integer.parseInt(courseType)+"  order by course_name";
				sqlQuery = session.createSQLQuery(sql);
				sqlQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List coursesData = sqlQuery.list();
				for(Object object : coursesData) {
		            Map row = (Map)object;
		            CourseData course = new CourseData();
		            course.setCourseId((Integer)row.get("course_id"));
		            course.setCourseName((String) row.get("course_name"));
		            coursesList.add(course);
		         }
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return coursesList;
		}
		@Override
		public List<CourseTypeList> getCourseTypesForCdeCet()
				throws DAOException {
			List<CourseTypeList> courseType = null;
			Session session = null;
			String courseTypeCode = "3,4,7,8,10";
				try
				{
					session = sessionFactory.openSession();
					//String hql = "from CourseTypeList where isActive='Y' and courseTypeCode in (3,4,7,8,10) order by courseTypeCode ";
					String hql = "from CourseTypeList where isActive= :isActive and courseTypeCode in (:courseTypeCode) order by courseTypeCode ";
					Query query = session.createQuery(hql);
					query.setParameter("isActive", "Y");
					query.setParameter(courseTypeCode, courseTypeCode);
					courseType = query.list();
				}
				catch(Exception e)
				{
					e.printStackTrace();
					throw new DAOException();
				}
				finally
				{
					session.close();
				}
				return courseType;
		}
		@Override
		public List<CourseTypeList> getCourseTypesForIcet() throws DAOException {
			List<CourseTypeList> courseType = null;
			Session session = null;
			String courseTypeCode = "3,4";
				try
				{
					session = sessionFactory.openSession();
					//String hql = "from CourseTypeList where isActive='Y' and courseTypeCode in (3,4) order by courseTypeCode ";
					String hql = "from CourseTypeList where isActive=:isActive and courseTypeCode in (:courseTypeCode) order by courseTypeCode ";
					Query query = session.createQuery(hql);
					query.setParameter(courseTypeCode, courseTypeCode);
					courseType = query.list();
				}
				catch(Exception e)
				{
					e.printStackTrace();
					throw new DAOException();
				}
				finally
				{
					session.close();
				}
				return courseType;
		}
		@Override
		public List getCoursesListForCdeCet(String courseType, String registrationNumber)
				throws DAOException {
			Session session = null;
			String sql = null;
			SQLQuery sqlQuery = null;
			List coursesList = new ArrayList<CourseData>();
			int courseTypeCode = 0;
			try
			{
				courseTypeCode = Integer.parseInt(courseType);
				session = sessionFactory.openSession();
				sql = " select amc.course_id, amc.course_code||'-'||amc.course_name as course_name from student_registration_data srd " +
						" join anu_mst_notification amn on amn.notification_number = srd.notification_number " +
						" join anu_notification_courses anc on anc.notification_id = amn.notification_id " +
						" join anu_mst_course amc on amc.course_id = anc.course_id " +
						" join anu_mst_course_type amct on amct.course_type_code = amc.course_type " +
						" where srd.registration_number = '"+registrationNumber+"' and amct.course_type_code in (3,4,7,8,10) order by course_name";
				
				sql = " select amc.course_id, amc.course_code||'-'||amc.course_name as course_name from student_registration_data srd " +
						" join anu_mst_notification amn on amn.notification_number = srd.notification_number " +
						" join anu_notification_courses anc on anc.notification_id = amn.notification_id " +
						" join anu_mst_course amc on amc.course_id = anc.course_id " +
						" join anu_mst_course_type amct on amct.course_type_code = amc.course_type " +
						" where srd.registration_number = '"+registrationNumber+"' and amct.course_type_code = "+Integer.parseInt(courseType)+" order by course_name";
				
				sql = " select amc.course_id, amc.course_code||'-'||amc.course_name as course_name from student_registration_data srd " +
						" join anu_mst_notification amn on amn.notification_number = srd.notification_number " +
						" join anu_notification_courses anc on anc.notification_id = amn.notification_id " +
						" join anu_mst_course amc on amc.course_id = anc.course_id " +
						" join anu_mst_course_type amct on amct.course_type_code = amc.course_type " +
						" where srd.registration_number = :registrationNumber and amct.course_type_code = :courseTypeCode order by course_name";
				
				sqlQuery = session.createSQLQuery(sql);
				sqlQuery.setParameter(registrationNumber, registrationNumber);
				sqlQuery.setParameter(courseTypeCode, courseTypeCode);
				sqlQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List coursesData = sqlQuery.list();
				for(Object object : coursesData) {
		            Map row = (Map)object;
		            CourseData course = new CourseData();
		            course.setCourseId((Integer)row.get("course_id"));
		            course.setCourseName((String) row.get("course_name"));
		            coursesList.add(course);
		         }
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return coursesList;
		}
		@Override
		public List<CourseTypeList> getCourseTypeForCourseChange()
				throws DAOException {
			List<CourseTypeList> courseType = new ArrayList<CourseTypeList>();
			Session session = null;
				try
				{
					session = sessionFactory.openSession();
					String hql = "from CourseTypeList where isActive='Y' order by courseTypeCode ";
					Query query = session.createQuery(hql);
					courseType = query.list();
				}
				catch(Exception e)
				{
					e.printStackTrace();
					throw new DAOException();
				}
				finally
				{
					session.close();
				}
				return courseType;
		}
		@Override
		public List getCourseApplied(String courseType,
				String registrationNumber) throws DAOException {
			Session session = null;
			String sql = null;
			SQLQuery sqlQuery = null;
			List coursesList = new ArrayList<CourseData>();
			try
			{
				session = sessionFactory.openSession();
				sql = " select amc.course_id, amc.course_code||'-'||amc.course_name as course_name from student_registration_data srd " +
						" join anu_mst_notification amn on amn.notification_number = srd.notification_number " +
						" join anu_notification_courses anc on anc.notification_id = amn.notification_id " +
						" join anu_mst_course amc on amc.course_id = anc.course_id " +
						" join anu_mst_course_type amct on amct.course_type_code = amc.course_type " +
						" where srd.registration_number = '"+registrationNumber+"' and amct.course_type_code = "+Integer.parseInt(courseType)+" order by course_name";
				sqlQuery = session.createSQLQuery(sql);
				sqlQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List coursesData = sqlQuery.list();
				for(Object object : coursesData) {
		            Map row = (Map)object;
		            CourseData course = new CourseData();
		            course.setCourseId((Integer)row.get("course_id"));
		            course.setCourseName((String) row.get("course_name"));
		            coursesList.add(course);
		         }
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return coursesList;
		}
		@Override
		public List getQualifyingExamsListLsc(String courseType)
				throws DAOException {
			List qualifyingExamsList = new ArrayList();
			Session session = null;
			try
			{
				session = sessionFactory.openSession();
				String sql = "SELECT degree_id,degree_name FROM anu_mst_eligibility_degree WHERE course_type = '"+courseType+"'";
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				qualifyingExamsList= query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return qualifyingExamsList;
		}
		@Override
		public List getQualifyingExamSubjectsListLSC(String courseType) throws DAOException {
			
			List qualifyingExamsSubjectsList = new ArrayList();
			Session session = null;
			String Condition=null;
			try
			{
				session = sessionFactory.openSession();
				if(courseType.equals("1")||courseType.equals("2")||courseType.equals("3")||courseType.equals("4")||courseType.equals("5")||courseType.equals("7")||courseType.equals("11")){
					Condition="where course_type= "+courseType+"";
				}
				else {
					Condition="where course_type not in(2,3,4,5,7,99) and subject_id not in (11)";
				}
				
				String sql = "SELECT subject_id, subject_name FROM  anu_mst_eligibility_degree_subject "+Condition+" ";
				SQLQuery query = session.createSQLQuery(sql);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				qualifyingExamsSubjectsList= query.list();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return qualifyingExamsSubjectsList;
		
			
		}
		@Override
		public List getCourseTypesForExp() throws DAOException {
				List<CourseTypeList> courseType = null;
				Session session = null;
					try
					{
						session = sessionFactory.openSession();
						String hql = "from CourseTypeList where isActive='Y' and courseTypeCode in (3) order by courseTypeCode ";
						Query query = session.createQuery(hql);
						courseType = query.list();
					}
					catch(Exception e)
					{
						e.printStackTrace();
						throw new DAOException();
					}
					finally
					{
						session.close();
					}
					return courseType;
			}
		@Override
		public List getCoursesListforEXP(String courseType,	String registrationNumber) throws DAOException {
			Session session = null;
			String sql = null;
			SQLQuery sqlQuery = null;
			List coursesList = new ArrayList<CourseData>();
			try
			{
				session = sessionFactory.openSession();
				sql = " select amc.course_id, amc.course_code||'-'||amc.course_name as course_name from student_registration_data srd " +
						" join anu_mst_notification amn on amn.notification_number = srd.notification_number " +
						" join anu_notification_courses anc on anc.notification_id = amn.notification_id " +
						" join anu_mst_course amc on amc.course_id = anc.course_id " +
						" join anu_mst_course_type amct on amct.course_type_code = amc.course_type " +
						" where srd.registration_number = '"+registrationNumber+"' and amct.course_type_code in (3,4,7,8,10) order by course_name";
				
				sql = " select amc.course_id, amc.course_code||'-'||amc.course_name as course_name from student_registration_data srd " +
						" join anu_mst_notification amn on amn.notification_number = srd.notification_number " +
						" join anu_notification_courses anc on anc.notification_id = amn.notification_id " +
						" join anu_mst_course amc on amc.course_id = anc.course_id " +
						" join anu_mst_course_type amct on amct.course_type_code = amc.course_type " +
						" where srd.registration_number = '"+registrationNumber+"' and amct.course_type_code = "+Integer.parseInt(courseType)+" order by course_name";
				
				
				sqlQuery = session.createSQLQuery(sql);
				sqlQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List coursesData = sqlQuery.list();
				for(Object object : coursesData) {
		            Map row = (Map)object;
		            CourseData course = new CourseData();
		            course.setCourseId((Integer)row.get("course_id"));
		            course.setCourseName((String) row.get("course_name"));
		            coursesList.add(course);
		         }
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return coursesList;
}
		@Override
		public String checkFeePaidStatus(String registrationNumber)
				throws DAOException {
			String sql = null;
			SQLQuery sqlQuery = null;
			Session session = null;
			String status = null;
			try
			{
				session = sessionFactory.openSession();
				sql = "select case when fee_paid = 'Y' then 'FeePaid' when fee_paid = 'N' then 'NotPaid' end as fee_status  from student_registration_data where registration_number = '"+registrationNumber+"'";
				sqlQuery = session.createSQLQuery(sql);
				status = (String) sqlQuery.uniqueResult();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return status;
		}
		@Override
		public List getAllCourseTypesList() throws DAOException {
			List<CourseTypeList> courseType = new ArrayList<CourseTypeList>();
			Session session = null;
				try
				{
					session = sessionFactory.openSession();
					String hql = "from CourseTypeList where isActive='Y'  order by courseTypeCode ";
					Query query = session.createQuery(hql);
					courseType = query.list();
				}
				catch(Exception e)
				{
					e.printStackTrace();
					throw new DAOException();
				}
				finally
				{
					session.close();
				}
				return courseType;
		}
		
		@Override
		public String getCaptchadb(HttpServletRequest request,LoginForm lf) throws DAOException {
			String captcha_in_db=null;
			Session session = null;

			String sql=null;
			SQLQuery sqlQuery=null;

			try{
				session = sessionFactory.openSession();
				String captcha_en = md5Encrypt(lf.getPasslineNormal()+"");

				sql=" SELECT coalesce(captcha_md5,'') FROM anu_captcha_info where" +
						" captcha_md5=:captcha_md5 and captcha_status= true";
				
				sqlQuery = session.createSQLQuery(sql);
				sqlQuery.setParameter("captcha_md5", captcha_en);
				captcha_in_db = (String) sqlQuery.uniqueResult();

			}catch (Exception e) {
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return captcha_in_db;
		}
		
		@Override
		public String UpdateCaptcha(HttpServletRequest request,LoginForm lf) throws DAOException {
			String status = null;
			int result = 0;
			Session session = null;
			Transaction tx = null;
			String sql = null;
			SQLQuery query = null;
			try
			{

				session = sessionFactory.openSession(); 
				tx = session.beginTransaction();

				sql = "update anu_captcha_info set captcha_status=false ,captcha_entered=:captcha_entered  where captcha=:captcha  AND captcha_status=true";
				query = session.createSQLQuery(sql);
				query.setParameter("captcha_entered", lf.getUserName().toString());
				query.setParameter("captcha", lf.getPasslineNormal().toString());
				result = query.executeUpdate();
				if(result!=0)
				{
					status = "success";
					tx.commit();
				}
				else
				{
					status = "Failure";
					tx.rollback();
				}
			}
			catch(Exception e)
			{
				tx.rollback();
				e.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				session.close();
			}
			return status;
		}
*/}