package com.apcfss.DAO;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.apcfss.exception.DAOException;
import com.apcfss.model.ExpenditureBillPSAuditAndWorks;
import com.apcfss.model.ExpenditureBillSubmissionWorkflow;
import com.apcfss.model.LabelValueBean;

public interface BillSubmissionPSAuditAndWorksDao {

	List<LabelValueBean> getDDOCodesByCfmsId(Long cfmsId) throws DAOException;

	List<Map<String, Object>> getPositionListByCfmsId(String cfmsId) throws DAOException;

	boolean saveBillSubmissionAudit(ExpenditureBillPSAuditAndWorks billPsAuditBean, HttpServletRequest request) throws DAOException, Exception;

}
