package com.apcfss.DAO;

import java.util.List;
import java.util.Map;

import com.apcfss.model.LabelValueBean;

public interface MasterDAO 
{
	public List<Map<String, Object>> getDepartments();
	public List<Map<String, Object>> getSpecialDepartments(String deptCode);
	public List<Map<String, Object>> getSpecialServices(String serviceCode);
	public List<Map<String, Object>> getTreasuryPao(String distCode,String treasuryPaoCode);
	public String getServicesAsJson(String departmentId,Integer challanType) ;
	public String getHOADetailsAsJson(String departmentId,String serviceCode,Integer challanType) ;
	public List<Map<String, Object>> getDistricts(String distCode);
	public String getTreasuryPaoAsJson(String distId) ;
	public String getDDOCodeJson(String treasurypao) ;
	
	public List<LabelValueBean> getDistrictsAsOptions();
	public List<LabelValueBean> getDepartmentsAsOptions();
	
	public String getchallanType(String challan);
	public List<Map<String, Object>> getDistForEscortGuardCharges();
	public String getEmpDetailsJson(String cfmsId) ;
	
	public List<Map<String, Object>>  getServices(String departmentId,Integer challanType) ;
	
	public List<Map<String, Object>>  getChallanStatusDetails(String transactionId) ;
	public List<Map<String, Object>> getPaymentModes(String challanType);
	
	public List<Map<String, Object>> knowDeptList();
}
