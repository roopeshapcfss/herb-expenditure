package com.apcfss.DAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.apcfss.beanmappers.ServiceMapper;
import com.apcfss.beanmappers.WorkFlowMapper;
import com.apcfss.model.ServiceMenu;
import com.apcfss.model.WorkFlowMenu;
import com.apcfss.service.ServiceUtil;
import com.apcfss.utils.CommonFunctionUtils;

@Repository("loginDAO")
public class LoginDaoImpl implements LoginDao 
{
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	CommonFunctionUtils commonutils;
	
	@Autowired
	public LoginDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	@Autowired
	CommonDao commonDAO;

	@Autowired
	ServiceUtil serviceUtil;
	

	/* Method  for inserting  user login Tracking Details ----START */

@Override
public String insertUserLogDetails(String username, HttpServletRequest request) throws Exception {
	// TODO Auto-generated method stub
	Map<String, Object> userlogMap=null;
	int count=0;
	CommonFunctionUtils commonFunctionUtils=new CommonFunctionUtils();
	try {
	userlogMap=new LinkedHashMap<String, Object>();
	userlogMap.put("user_id", username);
	userlogMap.put("ip_address", request.getRemoteAddr());
	userlogMap.put("is_user_loggedin", "Y");
	count=commonDAO.genericInsertWithColumns(userlogMap, "user_log_info"); 
	if(count>0) {
		List<Object> userLogInfoList=new ArrayList<Object>();
		Date currentdate=commonFunctionUtils.formatDate(getCurrentDate(), "dd/mm/yyyy");
		userLogInfoList.add(currentdate);
		userLogInfoList.add(getCurrentTime());
		userLogInfoList.add(username);
   		String sql = "update user02 set last_log_date=?,last_log_time=? where userid=?";
		    count=jdbcTemplate.update (sql, userLogInfoList.toArray());
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
/* Method  for inserting  user login Tracking Details ----END */

/* Method  for User Validation  Details ----START */
public String  checkLogginDetails(String username, String password) throws Exception 
{
	String msg="";
	
	String sql="select count(userid) from user02 where userid = ?";
	String sql1="select count(userid) from user02 where userid = ? and iniit_password=?";
	String sql2="select count(*) from user02 where userid = ? and iniit_password = ? " + 
			"and current_date between valid_from and valid_to";
	int count=jdbcTemplate.queryForObject(sql, new Object[]{username} ,Integer.class);
	int count1=jdbcTemplate.queryForObject(sql1, new Object[]{username,password} ,Integer.class);
	int count2=jdbcTemplate.queryForObject(sql2, new Object[]{username,password} ,Integer.class);
	if(count==1)
	{
		if(count1==1) {
			
			if(count2==1) {
				
			}else {
				msg="User has been blocked.Please contact Administration.";
			}
			
		}else {
			msg="Please enter valid Password.";
		}
	}else {
		
		msg="Please enter valid User Name.";
	}
	return msg;	
}
/* Method  for User Validation  Details ----END */

public String getCurrentDate() {
    String currentdate = "";
    try {
        final String sql = "select to_char(current_date,'dd/mm/yyyy')";
        currentdate = (String)this.jdbcTemplate.queryForObject(sql, (Class)String.class);
    }
    catch (Exception e) {
        e.printStackTrace();
    }
    return currentdate;
}
public String getCurrentTime() {
    String currenttimestamp = "";
    try {
        final String sql = "select CURRENT_TIME";
        currenttimestamp = (String)this.jdbcTemplate.queryForObject(sql, (Class)String.class);
    }
    catch (Exception e) {
        e.printStackTrace();
    }
    return currenttimestamp;
}
/* Method  for User Created  DAte details ----START */

public boolean checkCreatedDate(String username, String password) throws Exception 
{
	boolean flag=false;
	  String sql1="select  current_date - created_on  from user02 where userid = ? and iniit_password = ? "+
	  " and current_date between valid_from and valid_to"; 
	  int count1=jdbcTemplate.queryForObject(sql1, new Object[]{username,password} ,Integer.class);
	if(count1<=90)
	{
		flag=true;	
	}
	return flag;	
}
/* Method  for User Created  DAte details ----END */

/* Method  for Displaying parent Services ----START */
public List<ServiceMenu>  getParentServices() 
{ 
	String parentId="0";
String	sql = " select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst where parent_id=? and is_active=true order by parent_id, display_order ";
return jdbcTemplate.query(sql,new Object[]{Integer.parseInt(parentId)} , new ServiceMapper() );
}
/* Method  for Displaying parent Services ----END */

/* Method  for Displaying child Services based on moduleType----START */
public List<ServiceMenu>  getServicesbyParentId(String moduleType) 
{ 
String	sql = " select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst where module_type=? and is_active=true and parent_id!=0 order by  display_order ";
return jdbcTemplate.query(sql,new Object[]{moduleType} , new ServiceMapper() );
}
/*  Method  for Displaying child Services based on moduleType ----END */


/* Method  for Displaying ALL Services based on moduleType----START */
@Override
public List<ServiceMenu> getAllServices() {
	String parentId="0";
	String	sql = " select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst where parent_id!=? and is_active=true order by parent_id, display_order ";
	return jdbcTemplate.query(sql,new Object[]{Integer.parseInt(parentId)} , new ServiceMapper() );
}
/* Method  for Displaying ALL Services based on moduleType----END */

public List<ServiceMenu> getReportServices() {
	String parentId="0";
	String	sql = " select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst where parent_id!=? and module_type=? and is_active=true order by parent_id, display_order ";
	return jdbcTemplate.query(sql,new Object[]{Integer.parseInt(parentId),"R"} , new ServiceMapper() );
}

public List<ServiceMenu> getBudgetServices(String cfmsId) {
	String parentId="0";
	//String	sql = " select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst where parent_id!=? and module_type=? and is_active=true order by parent_id, display_order ";
	
	String sql="\r\n" + 
			"(select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst \r\n" + 
			"where parent_id!=? and  module_type=? and is_active=true and service_id not in(20,4)  order by parent_id, display_order)\r\n" + 
			"union all \r\n" + 
			"(select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst \r\n" + 
			"where   parent_id!=? and module_type=? and is_active=true and service_id=20  and ( select COUNT(*)  from org_unit_position_map WHERE  cfmsid=?\r\n" + 
			"AND position_id  IN(\r\n" + 
			"select position_id from budget_workflow_fmu where useraction='M') ) >=1 order by parent_id, display_order) "
			+ "union all \r\n" + 
			"\r\n" + 
			"(select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst \r\n" + 
			"where   module_type=? and is_active=true and service_id=4  and (select count(*) from org_unit_position_map where sub_type='012'  and cfmsid=? ) >=1 order by parent_id, display_order)"
			+ ""
			+ " ";
	
	return jdbcTemplate.query(sql,new Object[]{Integer.parseInt(parentId),"B",Integer.parseInt(parentId),"B",Integer.parseInt(cfmsId),"B",Integer.parseInt(cfmsId) } , new ServiceMapper() );
}


public List<ServiceMenu> getExpenditureServices() {
	String parentId="0";
	String	sql = " select service_name as serviceName,module_type as moduleType,service_id as serviceId,target as target from services_mst where parent_id!=? and module_type=? and is_active=true order by parent_id, display_order ";
	return jdbcTemplate.query(sql,new Object[]{Integer.parseInt(parentId),"E"} , new ServiceMapper() );
}
public List<WorkFlowMenu> getWorkFlowDetails(String userName) {
	
	String sql="select ab.process as process,bp.process_name,ab.org_id as org_id,org_name,'' as task_id,'' as task_hod_name,ddocode as hodcode,description as description_long,  " + 
			"(case when bw.workflow_avl=2 then 'Completed' else 'Pending' end) workflowstatus  " + 
			"from  " + 
			"(  " + 
			"select (case when length(b.ddocode)=11 then 8812 when length(b.ddocode)=9 then 7150 when length(b.ddocode)=5 then 3000 end) as process,  " + 
			"a.org_unit_id as org_id,d.org_name,a.position_id,a.position_name,a.cfmsid,b.ddocode,coalesce(dm.description_long,dd.description_long) as description  " + 
			"from org_unit_position_map a  " + 
			"inner join org_unit_ddocode_map b on(a.org_unit_id=b.org_unit_id)  " + 
			"inner join org_unit_mst d on(d.org_unit_id=a.org_unit_id)  " + 
			"left join ddo_mst dm on(dm.ddocode=b.ddocode)  " + 
			"left join dept_mst dd on(dd.deptcode=b.ddocode)  " + 
			"where a.cfmsid=? and a.sub_type='012' and b.service_type in('01','04')  " + 
			") ab  " + 
			"left join (  " + 
			"select process,org_id,  " + 
			"sum(case when useraction='M' and position_id is not null and end_date=to_date('31/03/9999','dd/mm/yyyy') then 1 else 0 end)+sum(case when useraction='A' and position_id is not null and end_date=to_date('31/03/9999','dd/mm/yyyy') then 1 else 0 end) as workflow_avl  " + 
			" from budget_workflow_dept group by 1,2  " + 
			") bw on( bw.process=ab.process and ab.org_id=bw.org_id )  " + 
			"inner join budget_process_codes_mst bp on(bp.process_id=ab.process)  " + 
			"order by 1,6";
	System.out.println("userName----!!!!!!!!!!!!!!!!------@@@@@@@@@-----"+sql);
	return jdbcTemplate.query(sql,new Object[] {Integer.parseInt(userName)},new WorkFlowMapper() );
}
public List<WorkFlowMenu> getWorkFlowDetailsforFMU(String userName) {
	String sql="select a.process,b.process_name,a.org_id,om.org_name,a.task_id,tm.task_hod_name,a.hodcode,dm.description_long,  " + 
			"(case when (bw.maker+bw.approver)=2 then 'Completed' else 'Pending' end) workflowstatus  " + 
			"from budget_process_task_map a   " + 
			"inner join budget_process_codes_mst b on(a.process=b.process_id)  " + 
			"inner join dept_mst dm on(a.hodcode=dm.deptcode)  " + 
			"inner join org_unit_mst om on(om.org_unit_id=a.org_id)   " + 
			"left join budget_tasks_mst tm on(a.task_id=tm.task_id)  " + 
			"left join (  " + 
			"select process,org_id,task_id,sum(case when useraction='M' then 1 else 0 end) as maker,  " + 
			"			sum(case when useraction='A' then 1 else 0 end) as approver from budget_workflow_fmu group by 1,2,3 )  " + 
			"			 bw on(bw.process=a.process and a.org_id=bw.org_id and a.task_id=bw.task_id)  " + 
			"where a.org_id=20000027  " + 
			"union   " + 
			"select ab.process as process,bp.process_name,ab.org_id as org_id,org_name, 0 as task_id,'' atask_hod_name,ddocode as hodcode,description as description_long,     " + 
			"			(case when bw.workflow_avl=2 then 'Completed' else 'Pending' end) workflowstatus     " + 
			"			from     " + 
			"			(     " + 
			"			select (case when length(b.ddocode)=11 then 8812 when length(b.ddocode)=9 then 7150 when length(b.ddocode)=5 then 3000 end) as process,     " + 
			"			a.org_unit_id as org_id,d.org_name,a.position_id,a.position_name,a.cfmsid,b.ddocode,coalesce(dm.description_long,dd.description_long) as description     " + 
			"			from org_unit_position_map a     " + 
			"			inner join org_unit_ddocode_map b on(a.org_unit_id=b.org_unit_id)     " + 
			"			inner join org_unit_mst d on(d.org_unit_id=a.org_unit_id)     " + 
			"			left join ddo_mst dm on(dm.ddocode=b.ddocode)     " + 
			"			left join dept_mst dd on(dd.deptcode=b.ddocode)     " + 
			"			where a.cfmsid=? and a.sub_type='012' and b.service_type in('01','04') and a.org_unit_id<>20000027  " + 
			"			) ab     " + 
			"			left join (     " + 
			"			select process,org_id,     " + 
			"			sum(case when useraction='M' and position_id is not null and end_date=to_date('31/03/9999','dd/mm/yyyy') then 1 else 0 end)+sum(case when useraction='A' and position_id is not null and end_date=to_date('31/03/9999','dd/mm/yyyy') then 1 else 0 end) as workflow_avl     " + 
			"			 from budget_workflow_fmu group by 1,2     " + 
			"			) bw on( bw.process=ab.process and ab.org_id=bw.org_id )     " + 
			"			inner join budget_process_codes_mst bp on(bp.process_id=ab.process)     " + 
			"			order by 1,6";
	
	
	System.out.println("userName----------@@@@@@@@@-----"+sql);
	return jdbcTemplate.query(sql,new Object[] {Integer.parseInt(userName)},new WorkFlowMapper() );
}
public int  getUserType(String username) 
{
	String sql="";
	sql="select count(*) from org_unit_position_map where sub_type='012'  and cfmsid=?";
	int count=jdbcTemplate.queryForObject(sql, new Object[]{Integer.parseInt(username)} ,Integer.class);
	return count;
}
/**
 * To verify the User belings to finance secretariat or not
 */
public String getTypeofUser(String username) {
	String sql="";
	String sql1="";
	String org="";
	sql1="select count(*) from org_unit_position_map where  cfmsid=? and org_unit_id=20000027";
	int count=jdbcTemplate.queryForObject(sql1, new Object[]{Integer.parseInt(username)} ,Integer.class);
	if(count>0) {
	sql="select distinct org_unit_id from org_unit_position_map where cfmsid=? and org_unit_id=20000027";
	org=jdbcTemplate.queryForObject(sql, new Object[]{Integer.parseInt(username)} ,String.class);
	}else {
		org=null;
	}
	return org;
}
public List<Map<String, Object>> getBudgetTransactionDetails(String userName) {
	String sql="select wrno,estimates,case when status='P' then 'Pending' else 'P' end as status,next_officer_cfmsid,deptcode  from budget_cbro_distribution_trn where year=? and next_officer_cfmsid=?";
	return jdbcTemplate.queryForList(sql, new Object[] { "2021",Integer.parseInt(userName)});
}
 

}
