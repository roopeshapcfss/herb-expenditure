package com.apcfss.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apcfss.DAO.LoginDao;
@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	private LoginDao loginDao;

	@Override
	public String getTypeofUser(String username) {
		return loginDao.getTypeofUser(username);
	}

	/*@Override
	public List<UserInformation> validateUser(String userName, String password, HttpServletRequest request)
			throws Exception 
	{
		return loginDao.validateUser(userName, password, request);
	}

	@Override
	public String saveLoginUserInfo(LoginUserInfo loginUserInfo)
			throws Exception {
		return loginDao.saveLoginUserInfo(loginUserInfo);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<AddService> getServices1(int roleId) throws Exception {
		return loginDao.getServices1(roleId);
	}

	@Override
	public String saveLogoutUserInfo(String userName, int serialNumber)
			throws Exception {
		return loginDao.saveLogoutUserInfo(userName, serialNumber);
	}

	@Override
	public String feedbackForm(LoginForm loginForm,HttpServletRequest request)
			throws Exception {
		return loginDao.feedbackForm(loginForm,request);
	}

	@Override
	public String changePassword(LoginForm loginForm, String confirmPassword,String userName ,String oldPassword, HttpServletRequest request)
			throws Exception {
		return loginDao.changePassword(loginForm ,confirmPassword,userName,oldPassword, request);
	}

	@Override
	public List<UserInformation> getNotificationDetails(
			HttpServletRequest request) throws Exception {
		return loginDao.getNotificationDetails(request);
	}

	@Override
	public List<UserInformation> getApplicationStatus(HttpServletRequest request)
			throws Exception {
		return loginDao.getApplicationStatus(request);
	}
	@Override
	public List<AddService> getuserServiceList(ReligionForm religionForm) throws Exception {
		return loginDao.getuserServiceList(religionForm);
	}

	@Override
	public String changeUserName(ReligionForm religionForm,HttpServletRequest request) throws Exception {
		return loginDao.changeUserName(religionForm,request);
	}*/
	/*public String getCaptcha(HttpServletRequest request) throws Exception {
		return loginDao.getCaptcha(request);
	}
*/
	/*@Override
	public String getrefreshCaptcha(HttpServletRequest request,
			String captcha_md5) throws Exception {
		return loginDao.getrefreshCaptcha(request, captcha_md5);
	}

	@Override
	public void checkPassword(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		loginDao.checkPassword(request, response);
		
	}

	@Override
	public String getLastLoginTime(LoginForm loginForm,
			HttpServletRequest request) throws Exception {
		return loginDao.getLastLoginTime(loginForm, request);
	}

	@Override
	public void changePassword(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		loginDao.changePassword(request, response);
		
	}

	@Override
	public String updateLoginFailedFlagStatus(LoginForm loginForm,
			HttpServletRequest request) throws Exception {
		return loginDao.updateLoginFailedFlagStatus(loginForm, request);
	}

	@Override
	public void checkUsername(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		 loginDao.checkUsername(request, response);
		
	}*/
	
	
}
