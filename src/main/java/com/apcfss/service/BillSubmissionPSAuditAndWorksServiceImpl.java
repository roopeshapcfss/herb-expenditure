package com.apcfss.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apcfss.DAO.BillSubmissionPSAuditAndWorksDao;
import com.apcfss.DAO.CommonDao;
import com.apcfss.DAO.CommonUtilDAO;
import com.apcfss.exception.DAOException;
import com.apcfss.model.ExpenditureBillPSAuditAndWorks;
import com.apcfss.model.LabelValueBean;
import com.apcfss.utils.CommonFunctionUtils;
import com.apcfss.utils.DatabaseTables;

@Service("billAuditService")
public class BillSubmissionPSAuditAndWorksServiceImpl implements BillSubmissionPSAuditAndWorksService {
	@Autowired
	BillSubmissionPSAuditAndWorksDao billAuditDao;
	
	@Autowired
	CommonUtilDAO commonUtilDAO ;
	
	@Autowired
	 CommonFunctionUtils commonutils;
	@Autowired
	CommonDao commonDao;
	

	@Override
	public List<LabelValueBean> getDDOCodesByCfmsId(Long cfmsId ) throws Exception {
		// TODO Auto-generated method stub
		return billAuditDao.getDDOCodesByCfmsId(cfmsId);
	}

	@Override
	public List<Map<String, Object>> getPositionListByCfmsId(String cfmsId) throws Exception {
		// TODO Auto-generated method stub
		return billAuditDao.getPositionListByCfmsId(cfmsId);
	}

	@Override
	public boolean saveBillSubmissionAudit(ExpenditureBillPSAuditAndWorks billPsAuditBean, HttpServletRequest request) throws Exception {
		//boolean status=false;
		billPsAuditBean.setCreatedBy(commonutils.getUserNameRequest(request) );
		return billAuditDao.saveBillSubmissionAudit(billPsAuditBean,request);
		//return billAuditDao.saveBillSubmissionAudit(billPsAuditBean,request);
	/*	try {
			Map<String, Object> map= commonUtilDAO.pojotoMapConverter(billPsAuditBean);
			map.put("ip_address"  , request.getRemoteAddr());
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return map;*/
	}
}
