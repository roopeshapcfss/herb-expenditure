package com.apcfss.service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.apcfss.DAO.BillSubmissionWorkFlowDao;
import com.apcfss.DAO.CommonUtilDAO;
import com.apcfss.exception.DAOException;
import com.apcfss.model.ExpenditureBillSubmissionWorkflow;
import com.apcfss.model.LabelValueBean;
import com.apcfss.utils.DatabaseTables;

@Service("billSubmissionWorkFlowservice")
public class BillSubmissionWorkFlowServiceImpl implements BillSubmissionWorkFlowService 
{
	@Autowired
	private PlatformTransactionManager transactionManager;
	
	@Autowired
	BillSubmissionWorkFlowDao billSubmissionWorkFlowDao;
	
	@Autowired
	CommonUtilDAO commonUtilDAO;
	

	@Override
	public List<LabelValueBean> getOrganisationByCfmsId(Long cfmsId) throws Exception {
		return billSubmissionWorkFlowDao.getOrganisationByCfmsId(cfmsId);
	}

	@Override
	public List<LabelValueBean> getDDOCodesByCFfmsId(Long cfmsId) throws Exception {
		return billSubmissionWorkFlowDao.getDDOCodesByCFfmsId(cfmsId);
	}

	@Override
	public List<Map<String, Object>> getPostionsInOrgIdbycfmsId(String cfmsId) throws DAOException {
		return billSubmissionWorkFlowDao.getPostionsInOrgIdbycfmsId(cfmsId);
	}

	@Override
	public List<LabelValueBean> getHoaByDDO(String ddocode) throws Exception {
		return billSubmissionWorkFlowDao.getHoaByDDO( ddocode);
	}

	@Override
	public boolean saveSubmissionWorkFlow(ExpenditureBillSubmissionWorkflow expenditureBillSubmissionWorkflow,
			HttpServletRequest request) throws Exception 
	{
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		boolean flag=false;
		try {
		
			String hoaList[]=expenditureBillSubmissionWorkflow.getHoa().split(",");
			
			String orgList[]=expenditureBillSubmissionWorkflow.getOrgId().split(",");
			
			Map<String, Object> map=null;
			for(String hoa : hoaList)
			{
			for(String org:orgList)
			{
				  map = commonUtilDAO.pojotoMapConverter(expenditureBillSubmissionWorkflow);
					map.put("ip_address", request.getRemoteAddr());
					map.put("start_date", LocalDate.now());
					map.put("end_date", new SimpleDateFormat("dd/MM/yyyy").parse(expenditureBillSubmissionWorkflow.getEndDate()));
					map.put("hoa", hoa);
					map.put("org_id", org);
					map.remove("work_flow_id");
					billSubmissionWorkFlowDao.saveSubmissionWorkFlow(map);	
			}
			
			}
			transactionManager.commit(status);
			flag=true;
		}
		catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(status);
		}
		return flag;
	}

	@Override
	public List<Map<String, Object>> getWorflowBillSubmissionMaker() throws Exception {
		return billSubmissionWorkFlowDao. getWorflowBillSubmissionMaker();
	}

	

}
