package com.apcfss.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.apcfss.model.ExpenditureBillPSAuditAndWorks;
import com.apcfss.model.LabelValueBean;

public interface BillSubmissionPSAuditAndWorksService {


	List<LabelValueBean> getDDOCodesByCfmsId(Long cfmsId) throws Exception;


	List<Map<String, Object>> getPositionListByCfmsId(String cfmsId) throws Exception;


	boolean saveBillSubmissionAudit(ExpenditureBillPSAuditAndWorks billPsAuditBean, HttpServletRequest request) throws Exception;


	

	
}
