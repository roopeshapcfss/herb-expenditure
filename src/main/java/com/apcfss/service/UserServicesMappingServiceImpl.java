package com.apcfss.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.apcfss.DAO.UserServicesMappingDAO;
import com.apcfss.model.LabelValueBean;

public class UserServicesMappingServiceImpl implements UserServicesMappingService  
{
	@Autowired
	UserServicesMappingDAO userServicesMappingDAO; 
	public List<LabelValueBean> getServices(String serviceType) 
	{
	return userServicesMappingDAO.getServices(serviceType);
	}
	
}
