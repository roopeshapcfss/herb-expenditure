package com.apcfss.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.apcfss.exception.DAOException;
import com.apcfss.model.ExpenditureBillSubmissionWorkflow;
import com.apcfss.model.LabelValueBean;


public interface BillSubmissionWorkFlowService {

	List<LabelValueBean> getOrganisationByCfmsId(Long cfmsId) throws Exception;
	
	List<LabelValueBean> getDDOCodesByCFfmsId(Long cfmsId) throws Exception;

	List<Map<String, Object>> getPostionsInOrgIdbycfmsId(String cfmsId) throws DAOException;

	List<LabelValueBean> getHoaByDDO(String ddocode) throws Exception;

	boolean saveSubmissionWorkFlow(ExpenditureBillSubmissionWorkflow expenditureBillSubmissionWorkflow, HttpServletRequest request) throws Exception;
	
	List<Map<String, Object>> getWorflowBillSubmissionMaker() throws Exception;
	
}
