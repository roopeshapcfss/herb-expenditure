package com.apcfss.model;

public class MenuService 
{
String serviceId;
String serviceName;
String serviceUrl;
String parentId;
String levelId;

public String getServiceId() {
	return serviceId;
}
public void setServiceId(String serviceId) {
	this.serviceId = serviceId;
}
public String getServiceName() {
	return serviceName;
}
public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}
public String getServiceUrl() {
	return serviceUrl;
}
public void setServiceUrl(String serviceUrl) {
	this.serviceUrl = serviceUrl;
}
public String getParentId() {
	return parentId;
}
public void setParentId(String parentId) {
	this.parentId = parentId;
}
public String getLevelId() {
	return levelId;
}
public void setLevelId(String levelId) {
	this.levelId = levelId;
}
	
}
