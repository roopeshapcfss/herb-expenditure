package com.apcfss.model;

public class WorkFlowMenu {

	String process =null;
	String org_id =null;
	String position_id =null;
	String job_id =null;
	String task_id =null;
	String level_no =null;
	String process_name=null;
	String org_name=null;
	String task_hod_name=null;
	String hodcode=null;
	String description_long=null;
	String fundcenter=null;
	String workflowstatus=null;
	public String getProcess_name() {
		return process_name;
	}
	public void setProcess_name(String process_name) {
		this.process_name = process_name;
	}
	public String getOrg_name() {
		return org_name;
	}
	public void setOrg_name(String org_name) {
		this.org_name = org_name;
	}
	public String getTask_hod_name() {
		return task_hod_name;
	}
	public void setTask_hod_name(String task_hod_name) {
		this.task_hod_name = task_hod_name;
	}
	public String getHodcode() {
		return hodcode;
	}
	public void setHodcode(String hodcode) {
		this.hodcode = hodcode;
	}
	public String getDescription_long() {
		return description_long;
	}
	public void setDescription_long(String description_long) {
		this.description_long = description_long;
	}
	public String getFundcenter() {
		return fundcenter;
	}
	public void setFundcenter(String fundcenter) {
		this.fundcenter = fundcenter;
	}
	public String getWorkflowstatus() {
		return workflowstatus;
	}
	public void setWorkflowstatus(String workflowstatus) {
		this.workflowstatus = workflowstatus;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getOrg_id() {
		return org_id;
	}
	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}
	public String getPosition_id() {
		return position_id;
	}
	public void setPosition_id(String position_id) {
		this.position_id = position_id;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getLevel_no() {
		return level_no;
	}
	public void setLevel_no(String level_no) {
		this.level_no = level_no;
	}
}
