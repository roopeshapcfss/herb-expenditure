package com.apcfss.model;

public class ServiceMenu {
	String serviceId =null;
	String serviceName =null;
	String serviceDescription =null;
	String target =null;
	String parentId =null;
	String moduleType =null;
	String is_childs =null;
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceDescription() {
		return serviceDescription;
	}
	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getModuleType() {
		return moduleType;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public String getIs_childs() {
		return is_childs;
	}
	public void setIs_childs(String is_childs) {
		this.is_childs = is_childs;
	}
}
