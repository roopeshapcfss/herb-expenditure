package com.apcfss.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Roopesh Reddy Putturu
 *
 */
@Configuration
public class HandlerConfig implements WebMvcConfigurer {
	@Autowired
	ValidateRequest validRequest;

	public void addInterceptors(InterceptorRegistry registry) {
		// registry.addInterceptor(validRequest);
		// .addPathPatterns("/config/**");
		// registry.addInterceptor(productServiceInterceptor).addPathPatterns("/**").excludePathPatterns("/admin/**");
		// multiple urls (same is possible for * `exludePathPatterns`)
		// registry.addInterceptor(productServiceInterceptor).addPathPatterns("/secure/*","/admin/**",
		// "/profile/**");

	}
}