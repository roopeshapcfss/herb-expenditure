//
//package com.apcfss.configurations;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
//import org.springframework.web.servlet.view.tiles3.TilesViewResolver;
//
//@Configuration
//
//@EnableWebMvc
//public class MVCConfigurations extends WebMvcConfigurerAdapter {
//
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("assets/**").addResourceLocations("/assets/");
//		registry.addResourceHandler("js/**").addResourceLocations("/js/");
//		registry.addResourceHandler("css/**").addResourceLocations("/css/");
//		registry.addResourceHandler("template-assets/**").addResourceLocations("/template-assets/");
//		registry.addResourceHandler("images/**").addResourceLocations("/images/");
//		registry.addResourceHandler("html/**").addResourceLocations("/html/");
//	}
//
//	@Override
//	public void configureViewResolvers(ViewResolverRegistry registry) {
//		TilesViewResolver viewResolver = new TilesViewResolver();
//		registry.viewResolver(viewResolver);
//	}
//
//	@Bean
//	public TilesConfigurer tiles() {
//		TilesConfigurer config = new TilesConfigurer();
//		String[] defs = { "WEB-INF/tiles.xml" };
//		config.setCheckRefresh(true);
//		config.setDefinitions(defs);
//		return config;
//	}
//
//}
