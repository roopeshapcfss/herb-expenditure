package com.apcfss.beanmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.apcfss.model.MenuService;
import com.apcfss.model.ServiceMenu;

public class ServiceMapper implements RowMapper{
	
    public  ServiceMenu  mapRow(ResultSet rs, int rowNum) throws SQLException {

    	ServiceMenu servicemenu=new ServiceMenu();
    		servicemenu.setServiceId(rs.getString("serviceId"));
			servicemenu.setServiceName(rs.getString("serviceName"));
			servicemenu.setModuleType(rs.getString("moduleType"));
			servicemenu.setTarget(rs.getString("target"));
		return servicemenu;	
    	
    }

}
