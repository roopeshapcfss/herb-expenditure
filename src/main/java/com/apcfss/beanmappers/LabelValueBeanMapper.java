package com.apcfss.beanmappers;

import java.sql.ResultSet;

import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.apcfss.model.LabelValueBean;

public class   LabelValueBeanMapper implements RowMapper
{
	    public LabelValueBean mapRow(ResultSet rs, int rowNum) throws SQLException {
	    	LabelValueBean labelValueBean = new LabelValueBean();
	    	labelValueBean.setLabel(rs.getString("label"));
	    	labelValueBean.setValue(rs.getString("value"));
	        return labelValueBean;
	    }
	
}
