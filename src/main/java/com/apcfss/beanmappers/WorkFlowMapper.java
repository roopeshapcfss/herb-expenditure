package com.apcfss.beanmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.apcfss.model.ServiceMenu;
import com.apcfss.model.WorkFlowMenu;

public class WorkFlowMapper   implements RowMapper{

		
	    public  WorkFlowMenu  mapRow(ResultSet rs, int rowNum) throws SQLException {

	    	WorkFlowMenu workFlowMenu=new WorkFlowMenu();
	    	workFlowMenu.setProcess(rs.getString("process"));
	    	workFlowMenu.setProcess_name(rs.getString("process_name"));
	    	workFlowMenu.setOrg_id(rs.getString("org_id"));
	    	workFlowMenu.setOrg_name(rs.getString("org_name"));
	    	workFlowMenu.setTask_id(rs.getString("task_id"));
	    	workFlowMenu.setTask_hod_name(rs.getString("task_hod_name"));
	    	workFlowMenu.setHodcode(rs.getString("hodcode"));
	    	workFlowMenu.setDescription_long(rs.getString("description_long"));
	    	workFlowMenu.setWorkflowstatus(rs.getString("workflowstatus"));
			return workFlowMenu;	
	    }

	
}
