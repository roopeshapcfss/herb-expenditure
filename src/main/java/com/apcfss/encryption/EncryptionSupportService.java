package com.apcfss.encryption;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.util.Base64;

/**
 * @author Roopesh Reddy Putturu
 *
 */
public class EncryptionSupportService {

	private static final Logger logger = LoggerFactory.getLogger(EncryptionSupportService.class);

	private static final String characterEncoding = "UTF-8";
	private static final String cipherTransformation = "AES/CBC/PKCS5Padding";
	private static final String aesEncryptionAlgorithm = "AES";

	// decrypt method to decrypt the byte[] text
	public static byte[] decrypt(byte[] cipherText, byte[] key, byte[] initialVector)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance(cipherTransformation);
		SecretKeySpec secretKeySpecy = new SecretKeySpec(key, aesEncryptionAlgorithm);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
		cipherText = cipher.doFinal(cipherText);
		return cipherText;
	}

	// encrypt method to encrypt the byte[] text
	public static byte[] encrypt(byte[] plainText, byte[] key, byte[] initialVector)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance(cipherTransformation);
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, aesEncryptionAlgorithm);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
		plainText = cipher.doFinal(plainText);
		return plainText;
	}

	// generates byte[] for the given String object
	private static byte[] getKeyBytes(String key) throws UnsupportedEncodingException {
		byte[] keyBytes = new byte[16];
		byte[] parameterKeyBytes = key.getBytes(characterEncoding);
		System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
		return keyBytes;
	}

	// Encrypts given String to Base64 encoded String
	public static String encrypt(String plainText, String key) {
		String text = "";
		try {
			byte[] plainTextbytes = plainText.getBytes(characterEncoding);
			byte[] keyBytes = getKeyBytes(key);
			text = Base64.encodeToString(encrypt(plainTextbytes, keyBytes, keyBytes), Base64.DEFAULT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}

	// Decodes given encrypted String after Base64 decoding
	public static String decrypt(String encryptedText, String key) {
		String text = "";
		try {
			byte[] cipheredBytes = Base64.decode(encryptedText, Base64.DEFAULT);
			byte[] keyBytes = getKeyBytes(key);
			text = new String(decrypt(cipheredBytes, keyBytes, keyBytes), characterEncoding);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}

	// Generates Hex value of given input after hashing
	public static String generateKeyHex(String myKey) {

		MessageDigest sha = null;
		SecretKeySpec secretKey = null;
		byte[] key = null;
		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");

			return toHex(key);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Exception handheld ", e);
		} catch (UnsupportedEncodingException e) {
			logger.error("Exception handled in SecretKeySpec() ::" + secretKey, e);
		}
		return "";
	}

	// Converts given byte[] to Hex value
	private static String toHex(byte[] array) {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}

}
