
package com.apcfss.encryption;

/**
 * @author Roopesh Reddy Putturu
 *
 */
public class ConfiguredValues {

	public static String INVALID_REQUEST = "Invalid request";

	public static String KEY_ENCODER = "bn^SZaniWNxiaJE";

	public static String DELIMITER = ":::";

	public static String HASHED_SECRET_KEY = "lskj";

	public static int TIMESTAMP_DIFF = 20; // valid time difference to identify valid packets
}
