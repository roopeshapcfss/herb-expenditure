package com.apcfss.encryption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author Roopesh Reddy Putturu
 *
 */
@Service
public class EncryptionService {

	private static final Logger logger = LoggerFactory.getLogger(EncryptionService.class);

	/**
	 * 
	 * encryption of string
	 * 
	 * @param json
	 * @return
	 */
	public static String encryptRequest(String username) {
		String encryptedString = "";
		try {
			String digest = username + ConfiguredValues.DELIMITER + System.currentTimeMillis()
					+ ConfiguredValues.DELIMITER + EncryptionSupportService.generateKeyHex(username);
			// System.out.println("digest :: " + digest);
			// System.out.println("hasehed key :::" + ConfiguredValues.HASHED_SECRET_KEY);
			encryptedString = EncryptionSupportService.encrypt(digest, ConfiguredValues.HASHED_SECRET_KEY);
		} catch (Exception e) {
			logger.error("failed to encryprt response ::: ", e);
		}
		return encryptedString;
	}

	/**
	 * 
	 * validate the request
	 * 
	 * 
	 * @param request
	 * @return
	 */
	public static boolean validateRequest(String request) {
		String decryptedVal;
		String requestDigest;
		boolean status = false;
		// String status = ConfiguredValues.INVALID_REQUEST;
		String encryptedVal = "";

		try {
			decryptedVal = EncryptionSupportService.decrypt(request, ConfiguredValues.HASHED_SECRET_KEY);
			// check whether obtained decrypted value contains ":" or not
			if (decryptedVal.contains(ConfiguredValues.DELIMITER)) {
				String[] val = decryptedVal.split(ConfiguredValues.DELIMITER);
				String originalText = val[0];
				encryptedVal = val[2];
				long timediff = Math.round(((System.currentTimeMillis() - Long.parseLong(val[1])) / 1000) / 60);
				// check for validating the request successful if the time stamp difference is
				// less than configured value Constants.TIMESTAMP_DIFF
				if (timediff <= ConfiguredValues.TIMESTAMP_DIFF) {
					// generating hash for the request value
					requestDigest = EncryptionSupportService.generateKeyHex(originalText);
					/*
					 * comparing hash value in decrypted string with generated hash in previous step
					 * valid if both values are equal
					 */
					if (requestDigest.equals(encryptedVal)) {
						status = true;
						logger.info(" Valid request");
						System.out.println("originalText :::" + originalText);
					} else {
						logger.info(" Invalid request : Digest differs ");

					}
				} else {
					logger.info(" Invalid request : Request timedout ");

				}
			} else {
				logger.info(" Invalid request : Invalid request format ");

			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return status;
	}

	public static void main(String[] args) {
		String token = EncryptionService.encryptRequest("Roopesh");
		System.out.println("token ::: " + token);
		System.out.println(" The token is " + EncryptionService.validateRequest(token));
	}

}
