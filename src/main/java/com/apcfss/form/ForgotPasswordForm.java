package com.apcfss.form;

public class ForgotPasswordForm {
	private String userName=null;
	private String	confirmNewPassword=null;
	private String newPassword=null;
	//private Integer enterOtp;
	private int otp;
	private String passlineValueEncoded;
	private String passlineNormal;
	private String userNameId=null;
	private String userNameHidden=null;
	private String enterOtpHidden=null;
	private String mobileNumber=null;
	private String mobileNo=null;
	private String enterOtp = null;
	private String genPassword=null;
	private String origpassword=null;
	private String checkpassword=null;
	private String loggedin=null;
	public String getLoggedin() {
		return loggedin;
	}
	public void setLoggedin(String loggedin) {
		this.loggedin = loggedin;
	}
	public String getCheckpassword() {
		return checkpassword;
	}
	public void setCheckpassword(String checkpassword) {
		this.checkpassword = checkpassword;
	}
	public String getOrigpassword() {
		return origpassword;
	}
	public void setOrigpassword(String origpassword) {
		this.origpassword = origpassword;
	}
	public String getGenPassword() {
		return genPassword;
	}
	public void setGenPassword(String genPassword) {
		this.genPassword = genPassword;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}
	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	/*
	public Integer getEnterOtp() {
		return enterOtp;
	}
	public void setEnterOtp(Integer enterOtp) {
		this.enterOtp = enterOtp;
	}
	*/
	public String getPasslineValueEncoded() {
		return passlineValueEncoded;
	}
	public void setPasslineValueEncoded(String passlineValueEncoded) {
		this.passlineValueEncoded = passlineValueEncoded;
	}
	public String getPasslineNormal() {
		return passlineNormal;
	}
	public void setPasslineNormal(String passlineNormal) {
		this.passlineNormal = passlineNormal;
	}
	public String getUserNameId() {
		return userNameId;
	}
	public void setUserNameId(String userNameId) {
		this.userNameId = userNameId;
	}
	public String getUserNameHidden() {
		return userNameHidden;
	}
	public void setUserNameHidden(String userNameHidden) {
		this.userNameHidden = userNameHidden;
	}
	public String getEnterOtpHidden() {
		return enterOtpHidden;
	}
	public void setEnterOtpHidden(String enterOtpHidden) {
		this.enterOtpHidden = enterOtpHidden;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEnterOtp() {
		return enterOtp;
	}
	public void setEnterOtp(String enterOtp) {
		this.enterOtp = enterOtp;
	}
	public int getOtp() {
		return otp;
	}
	public void setOtp(int otp) {
		this.otp = otp;
	}

}
