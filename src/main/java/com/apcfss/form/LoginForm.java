package com.apcfss.form;
import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class LoginForm  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotEmpty(message = "Please enter your username.")
	private String userName;
	/*
	 * @Pattern(regexp="/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-z0-9!@#$%^&*]{7,15}$/",
	 * message="Password should be minimum of 8 Characters,one special character,one numeric digit."
	 * )
	 */  
	private String password;
	private String oldPassword;
	private String newPassword;
	private String confirmPassword;
	private String name;
	private String email;
	private String subject;
	private String message;
	@NotEmpty(message = "Please enter your Captcha.")
	private String passlineNormal;
	private String uniqueId;
	private String type;
	private String username1;
	private String password1;
	private String newpassword;
	private String confirmNewpassword;
	
	private String process;
	private String org_id;
	private String position_id;
	private String job_id;
	private String task_id;
	private String level_no;
	
	
	private String passlineValueEncoded;
	
	public String getPasslineValueEncoded() {
		return passlineValueEncoded;
	}
	public void setPasslineValueEncoded(String passlineValueEncoded) {
		this.passlineValueEncoded = passlineValueEncoded;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPasslineNormal() {
		return passlineNormal;
	}
	public void setPasslineNormal(String passlineNormal) {
		this.passlineNormal = passlineNormal;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getUsername1() {
		return username1;
	}
	public void setUsername1(String username1) {
		this.username1 = username1;
	}
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	public String getNewpassword() {
		return newpassword;
	}
	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}
	public String getConfirmNewpassword() {
		return confirmNewpassword;
	}
	public void setConfirmNewpassword(String confirmNewpassword) {
		this.confirmNewpassword = confirmNewpassword;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getOrg_id() {
		return org_id;
	}
	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}
	public String getPosition_id() {
		return position_id;
	}
	public void setPosition_id(String position_id) {
		this.position_id = position_id;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getLevel_no() {
		return level_no;
	}
	public void setLevel_no(String level_no) {
		this.level_no = level_no;
	}
	

}
