<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <base href="<%=basePath%>">

    <title>Initiate BRO</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">



    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript">


    </script>
    <script src="<%=basePath%>js/billSubmissionWorkFlow.js"></script>
    <%-- <script src="<%=basePath%>js/jquery.sumoselect.min.js"></script>
    <link href="<%=basePath%>css/sumoselect.css" rel="stylesheet" /> --%>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.0.2/sumoselect.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.0.2/jquery.sumoselect.min.js"></script>
    <link rel="stylesheet" href="<%=basePath%>css/datepicker.min.css">
    <script type="text/javascript" src="<%=basePath%>js/bootstrap-datepicker.min.js"></script>

    <style>
        #cover-spin {
            /* position:fixed; */
            width: 100%;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            background-color: rgba(255, 255, 255, 0.7);
            /* z-index:9999; */
            display: none;
        }

        #cover-spin::after {
            display: block;
            position: absolute;
            left: 100%;
            top: 40%;
            width: 100px;
            height: 100px;
            border-style: solid;
            border-color: black;
            border-top-color: transparent;
            border-width: 4px;
            border-radius: 50%;
            -webkit-animation: spin .8s linear infinite;
            animation: spin .8s linear infinite;
        }

        .table-responsive {
            overflow-x: scroll;
        }

      .bg_hash {
		    background-color: #111;
		    width: 100%;
		    padding: 10px;
		    margin-top: 	px;
		    margin-left: inherit;
		}

        .mt5 {
            margin-top: 5px;
        }

        .input-group-text {
            min-width: auto !important;
        }

        .datepicker.dropdown-menu {
            width: auto !important;
        }

        .SumoSelect {
            width: 60%;
        }

        .workflow ul {
            padding: 0;
            margin: 0
        }

        .workflow ul li {
            display: inline-block;
            margin: 17px 14px;
            margin-bottom: 0;
        }

        .workflow ul li.active .workflow_block {
            border-bottom: 3px solid #5e696e;
        }

        .workflow ul li.active .workflow_icon {
            background: #5e696e;
            color: #fff;
            border: 0;
        }

        .workflow ul li.active .workflow_icon i {
            color: #fff;
            font-size: 1.5em;
        }

        .workflow_icon {
            background: transparent;
            border: 1px solid rgba(0, 0, 0, 0.5);
            width: 48px;
            height: 48px;
            border-radius: 50%;
            margin: 0px auto;
        }

        .workflow_icon i {
            margin: 14px;
            color: #5e696e;
            font-size: 1.5em;
        }

        .workflow_title {
            text-align: center;
        }

        .workflow {
            background: #eff4f9;
            border-bottom: 4px solid #d1e0ee
        }

        .workflow_title {
            font-size: 16px;
            padding-top: 6px;
            padding-bottom: 8px;
        }

        .workflow a {
            color: #78797a;
            text-decoration: none;
        }

        .heading_main {
            background: #eff4f9;
            color: #545252;
            font-weight: 600;
            margin-bottom: 0;
            padding: 10px;
            border-bottom: 2px solid #e0f0ff;
        }

        .heading_main p {
            margin-bottom: 0;
        }

        .org_det {
            padding-top: 24px;
            padding-bottom: 24px;
        }

        .org_det span b {
            color: #666;
        }

        .dis_int {
            display: flex;
        }

        input:read-only {
            background-color: #fff !important;
        }

        .text-danger {
            color: #dc3545 !important;
            border: 1px solid;
            margin: -2px;
            padding: 7px;
            border-radius: 6px;
            background-color: #f77b7b21;
        }

        .bg_info {
            background: #eff4f9;
            margin-left: 84px;
        }

        .SumoSelect .select-all {
            border-radius: 3px 3px 0 0;
            position: relative;
            border-bottom: 1px solid #ddd;
            background-color: #fff;
            padding: 8px 0 3px 35px;
            height: 37px;
            cursor: pointer;
            width: 164%;
        }

        .SumoSelect>.CaptionCont {

            width: 164%;
        }

        .SumoSelect>.optWrapper {

            width: 164%;

        }

        .multiselect,
        .form-group .btn-group {
            width: 100% !important;
            border-radius: 0;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example2').DataTable();

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.testselect2').SumoSelect({
                    search: true,
                    searchText: 'Enter here.',
                    selectAll: true
                }
                /* {
		    placeholder: 'Select Here',
		    csvDispCount: 4,
		    captionFormat:'{0} Selected', 
		    captionFormatAllSelected:'{0} all selected!',
		    floatWidth: 1000,
		    forceCustomRendering: true,
		    nativeOnDevice: ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'],
		    outputAsCSV: false,
		    csvSepChar: ',',
		    okCancelInMulti: false,
		    isClickAwayOk: false,
		    triggerChangeCombined: true,
		    selectAll: true,
		    search: true,
		    searchText: 'Search...',
		    noMatch: 'No matches for "{0}"',
		    prefix: '',
		    locale: ['OK', 'Cancel', 'Select All'],
		    up: false,
		    showTitle: true
		} */
            );
        });
    </script>
</head>

<body>
    <form:form method="post" modelAttribute="expenditureBillSubmissionWorkflow" action="./BillSubmission/Save" id="formId">
        <form:hidden path = "positionId" id="positionId"/>
        <form:hidden path = "orgId" id="orgId"/>
        <form:hidden path = "hoa" id="hoaId"/>
        <form:hidden path = "userAction" id="userActionId" value="M"/>

        <div id="cover-spin" style="display:none;margin-top:40px;" class="col-12  col-sm-12 col-md-12 col-lg-12 col-xl-12 overlay">
            <img src='images/loader1.gif' width='180px' height='180px' style="margin-left: 813px;margin-top: 293px;">
        </div>

        <div class="containerLoad">
            <div class="heading_main">
                <p id="heading" style="text-align: center;">Workflow Configurator(Bill Submission) <span class="float-right">14465279</span></p>
            </div>



            <div class="card-body" style="display: none;">
                <div class="wrapper text-center">
                    <button class="btn btn-outline-success" onclick="showSwal('success-message')" id="sweetAlertButton"></button>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xs-12 col-sm-3 col-lg-3 col-xl-3	"></div>
                <div class="col-12 col-xs-12 col-sm-6 col-lg-6 col-xl-6">
           <%--          <form:select path="ddocode" cssClass="form-control mandatoryField">
                        <form:options items="${ddoList}" itemLabel="label" itemValue="value" />
                    </form:select>
            --%>     </div>
            </div>
            <div class="row">
                <div class="col-12  col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="workflow">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="maker"><a href="#maker" data-toggle="tab">
                                    <div class="workflow_block">
                                        <div class="workflow_icon">
                                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="workflow_title">Maker</div>
                                    </div>
                                </a></li>
                            <li class="checker"><a href="#referenceId" data-toggle="tab">
                                    <div class="workflow_block">
                                        <div class="workflow_icon">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </div>
                                        <div class="workflow_title">Checker</div>
                                    </div>
                                </a></li>
                            <li class="submitter"><a href="#go" data-toggle="tab">
                                    <div class="workflow_block">
                                        <div class="workflow_icon">
                                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                        </div>
                                        <div class="workflow_title">Submitter</div>
                                    </div>
                                </a></li>
                            <li class="work_master"><a href="#order" data-toggle="tab">
                                    <div class="workflow_block">
                                        <div class="workflow_icon">
                                            <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="workflow_title">Ps-Audit</div>
                                    </div>
                                </a></li>
                            <li class="substitute"><a href="#list" data-toggle="tab">
                                    <div class="substitute">
                                        <div class="workflow_icon">
                                            <i class="fa fa-building" aria-hidden="true"></i>
                                        </div>
                                        <div class="workflow_title">Works Master</div>
                                    </div>
                                </a></li>

                            <li class="applications"><a href="#workflow" data-toggle="tab">
                                    <div class="workflow_block">
                                        <div class="workflow_icon">
                                            <i class="fa fa-keyboard-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="workflow_title">workflow</div>
                                    </div>
                                </a></li>
                        </ul>
                    </div>
                </div>
        </div>
        </div>
 </form:form>
        <div class="container" style="margin-top: 200px;">
            <div class="row bg_hash text-right">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <input type="submit" class="btn btn-success" onclick="return submitData();" />

                </div>
            </div>
        </div>
  
</body>
<script type="text/javascript">
function showaddform(){

$("#showform").show();

}

function showeditform(){

$("#showform").show();

}
    function deleteRow(obj) {
        if ($(obj).closest('tr').closest("tbody").find("tr").length > 1) {
            $(obj).closest('tr').remove();
        }
    }
</script>

</html>
