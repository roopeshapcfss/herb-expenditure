<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <base href="<%=basePath%>">

    <title>Initiate BRO</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">



    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript">


    </script>
    <script src="<%=basePath%>js/billSubmissionWorkFlow.js"></script>
    <%-- <script src="<%=basePath%>js/jquery.sumoselect.min.js"></script>
    <link href="<%=basePath%>css/sumoselect.css" rel="stylesheet" /> --%>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.0.2/sumoselect.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.0.2/jquery.sumoselect.min.js"></script>
    <link rel="stylesheet" href="<%=basePath%>css/datepicker.min.css">
    <script type="text/javascript" src="<%=basePath%>js/bootstrap-datepicker.min.js"></script>

    <style>
        #cover-spin {
            /* position:fixed; */
            width: 100%;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            background-color: rgba(255, 255, 255, 0.7);
            /* z-index:9999; */
            display: none;
        }

        #cover-spin::after {
            display: block;
            position: absolute;
            left: 100%;
            top: 40%;
            width: 100px;
            height: 100px;
            border-style: solid;
            border-color: black;
            border-top-color: transparent;
            border-width: 4px;
            border-radius: 50%;
            -webkit-animation: spin .8s linear infinite;
            animation: spin .8s linear infinite;
        }

        .table-responsive {
            overflow-x: scroll;
        }

      .bg_hash {
		    background-color: #111;
		    width: 100%;
		    padding: 10px;
		    margin-top: 	px;
		    margin-left: inherit;
		}

        .mt5 {
            margin-top: 5px;
        }

        .input-group-text {
            min-width: auto !important;
        }

        .datepicker.dropdown-menu {
            width: auto !important;
        }

        .SumoSelect {
            width: 60%;
        }

        .workflow ul {
            padding: 0;
            margin: 0
        }

        .workflow ul li {
            display: inline-block;
            margin: 17px 14px;
            margin-bottom: 0;
        }

        .workflow ul li.active .workflow_block {
            border-bottom: 3px solid #5e696e;
        }

        .workflow ul li.active .workflow_icon {
            background: #5e696e;
            color: #fff;
            border: 0;
        }

        .workflow ul li.active .workflow_icon i {
            color: #fff;
            font-size: 1.5em;
        }

        .workflow_icon {
            background: transparent;
            border: 1px solid rgba(0, 0, 0, 0.5);
            width: 48px;
            height: 48px;
            border-radius: 50%;
            margin: 0px auto;
        }

        .workflow_icon i {
            margin: 14px;
            color: #5e696e;
            font-size: 1.5em;
        }

        .workflow_title {
            text-align: center;
        }

        .workflow {
            background: #eff4f9;
            border-bottom: 4px solid #d1e0ee
        }

        .workflow_title {
            font-size: 16px;
            padding-top: 6px;
            padding-bottom: 8px;
        }

        .workflow a {
            color: #78797a;
            text-decoration: none;
        }

        .heading_main {
            background: #eff4f9;
            color: #545252;
            font-weight: 600;
            margin-bottom: 0;
            padding: 10px;
            border-bottom: 2px solid #e0f0ff;
        }

        .heading_main p {
            margin-bottom: 0;
        }

        .org_det {
            padding-top: 24px;
            padding-bottom: 24px;
        }

        .org_det span b {
            color: #666;
        }

        .dis_int {
            display: flex;
        }

        input:read-only {
            background-color: #fff !important;
        }

        .text-danger {
            color: #dc3545 !important;
            border: 1px solid;
            margin: -2px;
            padding: 7px;
            border-radius: 6px;
            background-color: #f77b7b21;
        }

        .bg_info {
            background: #eff4f9;
            margin-left: 84px;
        }

        .SumoSelect .select-all {
            border-radius: 3px 3px 0 0;
            position: relative;
            border-bottom: 1px solid #ddd;
            background-color: #fff;
            padding: 8px 0 3px 35px;
            height: 37px;
            cursor: pointer;
            width: 164%;
        }

        .SumoSelect>.CaptionCont {

            width: 164%;
        }

        .SumoSelect>.optWrapper {

            width: 164%;

        }

        .multiselect,
        .form-group .btn-group {
            width: 100% !important;
            border-radius: 0;
        }
        .table > tbody > tr > td {
		     vertical-align: middle;
		     text-align: center;
		}
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example2').DataTable();

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.testselect2').SumoSelect({
                    search: true,
                    searchText: 'Enter here.',
                    selectAll: true
                }
                /* {
		    placeholder: 'Select Here',
		    csvDispCount: 4,
		    captionFormat:'{0} Selected', 
		    captionFormatAllSelected:'{0} all selected!',
		    floatWidth: 1000,
		    forceCustomRendering: true,
		    nativeOnDevice: ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'],
		    outputAsCSV: false,
		    csvSepChar: ',',
		    okCancelInMulti: false,
		    isClickAwayOk: false,
		    triggerChangeCombined: true,
		    selectAll: true,
		    search: true,
		    searchText: 'Search...',
		    noMatch: 'No matches for "{0}"',
		    prefix: '',
		    locale: ['OK', 'Cancel', 'Select All'],
		    up: false,
		    showTitle: true
		} */
            );
        });
    </script>
</head>

<body>

   <form:form method="post" modelAttribute="expenditureBillSubmissionWorkflow" action="${submitPath}" id="formId">
             <form:hidden path="positionId" id="positionId" />
             <form:hidden path="orgId" id="orgId" />
             <form:hidden path="hoa" id="hoaId" />
             <form:hidden path="userAction" id="userActionId" value="${type}" />
			 <c:if test="${(type == 'M')||(type == 'C')}">
	             <div class="row">
	                 <div class="col-12  col-sm-12 col-md-12 col-lg-12 col-xl-12">
	                     <!-- Tablelist -->
	                     <div class="row">
	                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-12">
	                             <br />
	                             <table id="exampleTable" class="table table-striped  table-sm table-hover nowrap" style="width: 100%;">
	                                 <thead>
	                                     <tr>
	                                         <th align="center"></th>
	                                         <th align="center">Position</th>
	                                         <th align="center">Employee</th>
	                                         <th align="center">From Date</th>
	                                         <th align="center">To Date</th>
	                                         <th align="center">HoA</th>
	                                         <th align="center">Org Unit</th>
	                                         <th align="center">Delete</th>
	                                     </tr>
	                                 </thead>
	                                 <tbody>
	                                 	<c:forEach items="${workFlowList}" var="row"  varStatus="loop">
	                                     <tr>
	                                         <td align="center"><input type="radio" value="${row.work_flow_id}"></td>
	                                         <td><div><strong>${row.position_name}</strong></div><div>${row.position_id}</div></td>
	                                         <td><div><strong>${row.name}</strong></div>${row.cfmsid}</div></td>
	                                         <td>${row.startdate}</td>
	                                         <td>${row.endate}</td>
	                                         <td><form:input path="" cssClass="form-control " /></td>
	                                         <td><form:input path="" cssClass="form-control" /></td>
	                                         <td align="center"><a></a><i class="fa fa-trash" aria-hidden="true"></i></td>
	                                     </tr>
	                                     </c:forEach>
	                                 </tbody>
	                                 <tfoot>
	                                 </tfoot>
	                             </table>
	                         </div>
	                     </div>
	                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0">
	                         <div class="row breakClass">
	                             <div class=" col-md-6" style="text-align: left;">
	                                 <div class="blocks-title m-l-0">Add/Edit Authorization</div>
	                             </div>
	                             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-16 text-right">
	                                 <a href="javascript:showaddform()" class="m-r-1 mr-3"><i class="fa fa-plus" aria-hidden="true"></i></a>
	                                 <a href="javascript:showeditform()" class="m-r-1 mr-3"><i class="fa fa-pencil" aria-hidden="true"></i></a>
	                             </div>
	                         </div>
	                     </div>
	                     
	                     <!-- MAKER FORM -->
	                     <c:if test="${type == 'M'}">
	                     <!-- Tab panes -->
	                     <div class="tab-content">
	                         <div id="details" class="container tab-pane active">
	                             <br>
	                             <!-- Position -->
	                             <div class="row">
	                                 <div class="col-12  col-sm-12 col-md-1 col-xl-1 col-lg-1"></div>
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;Position:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <%-- <form:input path="" cssClass="form-control mandatoryField" id="positionTextId" autocomplete="off" /> --%>
	
	                                         <%-- <form:input path="" cssClass="form-control mandatoryField" id="positionTextId" autocomplete="off" /> --%>
	                                         <form:input path="" class="form-control mandatoryField" id="positionTextId" autocomplete="off" />
	                                         <span id="position_text"></span>
	                                         <span id="positionError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                                 <div class="col-12  col-sm-12 col-md-2 col-xl-2 col-lg-2"></div>
	                                 <!--  HOA -->
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;HOA:</strong> <span class="mandatory">*</span></span><!--   -->
	                                         </div>
	                                         <form:input path="" Class="form-control mandatoryField" id="hoaTextId" autocomplete="off" data-toggle="modal" data-target="#hoaModal" />
	                                         <%-- <form:input path="" cssClass="form-control mandatoryField" id="hoaTextId" autocomplete="off" data-toggle="modal" data-target="#hoaModal" /> --%>
	                                         <span id="hoa_text"></span>
	                                         <span id="hoaError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                             </div>
	                             <!--  Employee -->
	                             <div class="row">
	                                 <div class="col-12  col-sm-12 col-md-1 col-xl-1 col-lg-1"></div>
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;Employee:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <form:input path="" cssClass="form-control mandatoryField" id="employeeId" autocomplete="off" />
	                                         <span id="employee_text"></span>
	                                         <span id="employeeError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                                 <div class="col-12  col-sm-12 col-md-2 col-xl-2 col-lg-2"></div>
	                                 <!--  Org Unit -->
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class="orgId"></span><strong>&nbsp;Org Unit:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <%-- <form:input path="" /> --%>
	                                         <!-- <input  class="form-control mandatoryField" name="orgUnit" id="orgTextId" autocomplete="off" data-toggle="modal" data-target="#orgUnitModal" /> -->
	                                         <form:input class="form-control mandatoryField" path="" id="orgTextId" autocomplete="off" data-toggle="modal" data-target="#orgUnitModal" />
	                                         <span id="org_text"></span>
	                                         <span id="orgError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                             </div>
	                             <!-- From Date -->
	                             <div class="row">
	                                 <div class="col-12  col-sm-12 col-md-1 col-xl-1 col-lg-1"></div>
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;From Date:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <form:input path="startDate" cssClass="form-control mandatoryField date-picker" id="fromdateId" autocomplete="off" readonly="true" />
	                                         <div class="input-group-append">
	                                             <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	                                         </div>
	                                         <span id="fromdateError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                             </div>
	                             <!-- To Date -->
	                             <div class="row">
	                                 <div class="col-12  col-sm-12 col-md-1 col-xl-1 col-lg-1"></div>
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class="endDate"></span><strong>&nbsp;To Date:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <form:input path="endDate" cssClass="form-control mandatoryField todateId" id="todateId" autocomplete="off" />
	                                         <div class="input-group-append">
	                                             <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	                                         </div>
	                                         <span id="todateError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                             </div>
	
	                             <!-- Position Modal -->
	                             <div class="modal fade" id="positionModal" role="dialog">
	                                 <div class="modal-dialog">
	                                     <div class="modal-content selectClass">
	                                         <div class="modal-header">
	                                             <h5 class="modal-title" id="exampleModalCenterTitle" style="text-align: center;">&nbsp;Position</h5>
	                                         </div>
	                                         <div class="modal-body bodyClass">
	                                             <div class="input-group input-group-sm mb-3">
	                                                 <div class="input-group-append   ">
	                                                     <span class="input-group-text"><span class="fa fa-search searchspan"></span>&nbsp;Search Position</span>
	                                                 </div>
	                                                 <input type="text" class="form-control select" id="inputValidation" placeholder="Search" />
	
	                                             </div>
	                                             <div class="list-group list-group-flush" id="searchOption">
	                                                 <c:forEach var="row" items="${positionList}">
	                                                     <a href="javascript:void(0)" class="list-group-item list-group-item-action" data-id="${row.position_id}" id="departmentModelId" data-dismiss="modal" onclick="return positionModalOption(this)">
	
	                                                         <div class="row">
	                                                             <div class="col-12 col-xs-12 col-sm-6 col-lg-6 col-xl-6">
	                                                                 <div><strong>${row.position_id}</strong></div>
	                                                                 <div>${row.position_name}</div>
	
	                                                             </div>
	                                                             <div class="col-12 col-xs-12 col-sm-6 col-lg-6 col-xl-6">
	                                                                 <div><strong>${row.cfmsid}</strong></div>
	                                                                 <div>${row.name}</div>
	                                                             </div>
	
	                                                         </div>
	                                                         <div class="positionDetails" style="display: none;">
	                                                             ${row.position_id}@${row.position_name}@${row.cfmsid}@${row.name}
	                                                         </div>
	                                                     </a>
	                                                 </c:forEach>
	                                             </div>
	
	                                         </div>
	                                         <div class="modal-footer" style="">
	                                             <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal" style="line-height: normal;margin-top: -4px;">Cancel</button>
	                                         </div>
	                                     </div>
	                                 </div>
	                             </div>
	                             <!-- HOA Modal -->
	                             <div class="modal fade" id="hoaModal">
	                                 <div class="modal-dialog modal-lg">
	                                     <div class="modal-content pad0">
	
	                                         <!-- Modal Header -->
	                                         <div class="modal-header">
	                                             <div>HOA Selection</div>
	                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                         </div>
	                                         <div class="modal-body" style="height: 1000000px;background: #5f608c1c;" align="left">
	                                             <div class="input-group input-group-sm mb-3">
	                                                 <div class="input-group-append   ">
	                                                     <span class="input-group-text"><span class="fa fa-search search"></span>&nbsp;Search</span>
	                                                 </div>
	                                                 <input type="text" class="search form-control " placeholder="Search" />
	                                             </div>
	                                             <div class="list-group list hoaList" id="searchOption">
	                                                 <label class="hoaDivclass">
	                                                     <div class="row list-group-item hoaListItem hoaDivclass" style="background: #8fb5ec14; padding: 5px; border:1px solid #939fb32e;">
	                                                         <div class="col">
	
	                                                             <input type="checkbox" class="form-check-input checkAll" onchange="chechAll(this)" value="">
	                                                             <span class="list-group-item-text title">Select All</span>
	
	                                                         </div>
	                                                     </div>
	                                                 </label>
	                                                 <c:forEach items="${hoaList}" var="hoa">
	                                                     <label class="hoaDivclass">
	                                                         <div class="row list-group-item hoaListItem " style="background: #8fb5ec14; padding: 5px; border:1px solid #939fb32e;">
	                                                             <div class="col">
	
	                                                                 <input type="checkbox" class="form-check-input checkItem" value="${hoa.value}">
	                                                                 <span class="list-group-item-text title hoaValue">${hoa.label}</span>
	
	                                                             </div>
	                                                         </div>
	                                                     </label>
	
	                                                 </c:forEach>
	                                             </div>
	
	                                         </div>
	                                     </div>
	                                     <!-- Modal footer -->
	                                     <div class="modal-footer">
	                                         <button type="button" class="btn btn-secondary" id="assignId" onclick="selectOptionsHOA();">Select</button>
	                                         <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="selectOptionsHOA()"> Close</button>
	                                     </div>
	                                 </div>
	                             </div>
	
	                             <!-- OrgUnit Modal -->
	                             <div class="modal fade" id="orgUnitModal">
	                                 <div class="modal-dialog modal-lg">
	                                     <div class="modal-content pad0">
	
	                                         <!-- Modal Header -->
	                                         <div class="modal-header">
	                                             <div>Org Unit</div>
	                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                         </div>
	                                         <div class="modal-body" style="height: 1000000px;background: #5f608c1c;" align="left">
	                                             <div class="input-group input-group-sm mb-3">
	                                                 <div class="input-group-append   ">
	                                                     <span class="input-group-text"><span class="fa fa-search search"></span>&nbsp;Search</span>
	                                                 </div>
	                                                 <input type="text" class="search form-control select" id="searchBox" placeholder="Search" />
	                                             </div>
	                                             <div class="list-group list hoaList" id="searchOption">
	                                                 <label>
	                                                     <div class="row list-group-item hoaListItem" style="background: #8fb5ec14; padding: 5px; border:1px solid #939fb32e;">
	                                                         <div class="col">
	
	                                                             <input type="checkbox" class="form-check-input checkAll" value="">
	                                                             <span class="list-group-item-text title">Select All</span>
	
	                                                         </div>
	                                                     </div>
	                                                 </label>
	                                                 <c:forEach items="${orgList}" var="org">
	                                                     <label>
	                                                         <div class="row list-group-item hoaListItem" style="background: #8fb5ec14; padding: 5px; border:1px solid #939fb32e;">
	                                                             <div class="col">
	
	                                                                 <input type="checkbox" class="form-check-input checkItem" value="${org.value}">
	                                                                 <span class="list-group-item-text title hoaValue">${org.label}</span>
	
	                                                             </div>
	                                                         </div>
	                                                     </label>
	
	                                                 </c:forEach>
	                                             </div>
	
	                                         </div>
	                                     </div>
	                                     <!-- Modal footer -->
	                                     <div class="modal-footer">
	                                         <button type="button" class="btn btn-secondary" id="assignId" onclick="selectOptionsOrgUnit();">Select</button>
	                                         <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close</button>
	                                     </div>
	                                 </div>
	                             </div>
	                         </div>
	                     </div>
	                     </c:if>
	                     
	                     <!-- CHECHER FORM -->
	                     <c:if test="${type == 'C'}">
	                     <!-- Tab panes -->
	                     <div class="tab-content">
	                         <div id="details" class="container tab-pane active">
	                             <br>
	                             <!-- Position -->
	                             <div class="row">
	                                 <div class="col-12  col-sm-12 col-md-1 col-xl-1 col-lg-1"></div>
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;Position:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <%-- <form:input path="" cssClass="form-control mandatoryField" id="positionTextId" autocomplete="off" /> --%>
	
	                                         <%-- <form:input path="" cssClass="form-control mandatoryField" id="positionTextId" autocomplete="off" /> --%>
	                                         <form:input path="" class="form-control mandatoryField" id="positionTextId" autocomplete="off" />
	                                         <span id="position_text"></span>
	                                         <span id="positionError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                                 <div class="col-12  col-sm-12 col-md-2 col-xl-2 col-lg-2"></div>
	                                  <!--  Level -->
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;Level:</strong> <span class="mandatory">*</span></span><!--   -->
	                                         </div>
	                                         <form:select path="level" cssClass="form-control mandatoryField" id="levelId">
	                                         	<form:option value="1">Level 01</form:option>
	                                         	<form:option value="2">Level 02</form:option>
	                                         	<form:option value="3">Level 03</form:option>
	                                         	<form:option value="4">Level 04</form:option>
	                                         	<form:option value="5">Level 05</form:option>
	                                         </form:select>
	                                         <span id="hoa_text"></span>
	                                         <span id="hoaError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                             
	                             </div>
	                             <!--  Employee -->
	                             <div class="row">
	                                 <div class="col-12  col-sm-12 col-md-1 col-xl-1 col-lg-1"></div>
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;Employee:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <form:input path="" cssClass="form-control mandatoryField" id="employeeId" autocomplete="off" />
	                                         <span id="employee_text"></span>
	                                         <span id="employeeError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                                 <div class="col-12  col-sm-12 col-md-2 col-xl-2 col-lg-2"></div>
	                                 <!--  HOA -->
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;HOA:</strong> <span class="mandatory">*</span></span><!--   -->
	                                         </div>
	                                         <form:input path="" Class="form-control mandatoryField" id="hoaTextId" autocomplete="off" data-toggle="modal" data-target="#hoaModal" />
	                                         <%-- <form:input path="" cssClass="form-control mandatoryField" id="hoaTextId" autocomplete="off" data-toggle="modal" data-target="#hoaModal" /> --%>
	                                         <span id="hoa_text"></span>
	                                         <span id="hoaError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                                
	                             </div>
	                             <!-- From Date -->
	                             <div class="row">
	                                 <div class="col-12  col-sm-12 col-md-1 col-xl-1 col-lg-1"></div>
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class=""></span><strong>&nbsp;From Date:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <form:input path="startDate" cssClass="form-control mandatoryField date-picker" id="fromdateId" autocomplete="off" readonly="true" />
	                                         <div class="input-group-append">
	                                             <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	                                         </div>
	                                         <span id="fromdateError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                                 <div class="col-12  col-sm-12 col-md-2 col-xl-2 col-lg-2"></div>
	                                  <!--  Org Unit -->
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class="orgId"></span><strong>&nbsp;Org Unit:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <%-- <form:input path="" /> --%>
	                                         <!-- <input  class="form-control mandatoryField" name="orgUnit" id="orgTextId" autocomplete="off" data-toggle="modal" data-target="#orgUnitModal" /> -->
	                                         <form:input class="form-control mandatoryField" path="" id="orgTextId" autocomplete="off" data-toggle="modal" data-target="#orgUnitModal" />
	                                         <span id="org_text"></span>
	                                         <span id="orgError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                             </div>
	                             <!-- To Date -->
	                             <div class="row">
	                                 <div class="col-12  col-sm-12 col-md-1 col-xl-1 col-lg-1"></div>
	                                 <div class="col-12  col-sm-12 col-md-4 col-xl-4 col-lg-4">
	
	                                     <div class="input-group input-group-sm mb-3">
	                                         <div class="input-group-append">
	                                             <span class="input-group-text">
	                                                 <span class="endDate"></span><strong>&nbsp;To Date:</strong> <span class="mandatory">*</span></span>
	                                         </div>
	                                         <form:input path="endDate" cssClass="form-control mandatoryField todateId" id="todateId" autocomplete="off" />
	                                         <div class="input-group-append">
	                                             <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	                                         </div>
	                                         <span id="todateError" style="color: red"></span>
	                                     </div>
	                                 </div>
	                             </div>
	
	                             <!-- Position Modal -->
	                             <div class="modal fade" id="positionModal" role="dialog">
	                                 <div class="modal-dialog">
	                                     <div class="modal-content selectClass">
	                                         <div class="modal-header">
	                                             <h5 class="modal-title" id="exampleModalCenterTitle" style="text-align: center;">&nbsp;Position</h5>
	                                         </div>
	                                         <div class="modal-body bodyClass">
	                                             <div class="input-group input-group-sm mb-3">
	                                                 <div class="input-group-append   ">
	                                                     <span class="input-group-text"><span class="fa fa-search searchspan"></span>&nbsp;Search Position</span>
	                                                 </div>
	                                                 <input type="text" class="form-control select" id="inputValidation" placeholder="Search" />
	
	                                             </div>
	                                             <div class="list-group list-group-flush" id="searchOption">
	                                                 <c:forEach var="row" items="${positionList}">
	                                                     <a href="javascript:void(0)" class="list-group-item list-group-item-action" data-id="${row.position_id}" id="departmentModelId" data-dismiss="modal" onclick="return positionModalOption(this)">
	
	                                                         <div class="row">
	                                                             <div class="col-12 col-xs-12 col-sm-6 col-lg-6 col-xl-6">
	                                                                 <div><strong>${row.position_id}</strong></div>
	                                                                 <div>${row.position_name}</div>
	
	                                                             </div>
	                                                             <div class="col-12 col-xs-12 col-sm-6 col-lg-6 col-xl-6">
	                                                                 <div><strong>${row.cfmsid}</strong></div>
	                                                                 <div>${row.name}</div>
	                                                             </div>
	
	                                                         </div>
	                                                         <div class="positionDetails" style="display: none;">
	                                                             ${row.position_id}@${row.position_name}@${row.cfmsid}@${row.name}
	                                                         </div>
	                                                     </a>
	
	                                                 </c:forEach>
	                                             </div>
	
	                                         </div>
	                                         <div class="modal-footer" style="">
	                                             <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal" style="line-height: normal;margin-top: -4px;">Cancel</button>
	                                         </div>
	                                     </div>
	                                 </div>
	                             </div>
	                             <!-- HOA Modal -->
	                             <div class="modal fade" id="hoaModal">
	                                 <div class="modal-dialog modal-lg">
	                                     <div class="modal-content pad0">
	
	                                         <!-- Modal Header -->
	                                         <div class="modal-header">
	                                             <div>HOA Selection</div>
	                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                         </div>
	                                         <div class="modal-body" style="height: 1000000px;background: #5f608c1c;" align="left">
	                                             <div class="input-group input-group-sm mb-3">
	                                                 <div class="input-group-append   ">
	                                                     <span class="input-group-text"><span class="fa fa-search search"></span>&nbsp;Search</span>
	                                                 </div>
	                                                 <input type="text" class="search form-control " placeholder="Search" />
	                                             </div>
	                                             <div class="list-group list hoaList" id="searchOption">
	                                                 <label class="hoaDivclass">
	                                                     <div class="row list-group-item hoaListItem hoaDivclass" style="background: #8fb5ec14; padding: 5px; border:1px solid #939fb32e;">
	                                                         <div class="col">
	
	                                                             <input type="checkbox" class="form-check-input checkAll" onchange="chechAll(this)" value="">
	                                                             <span class="list-group-item-text title">Select All</span>
	
	                                                         </div>
	                                                     </div>
	                                                 </label>
	                                                 <c:forEach items="${hoaList}" var="hoa">
	                                                     <label class="hoaDivclass">
	                                                         <div class="row list-group-item hoaListItem " style="background: #8fb5ec14; padding: 5px; border:1px solid #939fb32e;">
	                                                             <div class="col">
	
	                                                                 <input type="checkbox" class="form-check-input checkItem" value="${hoa.value}">
	                                                                 <span class="list-group-item-text title hoaValue">${hoa.label}</span>
	
	                                                             </div>
	                                                         </div>
	                                                     </label>
	
	                                                 </c:forEach>
	                                             </div>
	
	                                         </div>
	                                     </div>
	                                     <!-- Modal footer -->
	                                     <div class="modal-footer">
	                                         <button type="button" class="btn btn-secondary" id="assignId" onclick="selectOptionsHOA();">Select</button>
	                                         <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="selectOptionsHOA()"> Close</button>
	                                     </div>
	                                 </div>
	                             </div>
	
	                             <!-- OrgUnit Modal -->
	                             <div class="modal fade" id="orgUnitModal">
	                                 <div class="modal-dialog modal-lg">
	                                     <div class="modal-content pad0">
	
	                                         <!-- Modal Header -->
	                                         <div class="modal-header">
	                                             <div>Org Unit</div>
	                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                         </div>
	                                         <div class="modal-body" style="height: 1000000px;background: #5f608c1c;" align="left">
	                                             <div class="input-group input-group-sm mb-3">
	                                                 <div class="input-group-append   ">
	                                                     <span class="input-group-text"><span class="fa fa-search search"></span>&nbsp;Search</span>
	                                                 </div>
	                                                 <input type="text" class="search form-control select" id="searchBox" placeholder="Search" />
	                                             </div>
	                                             <div class="list-group list hoaList" id="searchOption">
	                                                 <label>
	                                                     <div class="row list-group-item hoaListItem" style="background: #8fb5ec14; padding: 5px; border:1px solid #939fb32e;">
	                                                         <div class="col">
	
	                                                             <input type="checkbox" class="form-check-input checkAll" value="">
	                                                             <span class="list-group-item-text title">Select All</span>
	
	                                                         </div>
	                                                     </div>
	                                                 </label>
	                                                 <c:forEach items="${orgList}" var="org">
	                                                     <label>
	                                                         <div class="row list-group-item hoaListItem" style="background: #8fb5ec14; padding: 5px; border:1px solid #939fb32e;">
	                                                             <div class="col">
	
	                                                                 <input type="checkbox" class="form-check-input checkItem" value="${org.value}">
	                                                                 <span class="list-group-item-text title hoaValue">${org.label}</span>
	
	                                                             </div>
	                                                         </div>
	                                                     </label>
	
	                                                 </c:forEach>
	                                             </div>
	
	                                         </div>
	                                     </div>
	                                     <!-- Modal footer -->
	                                     <div class="modal-footer">
	                                         <button type="button" class="btn btn-secondary" id="assignId" onclick="selectOptionsOrgUnit();">Select</button>
	                                         <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close</button>
	                                     </div>
	                                 </div>
	                             </div>
	                         </div>
	                     </div>
	                     </c:if>
	                 </div>
	             </div>
             </c:if>
         </form:form>
        <div class="container" style="margin-top: 200px;">
            <div class="row bg_hash text-right">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <input type="submit" class="btn btn-success" onclick="return submitData();" />

                </div>
            </div>
        </div>
  
</body>
<script type="text/javascript">
function showaddform(){

$("#showform").show();

}

function showeditform(){

$("#showform").show();

}
    function deleteRow(obj) {
        if ($(obj).closest('tr').closest("tbody").find("tr").length > 1) {
            $(obj).closest('tr').remove();
        }
    }
</script>

</html>
