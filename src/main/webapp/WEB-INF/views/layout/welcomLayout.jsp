<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%> 
<html>
<head>

<!-- Allows both Text area and submit button for enter key  -->
<script language="JavaScript">
 var nav = window.Event ? true : false;
   if (nav) {
        window.captureEvents(Event.KEYDOWN);
        window.onkeydown = NetscapeEventHandler_KeyDown;
   } else {
        document.onkeydown = MicrosoftEventHandler_KeyDown;
   }

   function NetscapeEventHandler_KeyDown(e) {
       if (e.which == 13 && e.target.type != 'textarea' && e.target.type != 'submit') { 
           return false; 
       }
           return true;
   }

   function MicrosoftEventHandler_KeyDown() {
       if (event.keyCode == 13 && event.srcElement.type != 'textarea' && 
            event.srcElement.type!= 'submit')
             return false;
       return true;
   }
 </script>

<!-- Allows Only submit button for enter key -->
	<!-- <script language="JavaScript">
	 var nav = window.Event ? true : false;
	   if (nav) {
	        window.captureEvents(Event.KEYDOWN);
	        window.onkeydown = NetscapeEventHandler_KeyDown;
	   } else {
	        document.onkeydown = MicrosoftEventHandler_KeyDown;
	   }
	
	   function NetscapeEventHandler_KeyDown(e) {
	       if (e.which == 13 && e.target.type != 'submit') { 
	           return false; 
	       }
	           return true;
	   }
	
	   function MicrosoftEventHandler_KeyDown() {
	       if (event.keyCode == 13 && event.srcElement.type!= 'submit')
	             return false;
	       return true;
	   }
	 </script> -->
</head>
<body>
 
 	<div id="my-container" style="min-height:100vh;" >
		<tiles:insertAttribute  name="body"> </tiles:insertAttribute>
	</div>	 
	</body>
</html>