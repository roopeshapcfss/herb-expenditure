 <%@ page language="java" import="java.util.*" %>
<%-- <%@page import="app.common.util.*" %> --%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
  <!-- Fixed navbar -->
  <div class="container-fluid navbar-inverse">
  <nav class="navbar navbar-inverse container navbar-expand-lg navbar-light   navbar-fixed-top">
    <a class="navbar-brand" href="#"><img src="template-assets/img/herb.svg" width="50px" /></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto nav-left-style">
      <li class="nav-item "> <a href="<%=basePath%>"  onclick="window.history.go(-1); return false;" class="listt nav-link" data-toggle="tooltip" data-placement="bottom" title="Go Back"><i class="fa fa-arrow-circle-left" style="" aria-hidden="true"></i></a> </li>
	  <li class="nav-item "> <a href="javascript:void()"  class="listt nav-link"  data-toggle="tooltip" data-placement="bottom" title="Expand Container" id="full-width"> <i class="fa fa-arrows-h" style="" aria-hidden="true"></i></a> </li>	  
    </ul>
 
  </div>
</nav>
</div>