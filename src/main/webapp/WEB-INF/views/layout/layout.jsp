<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>    
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
  <base href="<%=basePath%>">
    <meta charset="utf-8">
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>HERB</title>
	<link rel="apple-touch-icon" sizes="57x57" href="<%=basePath%>template-assets/img/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<%=basePath%>template-assets/img/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<%=basePath%>template-assets/img/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<%=basePath%>template-assets/img/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<%=basePath%>template-assets/img/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<%=basePath%>template-assets/img/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<%=basePath%>template-assets/img/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<%=basePath%>template-assets/img/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<%=basePath%>template-assets/img/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<%=basePath%>template-assets/img/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<%=basePath%>template-assets/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<%=basePath%>template-assets/img/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<%=basePath%>template-assets/img/favicon-16x16.png">
	<link rel="manifest" href="<%=basePath%>template-assets/img/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<%=basePath%>template-assets/img/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href="<%=basePath%>template-assets/css/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
 	<link href="<%=basePath%>template-assets/css/inner-page.css" rel="stylesheet"/>
  <!-- </head> -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>    
    
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
 <!-- Datatables -->
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
   <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap4.min.css"> 
    <link href="css/sumoselect.css" rel="stylesheet" />

       
   
		<style type="text/css">
			.modal-body {
			    position: relative;
			    -ms-flex: 1 1 auto;
			    flex: 1 1 auto;
			    padding: 1rem;
			    max-height: 70vh;
			    overflow: auto;
			}
			.cloneButton{
			    color: white;
			    font-size: 11px;
			}
			.mandatory{
			    color: #c14646;
			    font-size: 1rem;
			    font-weight: bold;
			    margin-bottom: -7px;
			    margin-top: -7px;
			}
			.payment-content {
			    position: relative;
			    display: -ms-flexbox;
			    display: flex;
			    -ms-flex-direction: column;
			    flex-direction: column;
			    width: 173%;
			    pointer-events: auto;
			    background-color: #fff;
			    background-clip: padding-box;
			    border: 1px solid rgba(0,0,0,.2);
			    border-radius: .3rem;
			    outline: 0;
			    margin-left: -157px;
			    margin-top: 167px;
			}
			.hoa-content {
			    position: relative;
			    display: -ms-flexbox;
			    display: flex;
			    -ms-flex-direction: column;
			    flex-direction: column;
			    width: 253%;
			    pointer-events: auto;
			    background-color: #fff;
			    background-clip: padding-box;
			    border: 1px solid rgba(0,0,0,.2);
			    border-radius: .3rem;
			    outline: 0;
			    margin-left: -356px;
			    margin-top: 167px;
			    
			}             
			.hoa-body {
			    position: relative;
			    -ms-flex: 1 1 auto;
			    flex: 1 1 auto;
			    padding: 1rem;
			    max-height: 70vh;
			    overflow: auto;
			    height:239px;
			}
		</style>

<!-- Allows both Text area and submit button for enter key  -->
		<script language="JavaScript">
		 	   var nav = window.Event ? true : false;
			   if (nav) {
			        window.captureEvents(Event.KEYDOWN);
			        window.onkeydown = NetscapeEventHandler_KeyDown;
			   } else {
			        document.onkeydown = MicrosoftEventHandler_KeyDown;
			   }
			
			   function NetscapeEventHandler_KeyDown(e) {
			       if (e.which == 13 && e.target.type != 'textarea' && e.target.type != 'submit') { 
			           return false; 
			       }
			           return true;
			   }
			
			   function MicrosoftEventHandler_KeyDown() {
			       if (event.keyCode == 13 && event.srcElement.type != 'textarea' && 
			            event.srcElement.type!= 'submit')
			             return false;
			       return true;
			   }
		 </script>
		 <script type="text/javascript">
			if (top!=self)
		    {
		   		top.location.replace(location.href); 
		    }
		</script>

<!-- Allows Only submit button for enter key -->
<!-- <script language="JavaScript">
 var nav = window.Event ? true : false;
   if (nav) {
        window.captureEvents(Event.KEYDOWN);
        window.onkeydown = NetscapeEventHandler_KeyDown;
   } else {
        document.onkeydown = MicrosoftEventHandler_KeyDown;
   }

   function NetscapeEventHandler_KeyDown(e) {
       if (e.which == 13 && e.target.type != 'submit') { 
           return false; 
       }
           return true;
   }

   function MicrosoftEventHandler_KeyDown() {
       if (event.keyCode == 13 && event.srcElement.type!= 'submit')
             return false;
       return true;
   }
 </script> -->
 

   
 
<!-- </head> -->
		<style type="text/css">
			.footer{
				background-color :inear-gradient(to bottom, #0099ff -6%, #ffffff 100%);     /* #0c8ac1             #243f6b  */
			}
			.footerbarwrap {
				background-color: linear-gradient(to bottom, #0099ff -6%, #ffffff 100%);
			}
			body{
			 margin: 0;
			}
		</style>
	</head>
 	<body>
	  	<tiles:insertAttribute  name="header"> </tiles:insertAttribute> 
	 	<div id="my-container" class="container my-container " style="min-height:100vh;padding-bottom: 50px;" >
			<tiles:insertAttribute  name="body"> </tiles:insertAttribute>
		</div>	 
	 	<tiles:insertAttribute  name="footer"> </tiles:insertAttribute>
	<!-- </body> -->
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Datatables -->  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/responsive.bootstrap4.min.js"></script>

	<script src="<%=basePath%>template-assets/js/jquery.slimscroll.min.js"></script>
	<script src="<%=basePath%>template-assets/js/jquery.droptabs.min.js"></script>
	<script src="<%=basePath%>template-assets/js/bs4-toast.js"></script>
	<script src="<%=basePath%>js/commonValidation.js"></script>
	<script src="<%=basePath%>js/commonModal.js"></script>
	<script src="<%=basePath%>js/numbers.js"></script>
	<script src="<%=basePath%>js/jquery.sumoselect.min.js"></script>
	
	 <script> 
		$('#full-width').click(function(){$('#my-container').toggleClass('container container--fluid');});
		$(document).ready(function(){
		  $('[data-toggle="tooltip"]').tooltip();
		});
		function openNav() {
		  document.getElementById("myNav").style.width = "100%";
		}
		/* Close when someone clicks on the "x" symbol inside the overlay */
		function closeNav() {
		  document.getElementById("myNav").style.width = "0%";
		}
		 $(function(){
			$('.slimscroll-setting').slimScroll({
				height: '100vh'
			});
		});
	
	 </script>
    <script>
		$(document).ready(function() { 
		  $('[data-toggle="popover"]').popover({trigger: 'hover'}); 
		  $('.v-tab-head .v-tab-link').mouseover(tabHandler);
		  $('.v-tab-head.v-tab-link').click(tabHandler); 
		});
		var tabHandler = function(e) {
		  e.preventDefault(); 
		  var target = $($(this).data('target')),
			  tabLink = $('.v-tab-link[data-target="' + $(this).data('target') + '"]'); 
		  tabPanelToShow(tabLink);
		  tabLinkToActivate(target);
		}; 
		var tabPanelToShow = function(elem) {
		  $('.v-tab-link').removeClass('active').parent().find(elem).addClass('active');
		};
		var tabLinkToActivate = function(elem) {
		  $('.v-tab-pane').children('div').removeClass('in').parent().find(elem).addClass('in');
		};
	</script>
	 <script type="text/javascript">
		  $(".droptabs").droptabs({
		    development:true
		  });
	  </script>
  </body>
</html>


