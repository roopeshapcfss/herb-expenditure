<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'knowYourDepartment.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
   <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap4.min.css"> 
	
<!-- <script type="text/javascript">
$(document).ready(function() {
    var table = $('#example1').DataTable( {
        responsive: true,
        "lengthMenu": [[25, 50, 100], [25, 50, 100]]
                
    } );
 
    new $.fn.dataTable.FixedHeader( table );
} );
</script> -->
  </head>
  
  <body>
   <br />
   
   <div class="  home">
		 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<table id="example1" class="table table-striped table-bordered table-hover nowrap" style="width: 100%;">
		<thead>
		<tr><th colspan="3" align="center"  class="text-center" style="background-color: #bdd4d6;"> <font size="3"  ><b>Know Your Department</b> </font></th></tr>
		<tr>
					<th align="center" style="color:white;background-color:#285498;">Department</th>
					<th align="center" style="color:white;background-color:#285498;">Service</th>
					<th align="center" style="color:white;background-color:#285498;">HOA</th>
		</tr>
		</thead>
		<tbody>
		
		<c:forEach var="row" items="${deptList}" varStatus="index">
		<tr>   
			<td align="left"><b>${row.dept_desc }</b></td>
			<td align="left">${row.service_desc_short }</td>
			<td align="left">${row.hoa }</td>
		</tr>
		</c:forEach>	
		</tbody>
		</table>
		
   </div>
   </div>
  <%--  </form:form> --%>
  </body>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#example1').DataTable();
} );
  
  </script>
</html>
