<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
			String loggedin=(String)request.getAttribute("loggedin");
			System.out.println("loggedin-----"+loggedin);
%>

<!DOCTYPE html>
<html>
<head>

<script type="text/javascript" src="<%=basePath%>js/md5/md5.js"></script>

<style type="text/css">
    .errormsg {
        color: red;
    }
</style>
        
<base href="<%=basePath%>">

<title>Pension Recovery</title>
<script src="js/numbers.js" type="text/javascript"></script>
<script>
	function sendOtp() 
	{
		if ($("#userName").val() == "" || $("#userName").val() == null) 
		{
			alert("Please Enter Username");
			$("#userName").focus();
			return false;
		}
		else
		{
			document.getElementById("sendOtpId").disabled = true;
			document.getElementById("forgotPasswordId").action = "sendOtp";
			document.getElementById("forgotPasswordId").method = "POST";
			document.getElementById("forgotPasswordId").modelAttribute = "forgotPasswordForm";
			document.forms[0].submit();
		}
	}
	function validateOtp()
	{
		if ($("#userName").val() == "" || $("#userName").val() == null) 
		{
			alert("Please Enter Username");
			$("#userName").focus();
			return false;
		}
		
		if ($("#enterOtp").val() == "" || $("#enterOtp").val() == 0) 
		{
			alert("Please Enter OTP");
			$("#enterOtp").focus();
			return false;
		}
		else
		{
			$("#enterOtp").val(hex_md5($("#enterOtp").val()));
			document.getElementById("validateOtpId").disabled = true;
			document.getElementById("forgotPasswordId").action = "validateOtp";
			document.getElementById("forgotPasswordId").method = "POST";
			document.getElementById("forgotPasswordId").modelAttribute = "forgotPasswordForm";
			document.forms[0].submit();
		}
		
		
	}
	function changePassword()
	{
	document.getElementById("origpassword").value=document.getElementById("newPassword").value;
		if ($("#newPassword").val() == "" || $("#newPassword").val() == null) 
		{
			alert("Please Enter New Password");
			$("#newPassword").focus();
			return false;
		}
		if ($("#confirmNewPassword").val() == "" || $("#confirmNewPassword").val() == null) 
		{
			alert("Please Enter Confirm New Password");
			$("#confirmNewPassword").focus();
			return false;
		}
		if(hex_md5(document.getElementById("newPassword").value) == hex_md5(document.getElementById("confirmNewPassword").value))
		{
			document.getElementById("newPassword").value = hex_md5(document.getElementById("newPassword").value);
			document.getElementById("confirmNewPassword").value = hex_md5(document.getElementById("confirmNewPassword").value);
			document.getElementById("changePasswordId").disabled = true;
			document.getElementById("loggedin").value='<%=loggedin%>';
			document.getElementById("forgotPasswordId").action = "forgotPasswordPost";
			document.getElementById("forgotPasswordId").method = "POST";
			document.getElementById("forgotPasswordId").modelAttribute = "forgotPasswordForm";
			document.forms[0].submit();
		}
		else
		{
			alert("New Password and Confirm New Password are not matched");
			return false;
		}
		
	}
	
	function updatePassword(password){
	document.getElementById("checkpassword").value=password.value;
	var theResponse = null;
	$.ajax({
	type : 'GET',
	url : "loadResource/checkPassword" + '?key=ajax&pass='+ document.getElementById("checkpassword").value+'&userName='+$("#userName").val(),
	dataType : "html",
	async : false,
	success : function(res) {
	if(res!=""){
	$("#passwordckError").html("Users should not reuse last three passwords.");
		document.getElementById("newPassword").value ="";
	}
	},
	error : function() {
	alert('Error occured');
	}
});
	
	}
	
	function CheckPassword(inputtxt) 
	{ 
	var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-z0-9!@#$%^&*]{7,15}$/;
	if(inputtxt.value.match(paswd)) 
	{
		return true;
	}
	else
	{ 
		$("#passwordcktError").html("Password should be minimum of 8 Characters,one special character,one numeric digit.");
		return false;
	}
	}
	
	function checkMob(mobileNo){
	document.getElementById("mobileNo").value=mobileNo.value;
	 document.getElementById("mobileNo").value=mobileNo.value;
	var theResponse = null;
	$.ajax({
	type : 'GET',
	url : "loadResource/checkMobileNo" + '?key=ajax&mobileNo='+ document.getElementById("mobileNo").value+'&userName='+$("#userName").val(),
	dataType : "html",
	async : false,
	success : function(res) {
	if(res!=""){
	$("#mobileckError").html("User Should enter Registered Mobile Number");
		document.getElementById("mobileNo").value ="";
	}
	},
	error : function() {
	alert('Error occured');
	}
}); 
	
	} 
</script>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
  
</head>

<body>
	<script type="text/javascript" src="js/md5/md5.js"></script>
	<form:form method="post" modelAttribute="forgotPasswordForm" action="${url}" id="forgotPasswordId" enctype="multipart/form-data" class="needs-validation">
	<form:hidden path="origpassword" id="origpassword"></form:hidden>
	<form:hidden path="checkpassword" id="checkpassword"></form:hidden>
	<form:hidden path="loggedin" id="loggedin"></form:hidden>
	<div class=" container">
			<%-- <h3 class=" ">${headerMessage}</h3> --%>
			<div class="successMessage">${message}</div>
			<div class="errorMessage">${errorMessage}</div>
			<div class="  home">
				<div class="header" style="margin-top: 12px;">
					<div class="row">
					
					<c:if test="${otpValidation =='true'}">
					<div class="col-12 col-md-8">
							<span class="page-title" style="display: inline;margin-left: 25px;">Change Password</span>
						</div>
					</c:if>
					<c:if test="${otpValidation !='true'}">
						<div class="col-12 col-md-8">
							<span class="page-title" style="display: inline;margin-left: 25px;">Forgot Password</span>
						</div>
						</c:if>
					</div>
				</div>
			</div>
			<c:if test="${displayForm == true}">
					 <div class="row mt40">
						 <div class="col-12 col-md-8 offset-md-2">
						 	 <div class="input-group input-group-sm mb-3">
							   <div class="input-group-append   ">
							      <span class="input-group-text" ><span class="fa fa-search searchspan"></span>&nbsp;Username</span>
							   </div>
							     <form:input path="userName" placeholder="Username" cssClass="form-control input-sm" id="userName" autocomplete="off" />
							     <small><form:errors path="userName" cssClass="errormsg" /></small>
							   </div>
					  		</div>
					  </div>
					  
					    <div class="row mt40">
					 <div class="col-12 col-md-8 offset-md-2">
					 	 <div class="input-group input-group-sm mb-3">
						   <div class="input-group-append   ">
						      <span class="input-group-text" ><span class="fa fa-search searchspan"></span>&nbsp;Mobile Number</span>
						   </div>
						     <form:input path="mobileNo" placeholder="mobileNo" cssClass="form-control input-sm" id="mobileNo" autocomplete="off"  onblur="return checkMob(this)"/>
						     <span id="mobileckError" style="color: red"></span>
						   </div>
				  		</div>
				  </div> 
					<div class="col-md-6 offset-md-5">
						<button type="button" class="btn btn-primary btn-sm" id = "sendOtpId" style="margin-top: 20px;" onclick="javascript:return sendOtp()">Submit</button>
					</div>
			</c:if>
			<c:if test="${otpEnable =='true'}">
				<div class="row mt40">
					 <div class="col-12 col-md-8 offset-md-2">
					 	 <div class="input-group input-group-sm mb-3">
						   <div class="input-group-append   ">
						      <span class="input-group-text" ><span class="fa fa-search searchspan"></span>&nbsp;Username</span>
						   </div>
						     <form:input path="userName" placeholder="userName" cssClass="form-control input-sm" id="userName" autocomplete="off" readonly = "true"/>
						   </div>
				  		</div>
				  </div>
				   <div class="row mt40">
					 <div class="col-12 col-md-8 offset-md-2">
					 	 <div class="input-group input-group-sm mb-3">
						   <div class="input-group-append   ">
						      <span class="input-group-text" ><span class="fa fa-search searchspan"></span>&nbsp;Mobile Number</span>
						   </div>
						     <form:input path="mobileNo" placeholder="mobileNo" cssClass="form-control input-sm" id="mobileNo" autocomplete="off"   readonly = "true"/>
						   </div>
				  		</div>
				  </div> 
				<div class="row mt40">
					 <div class="col-12 col-md-8 offset-md-2">
					 	 <div class="input-group input-group-sm mb-3">
						   <div class="input-group-append   ">
						      <span class="input-group-text" ><span class="fa fa-search searchspan"></span>&nbsp;Generated Password</span>
						   </div>
						     <form:input path="enterOtp" placeholder="Generated Password" cssClass="form-control input-sm" id="enterOtp" autocomplete="off" />
						   </div>
				  		</div>
				  </div>
				<div class="col-md-6 offset-md-5">
					<button type="button" class="btn btn-primary btn-sm" style="margin-top: 20px;" id = "validateOtpId" onclick="javascript:return validateOtp()";>Submit</button>
				</div>
			</c:if>
			<c:if test="${otpValidation =='true'}">
				<form:hidden path="enterOtp" id="enterOtp"/>
				<div class="row mt40">
					 <div class="col-12 col-md-8 offset-md-2">
					 	 <div class="input-group input-group-sm mb-3">
						   <div class="input-group-append   ">
						      <span class="input-group-text" ><span class="fa fa-search searchspan"></span>&nbsp;Username</span>
						   </div>
						     <form:input path="userName" placeholder="userName" cssClass="form-control input-sm" id="userName" autocomplete="off" />
						   </div>
				  		</div>
				  </div>
				<div class="row mt40">
					 <div class="col-12 col-md-8 offset-md-2">
					 	 <div class="input-group input-group-sm mb-3">
						   <div class="input-group-append   ">
						      <span class="input-group-text" ><span class="fa fa-search searchspan"></span>&nbsp;New Password</span>
						   </div>
						     <form:password path="newPassword" placeholder="New Password" cssClass="form-control input-sm" id="newPassword" autocomplete="off" onblur="CheckPassword(this),updatePassword(this)"/>
						      <span id="passwordcktError" style="color: red"></span>
						      <span id="passwordckError" style="color: red"></span>
						     
						   </div>
				  		</div>
				  </div>
				  <div class="row mt40">
					 <div class="col-12 col-md-8 offset-md-2">
					 	 <div class="input-group input-group-sm mb-3">
						   <div class="input-group-append   ">
						      <span class="input-group-text" ><span class="fa fa-search searchspan"></span>&nbsp;Confirm New Password</span>
						   </div>
						     <form:password path="confirmNewPassword" placeholder="Confirm Password" cssClass="form-control input-sm" id="confirmNewPassword" autocomplete="off" />
						   </div>
				  		</div>
				  </div>
				<div class="row" style="text-align: center">
					<div class="col-md-6 offset-md-5">
						<button type="button" class="btn btn-primary btn-sm" style="margin-top: 20px;" id = "changePasswordId" onclick="javascript:return changePassword();">Update Password</button>
					</div>
				</div>
			</c:if>
	</div>
	</form:form>
</body>

</html>
