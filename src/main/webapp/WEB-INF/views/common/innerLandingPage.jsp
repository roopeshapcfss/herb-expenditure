<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<style>

/* The Modal (background) */
.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	padding-top: 100px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
	background-color: #fefefe;
	margin: auto;
	padding: 20px;
	border: 1px solid #888;
	width: 80%;
}

/* The Close Button */
.close {
	color: #aaaaaa;
	float: right;
	font-size: 28px;
	font-weight: bold;
}

.close:hover, .close:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}

@media ( min-width : 992px) {
	.modal-lg, .modal-xl {
		max-width: 800px !important;
	}
}

@media ( min-width : 1200px) {
	.modal-xl {
		max-width: 1250px !important;
	}
}

@media ( min-width : 576px) {
	.modal-dialog {
		max-width: 500px;
		margin: 1.75rem auto !important;
	}
}

.modal-dialog  table.table-striped>thead>tr>th {
	text-align: left !important;
	width: 25%;
	font-size: 14px;
}

.modal-dialog  table.table-striped>tbody>tr>td label {
	font-weight: bold;
	font-size: 14px;
}

.modal-header {
	background: #fff !important
}

.pad0 {
	padding: 0
}

h4.modal-title {
	margin-bottom: 0;
	line-height: 1.5;
	color: #4c4c4c;
	text-align: CENTER;
	width: 100%;
	margin: 0;
	font-size: 18px;
}

#myModal .modal-body {
	height: 60vh;
	overflow: auto;
}

#myModalhod .modal-body {
	height: 60vh;
	overflow: auto;
}
</style>

</head>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>HERB</title>

<!-- Bootstrap -->

<link rel="apple-touch-icon" sizes="57x57"
	href="template-assets/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60"
	href="template-assets/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="template-assets/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="template-assets/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="template-assets/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="template-assets/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144"
	href="template-assets/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="template-assets/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180"
	href="template-assets/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"
	href="template-assets/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32"
	href="template-assets/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96"
	href="template-assets/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16"
	href="template-assets/img/favicon-16x16.png">
<link rel="manifest" href="template-assets/img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage"
	content="template-assets/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<%-- <link href="<%=basePath%>template-assets/css/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
	<link href="<%=basePath%>template-assets/css/bootstrap.min.css" rel="stylesheet"/>
 	<link href="<%=basePath%>template-assets/css/inner-page.css" rel="stylesheet"/> --%>
<link href="template-assets/css/jquery.mCustomScrollbar.min.css"
	rel="stylesheet" />
<link href="template-assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="template-assets/css/inner-page.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
	integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
	crossorigin="anonymous"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<%=basePath%>template-assets/js/popper.min.js"></script>
<script src="<%=basePath%>template-assets/js/bootstrap.min.js"></script>
<script src="<%=basePath%>template-assets/js/jquery.slimscroll.min.js"></script>
<script src="<%=basePath%>template-assets/js/tabdrop.js"></script>
</head>
<body>


	<div class="container-fluid navbar-inverse">
		<nav
			class="navbar navbar-inverse container navbar-expand-lg navbar-light   navbar-fixed-top">
			<a class="navbar-brand" href="#"><img
				src="template-assets/img/herb.svg" width="50px" /></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto nav-left-style">
					<li class="nav-item "><a href="#" class="listt nav-link"
						onclick="openNav()" data-toggle="tooltip" data-placement="bottom"
						title="Menu" class="toggle"> <i class="fa fa-bars " style=""
							aria-hidden="true"></i>
					</a></li>
					<li class="nav-item "><a href="#" class="listt nav-link"
						data-toggle="tooltip" data-placement="bottom" title="Go Back"><i
							class="fa fa-arrow-circle-left" style="" aria-hidden="true"></i></a>
					</li>
					<li class="nav-item "><a href="#" class="listt nav-link"
						data-toggle="tooltip" data-placement="bottom"
						title="Expand Container" id="full-width"> <i
							class="fa fa-arrows-h" style="" aria-hidden="true"></i></a></li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item "><a href="#" class="listt nav-link"
						onclick="openNav2()" data-toggle="tooltip" data-placement="bottom"
						title="User settings" class="toggle"> <i class="fa fa-cogs "
							style="" aria-hidden="true"></i>&nbsp;
					</a></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#"
						id="navbarDropdownMenuLink" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> User:
							${sessionScope.welcomeUser} </a>
						<div class="dropdown-menu"
							aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="#">Profile</a> <a
								class="dropdown-item" href="#">Settings</a> <a
								class="dropdown-item" href="./secure/logout">Logout</a>
						</div></li>
				</ul>
			</div>
		</nav>
	</div>

	<script type="text/javascript">
	
		function getChildServices(moduleType) {
			var theResponse = null;
			$.ajax({
				type : 'GET',
				url : "loadResource/childServices" + '?key=ajax&moduleType=' + moduleType,
				dataType : "html",
				async : false,
				success : function(res) {
					theResponse = JSON.parse(res);
					var a = "";
					for (var i = 0; i <= theResponse.length - 1; i++) {
						a += "<a  class=\"big-title\" id=\"childServiceIds\"  data-dismiss=\"modal\" style=\"border-bottom: 1px solid rgba(0,0,0,.125);\"   data-id=\"" + theResponse[i].serviceName + "\" href=\"" + theResponse[i].target + "\"> <div class='block-parent block-parent-bg card card-1'><b>" + theResponse[i].serviceName + "</b></div></a> ";
					}
					/* $(".childTags").hide(); */
					$("#childServiceIds").html(a);
				},
				error : function() {
					alert('Error occured');
				}
			});
		}
		function getServicesbyParent(target) {
		}
		function getWorkFlow(process, org_id, task_id) {
			document.getElementById("process").value = process;
			document.getElementById("org_id").value = org_id;
			document.getElementById("task_id").value = task_id;
			document.getElementById("tilesId").action = "billSubmissionWorkFlow/budgetWorkflow";
			document.getElementById("tilesId").method = "POST";
			document.getElementById("tilesId").modelAttribute = "workFlowConfigForm";
			document.forms[0].submit();
		}
		function getWorkFlowhod(process, org_id, task_id) {
			document.getElementById("process").value = process;
			document.getElementById("org_id").value = org_id;
			document.getElementById("task_id").value = task_id;
			document.getElementById("tilesId").action = "hodbillSubmissionWorkFlow/hodbudgetWorkflow";
			document.getElementById("tilesId").method = "POST";
			document.getElementById("tilesId").modelAttribute = "workFlowConfigForm";
			document.forms[0].submit();
		}
		function showBudgetDistribution() {
			document.getElementById("tilesId").action = "getdetails";
			document.getElementById("tilesId").method = "POST";
			document.getElementById("tilesId").modelAttribute = "budgetDistributionForm";
			document.forms[0].submit();
		}
	</script>
	<form:form method="post" modelAttribute="loginForm" id="tilesId"
		enctype="multipart/form-data" class="needs-validation">
		<form:hidden path="process" id="process" />
		<form:hidden path="org_id" id="org_id" />
		<form:hidden path="task_id" id="task_id" />

		<div id="my-container" class="container my-container "
			style="padding:4px 4px 15px 4px;padding-bottom:50px;scroll-behavior: smooth; block: 'start' ">
			<div class="row ">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<ul class="nav nav-tabs droptabs header-fixed" id="myHeader"
						style="background:#e7f0ff;">
						<c:forEach var="row" items="${servicesList}">
							<li class="nav-item"><a class="nav-link"
								href="loginCheck#${row.moduleType}"
								onclick="getChildServices('${row.moduleType}')"><b>${row.serviceName}</b></a></li>
						</c:forEach>
					</ul>
					<div class="tab-content  " style="">
						<div role="tabpanel" class="tab-pane in active">
							<div class="blocks-title" id="loginCheck#R" style="">Report</div>
							<div style="spacer">&nbsp;</div>
							<c:forEach var="row" items="${reportservicesList}">
								<a href="${row.target}"
									onclick="getServicesbyParent('${row.target}')">
									<div class="block-parent block-parent-bg card card-1">
										<div class="big-title">${row.serviceName}</div>
										<div class="small-title">&nbsp;</div>
										<i class="fa fa-cogs icon-style"></i>
									</div>
								</a>
							</c:forEach>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="blocks-title" id="loginCheck#B" style="">Budget</div>
					<div style="spacer">&nbsp;</div>
					<c:forEach var="row" items="${budgetservicesList}">
						<c:if test="${row.target=='budgetWorkflow' && userType=='HOD'}">
							<a href="javascript:void(0)"
								onclick="getServicesbyParent('${row.target}')"
								data-toggle="modal" data-target="#myModal" >
								<div class="block-parent block-parent-bg card card-1">
							<div class="big-title">${row.serviceName}</div>
							<div class="small-title">&nbsp;</div>
							<i class="fa fa-cogs icon-style"></i>
						</div>
								</a>
						</c:if>
						
						<c:if test="${row.target=='hodbudgetWorkFlow'}">
							<a href="javascript:void(0)"
								onclick="getServicesbyParent('${row.target}')"
								data-toggle="modal" data-target="#myModalhod">
								<div class="block-parent block-parent-bg card card-1">
							<div class="big-title">${row.serviceName}</div>
							<div class="small-title">&nbsp;</div>
							<i class="fa fa-cogs icon-style"></i>
						</div>
								
								 </a> 
						</c:if>
						
						<c:if test="${row.target!='hodbudgetWorkFlow' && !(row.target=='budgetWorkflow' && userType=='HOD')    }">
						<a href="${row.target}"
									onclick="getServicesbyParent('${row.target}')">
						<div class="block-parent block-parent-bg card card-1">
							<div class="big-title">${row.serviceName}</div>
							<div class="small-title">&nbsp;</div>
							<i class="fa fa-cogs icon-style"></i>
						</div>
						</a>
						</c:if>
					</c:forEach>
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="blocks-title" id="loginCheck#E" style="">Expenditure</div>
			<div style="spacer">&nbsp;</div>
			<c:forEach var="row" items="${expenditureServiceList}">
				<a href="${row.target}"
					onclick="getServicesbyParent('${row.target}')">
					<div class="block-parent block-parent-bg card card-1">
						<div class="big-title">${row.serviceName}</div>
						<div class="small-title">&nbsp;</div>
						<i class="fa fa-cogs icon-style"></i>
					</div>
				</a>
			</c:forEach>
		</div>
		<c:if test="${not empty errorMsg}">
			<script type="text/javascript">
				$(document).ready(function() {
					$('#myModalclose').modal('show');
				});
			</script>
		</c:if>

	</form:form>
	<div class="footer">
		<p>
			Designed and Developed by <a href="#" target="_blank">APCFSS</a>
		</p>
	</div>


	<!-- The Modal -->
	<div class="modal fade" id="myModal">
		<div class="modal-dialog modal-xl ">
			<div class="modal-content pad0">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Select Process(379)</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<input id="myInput" type="text" placeholder="Search.."
						class="form-control">
					<table class="table table-striped budget_work_data table-hover">
						<thead>
							<tr>
								<th>Process</th>
								<th>Org Unit</th>
								<th>Task</th>
								<th>Funds Center</th>
								<th>Status</th>
							</tr>
						<tbody>
							<c:forEach var="row" items="${workFlowServiceList}">
								<tr
									onclick="getWorkFlow('${row.process}','${row.org_id}','${row.task_id}')">
									<td><label>${row.process}</label><br>${row.process_name}</td>
									<td><label>${row.org_id}</label><br>${row.org_name}</td>
									<td><label>${row.task_id}</label><br>${row.task_hod_name}</td>
									<td><label>${row.hodcode}</label><br>${row.description_long}</td>
									<td><label>${row.workflowstatus}</label></td>
							</c:forEach>

						</tbody>
					</table>

				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>


	<div class="modal fade" id="myModalhod">
		<div class="modal-dialog modal-xl ">
			<div class="modal-content pad0">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Notification</h4>

				</div>

				<!-- Modal body -->
				<div class="modal-body">

					<table class="table table-striped budget_work_data table-hover">
						<thead>
							<tr>
								<th>Wrno</th>
								<th>Estimated amount</th>
								<th>Status</th>
								<th>Next Officer</th>
								<th>Department</th>
							</tr>
						<tbody>
							<c:forEach var="row" items="${budgettransactionList}">
								<tr>
									<td><label>${row.wrno}</label></td>
									<td><label>${row.estimates}</label></td>
									<td><label>${row.status}</label></td>
									<td><label>${row.next_officer_cfmsid}</label></td>
									<td><label>${row.deptcode}</label></td>
							</c:forEach>

						</tbody>
					</table>

					<button type="button" class="btn btn-info" id="brodistbribution"
						onclick="showBudgetDistribution()">View CBRO Budget
						Distribution</button>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>

	<div class="modal fade" id="myModalclose">
		<div class="modal-dialog modal-l ">
			<div class="modal-content pad0">
				<p>${errorMsg}</p>

			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
	</div>
	<!-- /**********************************************************************************************************************************************************************************************/ -->
	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav2()">&times;</a>
		<a href="#">About</a> <a href="#">Services</a> <a href="#">Contact</a>
	</div>

	<%-- <c:if test="${sessionLogout=='Yes'}">
		   <script>
		location.href= '<%=basePath%>login';
		</script>
   </c:if>
 --%>



	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<script>
		$('#full-width').click(function() {
			$('#my-container').toggleClass('container container--fluid');
		});
	
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
		function openNav() {
			document.getElementById("myNav").style.width = "100%";
		}
	
		/* Close when someone clicks on the "x" symbol inside the overlay */
		function closeNav() {
			document.getElementById("myNav").style.width = "0%";
		}
	
	
		$(function() {
			$('.slimscroll-setting').slimScroll({
				height : '100vh'
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$('li.nav-item').click(function() {
				$('.nav-link.active').removeClass("active");
				$(this).addClass('active').siblings().removeClass('active');
	
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$('[data-toggle="popover"]').popover({
				trigger : 'hover'
			});
			$('.v-tab-head .v-tab-link').mouseover(tabHandler);
			$('.v-tab-head.v-tab-link').click(tabHandler);
		});
	
		var tabHandler = function(e) {
			e.preventDefault();
			var target = $($(this).data('target')),
				tabLink = $('.v-tab-link[data-target="' + $(this).data('target') + '"]');
			tabPanelToShow(tabLink);
			tabLinkToActivate(target);
	
		};
		var tabPanelToShow = function(elem) {
			$('.v-tab-link').removeClass('active').parent().find(elem).addClass('active');
		};
	
		var tabLinkToActivate = function(elem) {
			$('.v-tab-pane').children('div').removeClass('in').parent().find(elem).addClass('in');
		};
	</script>




	<script>
		function openNav2() {
			document.getElementById("mySidenav").style.width = "250px";
		}
	
		/* Set the width of the side navigation to 0 */
		function closeNav2() {
			document.getElementById("mySidenav").style.width = "0";
		}
	</script>

	<script>
		$(window).scroll(function() {
			if ($(this).scrollTop() > 10) {
				$('.header-fixed').addClass('fixed');
			} else {
				$('.header-fixed').removeClass('fixed');
			}
		});
	
		$(document).ready(function() {
			$("#myInput").on("keyup", function() {
				var value = $(this).val().toLowerCase();
				$(".budget_work_data tr").filter(function() {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
				});
			});
		});
	</script>
</body>
</html>