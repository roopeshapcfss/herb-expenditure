
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'logout.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript"> 
       /*  window.history.forward(); 
        function noBack() { 
            window.history.forward(); 
        }  */
    </script>
  </head>
  
  <body>
    <div class="boxed_wrapper">
	
	<br>
	<br>
	<%-- <div align="center">
       Un-Authorized Access
       <br>
       Click <a href="${pageContext.request.contextPath}/welcome">Here</a> to Login
    </div> --%>
    <div class="col-md-12" style="text-align: center;padding-top: 8%; ">
			<div class="row">
			<c:if test="${not empty message}">
	  			<h3 style="color: green;" class="message">${message}</h3>
	  		</c:if>
	  		<c:if test="${empty message}">
	  			<h1 style="color: #c52e1f;">You Have been Logged Out </h1>	
	  		</c:if>
				<h1 >
					To Logging again please Click here to <a href="${pageContext.request.contextPath}" >Re Login</a>....
				</h1>
			</div>
		</div>

</div>
  </body>
</html>
