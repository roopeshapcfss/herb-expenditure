<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>HERB</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet"/>
 	<link href="css/inner-page.css" rel="stylesheet"/>
	<link href="css/landing.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="57x57" href="img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<link rel="manifest" href="img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	 <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
  </head>
  <body>
   <div  style="padding:0px;margin:0px;">
 


<nav class="navbar navbar-inverse navbar-expand-lg navbar-light   navbar-fixed-top" style="padding:5px 10px;" >
   <a class="navbar-brand" href="#"><img src="assets/img/herb.svg" width="65px" /></a> 
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto pull-right">
      <li class="nav-item active">
        <a class="nav-link" href="#"><i class="fa fa-home"></i>&nbsp;Home  </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="login"><i class="fa fa-sign-in"></i>&nbsp;Login </a>
      </li>
     <!--  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li> -->
      
    </ul>
    
  </div>
</nav>
</div>
 

	<header class="  text-white"> <div class="container-fluid text-center" style="padding:0px;" >   	<img src="assets/img/landing.png" style="" class="img-fluid" />   </div>  </header>	 



<div class="container" style="background:#ffffff;padding:50px 10px; 100px 10px;height:700px;">
<div class="row">
<div class="col  col-sm-12 col-md-4 col-lg-4 col-xl-4">
	 
	 <div class="card  " style="width:100%;background:#09367b;min-height:275px;">
	 <h5 style=" ">Citizen Services</h5>
    <div class="card-body">
	 	<ul class="tree1">
		  <li><a href="#">Receipts Links</a>
		  <ul> 
		  				<li><a href="public/Receipts/PDChallan" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;PD Challan</a></li>
						<li><a href="public/Receipts/CashRecoveryChallan" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Cash Recovery Challan</a></li>
						<li><a href="public/Receipts/PensionRecovery" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Pension Recovery  </a></li>
						<li><a href="public/Receipts/SocialPensions" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Undisbursed Social Pensions  </a></li>
						<li><a href="#" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Expenditure Reimbursement  </a></li>
						<li><a href="public/Receipts/EmployeeChallan" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Employee Challan</a></li>
						<li><a href="public/Receipts/CPSContribution" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;CPS Contribution</a></li>
						<li><a href="public/Receipts/CitizenChallan" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Citizen Challan</a></li>
						<li><a href="public/Receipts/CivilDepositsChallan" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Civil Deposits</a></li>
						<li><a href="public/Receipts/CMReliefFund" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;CM Relief Fund</a></li>
						<li><a href="public/Receipts/JudicialRemittance" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Judicial Challan</a></li>
						<li><a href="public/Receipts/CapitalDevFund" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Capital Development Fund</a></li>
						<li><a href="public/Receipts/EscortCharges" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Escort Charges</a></li>
						<li><a href="public/Receipts/GuardCharges" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Guard Charges</a></li>
						<li><a href="public/Receipts/ChallanStatus" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Challan Status</a></li>
						<li><a href="#" target="_blank" class=""><i class="fa fa-angle-right"></i>&nbsp;Transaction Cancellation</a></li>
						</ul></li>
		</ul>
		
	 
		<ul class="tree1">
		<li><a href="#">Expenditure Links</a>
		<ul>
						<li><a href="#" target="_blank"><i class="fa fa-angle-right"></i>&nbsp;Bill Status</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-angle-right"></i>&nbsp;DDO Search</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-angle-right"></i>&nbsp;Beneficiary Account Statement </a></li>
						<li><a href="#" target="_blank"><i class="fa fa-angle-right"></i>&nbsp;Beneficiary Search</a></li> 
						<li><a href="#" target="_blank"><i class="fa fa-angle-right"></i>&nbsp;Payments</a></li>
			</ul>
			</li>
		</ul>
	 	<ul class="tree1">
			<li><a href="#">Budget</a>
			<ul>
					<li><a href="#"><i class="fa fa-angle-right"></i>&nbsp; Budget at Glance </a></li> 
				</ul>	
</li>				
		</ul>
		 
		
		<ul class="tree1">
		<li><a href="#">HR</a>
		<ul>
					<li><a href="#"><i class="fa fa-angle-right"></i>&nbsp; ORG Chart</a></li> 
			</ul>	
</li>			
		</ul>
	</div>
  </div>
	
</div>

<div class="col  col-sm-12 col-md-4 col-lg-4 col-xl-4" >

	<div class="card  " style="width:100%;background:#09367b;min-height:275px;">

	 <h5 style=" ">Department Services</h5>
    <div class="card-body">
		 
		<ul class="tree1">
		<li><a href="#">Budget</a>
		<ul>
						<li><a href="#" target="_blank"><i class="fa fa-angle-right"></i>&nbsp;Budget Allocated </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-angle-right"></i>&nbsp;Budget Proposal Status </a></li>
							</ul>
		</li>
		</ul>
	 
		<ul class="tree1">
		<li><a href="#">Expenditure</a>
		<ul>
						<li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; Department Expenditure </a></li>
						</ul>
		</li>
		</ul>
		
	 
		<ul class="tree1">
		<li><a href="#">Receipts</a>
<ul>
						<li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; Department Receipts </a></li>
						</ul>
						</li>
		</ul>
		
		 
		<ul class="tree1">
		<li><a href="#">HR</a>
<ul>
						<li><a href="#" target="_blank"><i class="fa fa-angle-right"></i>&nbsp;  My Department  </a></li>
						</ul>
</li>
		</ul>
		
		 
		<ul class="tree1">
		<li><a href="#">PD Accounts</a>
		<ul>	
						<li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; PD Accounts </a></li>
						</ul>
</li>
		</ul>
	</div>
  </div>
</div>

<div class="col col-sm-12 col-md-4 col-lg-4 col-xl-4">
 
<div class="card  " style="width:100%;background:#09367b;min-height:275px;">
<h5 style="">Employee & Pensioner Services</h5>
    <div class="card-body">
	 
		<ul class="tree1">
		<li><a href="#">	Employee Services</a>
<ul>
	
		 <li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; My Payslip </a></li>
		 <li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; Income for the Year </a></li>
		 <li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; My Loans </a></li>
		 </ul>
</li>
		</ul>
		
		 
		

		<ul class="tree1">
		<li><a href="#">Pensioner Services </a>
<ul>
		 <li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; Pension Slip </a></li>
		 <li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; Annual Income </a></li>
		 <li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; Pension Details  </a></li>
		  <li><a href="#" target="_blank"> <i class="fa fa-angle-right"></i>&nbsp; Pension Portal </a></li>
		 </ul>
		 </li>
		</ul>
		
		 
	</div>
  </div>
</div>
</div>
</div>



 <footer>
  <div class="copyright">
    <p>&copy 2021 - Designed and developed by <a href="#" style="color:orange;"> APCFSS </a> </p>
  </div>
  <div class="social">
    <a href="#" class="support">Contact Us</a>
    <a href="#" class="face">f</a>
    <a href="#" class="tweet">t</a>
    <a href="#" class="linked">in</a>
  </div>
</footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script> 
	<script>
	$(document).ready(function() {
        // Transition effect for navbar 
			$(window).scroll(function() {
			  // checks if window is scrolled more than 500px, adds/removes solid class
			  if($(this).scrollTop() > 500) { 
				  $('.navbar').addClass('solid');
			  } else {
				  $('.navbar').removeClass('solid');
			  }
			});
	});
	
	
	/*************************************************/
	
 
	</script>

<script>
 $.fn.extend({
    treed: function (o) {
      
      var openedClass = 'fa-minus-circle';
      var closedClass = 'fa-plus-circle';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator fa " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('.tree1').treed();

$('.tree2').treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});

$('.tree3').treed({openedClass:'glyphicon-chevron-right', closedClass:'glyphicon-chevron-down'});

  </script>
  </body>
</html>