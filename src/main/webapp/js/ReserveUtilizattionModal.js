/**
 * Js For modal pop up selections 
 */

var objectId;
var estimatesId;
$(document).ready(function(){
	
	/*$('#hoaTableId tbody tr').each(function() {
		var 
		
		$($(this).find("input").eq(1).val()).hide();
	});*/
	
	
	$("#departmentId").click(function(){ 
		$("#departmentModal").modal("show");
		$("#inputValidation").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$('.list-group-flushlevel').find('a').filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			});
		});
	});
	



	$("#departmentModal").on('hidden.bs.modal', function(){
		modalOptionDept(obj);
	});
	
	$(".hoaModalField").click(function(){ 
		
		var deptCode=$("#departmentId").val().trim();
		if(!isValidData(deptCode)){
			if($(this).parent().next('.style-for-table-warning').length==0)
				$(this).parent().append('<div class="style-for-table-warning">Please Select Department</div>');
		}
		else
		{
			objectId=$(this).attr("id");
			estimatesId=$(this).closest('tr').find('input').eq(1).attr('id');
			
			$("#hoaModal").modal("show");
			
		
			
			$('.hideDetailsOfHOAClass').show();
			$('#hoaTableId tbody tr').each(function() {
				$("#"+$(this).find("input").eq(0).val()).hide();
			});
			
			$("#inputValidation").on("keyup", function() {
				var value = $(this).val().toLowerCase();
				$('.list-group-flushlevel').find('a').filter(function() {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
				});
			});
		}
		
	});
	
	$("#hoaModal").on('hidden.bs.modal', function(){
		modalOptionHoa(obj);
	});
	
});

function RUmodalOptionDept(obj){
	var depts = $(obj).text().split('-');
	$('input[name="department"]').val(depts[0].trim());
	$("#department_text").html(depts[1].trim());
	$('input[name="departmentDesc"]').val(depts[1].trim());
	$('input[name="secrateriate"]').val(depts[1].trim());
	$('input[name="deptCode"]').val($(obj).attr("data-id"));
	var hoaUrl="loadResource/ReserveutilloadHoaWithDept" + '?key=ajax&deptCode='+$(obj).attr("data-id");
	
	loadAjaxSingleRowValue(hoaUrl,"hoaLoadId","hoaId","id","value","modalOptionHoa(this)");
}
function modalOptionHoa(obj){
	var hoa = $(obj).text().trim();
	var deptCode=$('input[name="deptCode"]').val();
	objectId="#"+objectId;
	$(objectId).val(hoa);
	$('#loadAmount').show();
	$('#loadReserveAmount').show();
	//$('#2401001140610310312VN').show();
	//alert("test");
	
	
	var estimatesUrl="loadResource/loadEstimates" + '?key=ajax&deptCode='+deptCode+'&hoa='+hoa;
	loadAjaxSetValues(estimatesUrl);
	
}

function loadAjaxSingleRowValue(url,target,id,value,label,childFunction){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) {
			theResponse = JSON.parse(res);

			var a = "";
			for(var i = 0;i <= theResponse.length-1;i++){
				a += "<a  class=\"list-group-item list-group-item-action hideDetailsOfHOAClass\" id=\""+theResponse[i][value]+"\" " +
						"  data-dismiss=\"modal\" style=\"border-bottom: 1px solid rgba(0,0,0,.125);\" onclick=\"return "+childFunction+"\"  data-id=\""+theResponse[i][value]+"\" href=\"javascript:void(0)\"> <b>"+theResponse[i][value]+"</b> </a> ";
			}
			var lableCount=target+"Label";
			$("#"+lableCount).html(theResponse.length);
			$('#'+target).html(a);
		},
		error : function() {
			alert('Error occured');
		}
	});
}
function loadAjax(url,target,id,value,label,childFunction){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) {
			theResponse = JSON.parse(res);
			var a = "";
			for(var i = 0;i <= theResponse.length-1;i++){
				a += "<a  class=\"list-group-item list-group-item-action\" id=\""+id+"\"  data-dismiss=\"modal\" style=\"border-bottom: 1px solid rgba(0,0,0,.125);\" onclick=\"return "+childFunction+"\"  data-id=\""+theResponse[i][value]+"\" href=\"javascript:void(0)\"> <b>"+theResponse[i][value]+"</b>-<br>"+theResponse[i][label]+" </a> ";
			}
			var lableCount=target+"Label";
			$("#"+lableCount).html(theResponse.length);
			$('#'+target).html(a);
		},
		error : function() {
			alert('Error occured');
		}
	});
}
function loadSingleAjaxValue(url,target,value,secondValue){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) {
			theResponse = JSON.parse(res);
			var a = "";
			for(var i = 0;i <= theResponse.length-1;i++){
				console.log(theResponse[i]);
				a += theResponse[i][value];
			}
			$('input[name='+value+']').val(a);
			$('input[name='+secondValue+']').val(a);
			$('#'+target).html(a);
		},
		error : function() {
			alert('Error occured');
		}
	});

}
function loadAjaxSetValues(url,target){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) 
		{
			theResponse = JSON.parse(res);
			console.log(theResponse[0].estimates);
			$('#'+target).val(theResponse[0].estimates);
		},
		error : function() {
			alert('Error occured');
		}
	});

}