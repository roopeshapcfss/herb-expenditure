//Payment Mode alerts
/*bs4Toast.warning,bs4Toast.primary,bs4Toast.error*/
	 function dangerT(){ bs4Toast.warning('Manual challan', 'Please take the printout of Challan Reference Form and approach any SBI branch to remit the amount either by way of Cash / Cheque / DD.',{ delay: 10000});}
	 function rtgs(){ bs4Toast.warning('RTGS/NEFT(Including Manual)', 'Please register the CFMS ID as Bank Account Number along with IFSC Code mentioned in the challan reference form as Payee to make the Online Payment. You may also approach any Bank to remit the amount in the form of Cash / Cheque or DD against the CFMS ID.',{ delay: 10000});}
	 function ePayment(){ bs4Toast.warning('e-Pay challan', 'Please select the Payment Gateway i.e. either SBI MOPS or PayU to remit the amount through Internet Banking, Debit Card or Credit Card.',{ delay: 10000});}


function checkPayment(obj){

var value=$(obj).val();
if(value=='manual'){
 dangerT();
}
else if(value=='rtgsNeft'){
rtgs();
}
else if (value=='ePayment'){
ePayment();
}
}

// Converts Amount to Words
function amount(){
	
	var words="";
	    $("#amountinRs").on("keydown keyup change", per());
	 function per() {
		 $('#paymentModes').show();
		 $('#rowhoa').show();
	 var totalamount = Number($("#amountinRs").val()) ;
	 $("#amountinRs").val(totalamount);
	 words = toWordsconver(totalamount);
	 $("#amountWords").val(words + "Rupees Only");
	 }
	}
	
		function submitData() 
		{
//			$('#cover-spin').show();
//			$('.estimatesClass').prop("disabled", false);
//			$('#hoaTableId tbody tr').each(function()
//			{
//				var beAmount=parseInt($(this).find("input").eq(2).val());
//				var estimateAmount=parseInt($(this).find("input").eq(1).val());
//				if(beAmount > estimateAmount  || beAmount==0 || beAmount==0.0)
//				{
//				alert("Invalid BE Amount");	
//				$(this).find("input").eq(2).val('');	
//				return false;
//				}
//			});
			
			validateForm("formId",function(value) {	
				if(value==0)
				{
					   $('#goTbody').find("tr").each(function(index) 
							{
							for(var i=0;i<3;i++)
							{
							var name =$(this).find("input").eq(i).attr("name");
							var value=name.substring(name.indexOf("[")+1,name.indexOf("]"));
							$(this).find("input").eq(i).attr("name",name.replace("["+value+"]","["+(index)+"]"));
							}
							});	
							
							$('#toDoListBody').find("tr").each(function(index) 
							{
							for(var i=0;i<1;i++)
							{
							var name =$(this).find("input").eq(i).attr("name");
							var value=name.substring(name.indexOf("[")+1,name.indexOf("]"));
							$(this).find("input").eq(i).attr("name",name.replace("["+value+"]","["+(index)+"]"));
							}
							});
							document.forms[0].submit();
					
				}
			});
			
			
			return false;
			
		}
	/*function submitData() {
		validateForm("formId",function(value) {	
		//alert(value);
			if(value==0){
							document.forms[0].submit();
			}
			});
				return false;
				}*/
	function submitDataDetail() {
		var paymentMode=$('input[name="paymentMode"]:checked').val();
		validateForm("formId",function(value) {	
			if(value==0)
			{
			if(!isValidData(paymentMode))
			{
				$('#paymentErrId').show();
				return false;
			}	
			else
			{
				$('#paymentErrId').hide();
				if(paymentMode=='ePayment')
				{
				$("#submitModal").modal("show");
				}
				else{
					$("#typeOfGateway").val(paymentMode);
					document.forms[0].submit();
				}
			}
			
			}
			});
				return false;
				}
	function submitDataDetailOnlinePayment(typeOfGateway) 
	{
	$("#typeOfGateway").val(typeOfGateway);

		validateForm("formId",function(value) 
		{	
			if(value==0)
			{
			document.forms[0].submit();
			}
		});
				return false;
	}
//Autocomplete off for input fields

var countAppendDiv=0;

function cloneDiv(id,value) 
{
	if (value == 'add') 
	{
		$('#removeId').show();
		countAppendDiv = countAppendDiv + 1;

		
		var estimatediv=$('#estimatesId').clone(true).attr('id');
		
		
		//$('#estimatesId').clone(true).attr('id', 'estimatesId'+ countAppendDiv);
		
		
		var $clone = $('#cloneId').clone(true);
		var id = $clone.prop('id');
		$clone.prop('id', id + countAppendDiv);


		
		$clone.find('input[type=text],select').each(function(index) {
			var id = $(this).attr('id');
			id = id + countAppendDiv;
			var name= $(this).attr('name');
			$(this).prop("id", id);
			$(this).prop("name", name.replace("[0]","["+countAppendDiv+"]"));
			$(this).val('');
		});
		

		$clone.find('#cloneDivWithButtonsId').remove();
		var cloning = "";
		if (countAppendDiv == 1) 
		{
			cloning = "#cloneId";
		} 
		else 
		{
			cloning = "#cloneId" + (countAppendDiv - 1);
		}
		$(cloning).after($clone);
	} 
	else 
	{
		id = id + countAppendDiv;
		$('#' + id).remove();
		countAppendDiv--;
		if (countAppendDiv == 0) 
		{
			$('#removeId').hide();
		}
	}
	
	
	
}


//Regenerate Captcha
function getcapctha() {
	$.ajax({
	type: 'POST',
	url: "getCaptcha",  
	dataType:"html",
	async:false,
	success: function(res){
	$("#captchaid").val(res);
	},
	error: function() {
	alert('Error occured');
	}
	});
	}

//Disable Select Input Fields
$('#departmentId').keydown(function(event) {
	var key = event.charCode || event.keyCode || event.which; 
	var char = String.fromCharCode(event.key);
	if( key === 8 || key=== 46 ){
		event.preventDefault();
		return false;
	}
    return false;
});

//datatable
$(document).ready(function() {
    $('#example').DataTable();
} );


$(document).ready(function(){
	
	   $('.form-control').attr('autocomplete', 'off');
	   
	   
	   /*   $('.datePickerClass').datepicker({ 
	        format: "dd-mm-yyyy" , 
	         container: '#custom-pos',
	         autoclose: true,
	    startDate: new Date(),
	    setDate: new Date(),
	    todayHighlight: true
	   }); 
	        */
	
	
	 
	    $('.pastereplacesingleQuotesandDouble').bind('paste', function (e){
	           var ctl = $(this);
	                setTimeout(function() {
	                var val=$(ctl).val();
	                val=val.replace(/'/g, '').replace(/"/g, '') ;
	                $(ctl).val(val);
	             
	                }, 100);
	
	
	     });     
});
function  isValidData(data)
{
	
 var flag=true;
if(data==null || data==undefined || data=="" || data=='null' )
{
flag=false;
}
return flag;
}
var dataTableID;
$( document ).ready(function() {
	
		if($('#example').length >0)
		{
		 dataTableID = $('#example').DataTable( {
	        responsive: true
	    } );
	 
	    new $.fn.dataTable.FixedHeader( table );	
		}
	
});

$( document ).ready(function() {
	
	$( ".mandatoryField" ).focusout(function() {
	
				if( isValidData($(this).val()) )
				{
				$(this).parent().parent().find('.style-for-warning').remove();
				}
				/*else{
					$(this).parent().after('<div class="style-for-warning">Required</div>');	
				}*/
		
	});
	$( ".mandatoryTableField" ).focusout(function() {
		
		if( isValidData($(this).val()) )
		{
			$(this).next().remove();
		}
		
		
		
	});
	
	
	$('input').each(
		    function(index){  
		        var input = $(this);
		        input.attr("autocomplete", "off");
		    }
		);
	
});

function validateForm(formId,callback)
{
	
	var countErrors=0;
	 $('.mandatoryField') .each(function() {
			$(this).parent().parent().find('.style-for-warning').remove();
		 	if(!isValidData($(this).val()) )
			 {
		 		 console.log($(this).attr("id")+" "+$(this).attr("name")+" "+$(this).val() );
		 		 countErrors++;
		 		$(this).parent().after('<div class="style-for-warning">Required</div>');
		 	 }
		 	
	 });
	 
	 
	 $('.mandatoryTableField') .each(function() {
			$(this).next().remove();
		 	if(!isValidData($(this).val()) )
			 {
		 		 console.log($(this).attr("id")+" "+$(this).attr("name")+" "+$(this).val() );
		 		 countErrors++;
		 		$(this).parent().append('<div class="style-for-table-warning">Required</div>');
		 	 }
	 });
	 
	return callback(countErrors);
}

function checkMobileNumber(mbl)
{
	if(mbl.value.charAt(0)!='8'&&mbl.value.charAt(0)!='9'&&mbl.value.charAt(0)!='7' && mbl.value.charAt(0)!='6')
		{
		$(mbl).parent().after('<div class="style-for-warning">Mobile Number shoud start with 9/8/7/6</div>');
		mbl.focus();
		mbl.value="";
		}
		else if(mbl.value.length!=10)
		{
		$(mbl).parent().after('<div class="style-for-warning">Mobile Number shoud Contain 10 digits</div>');	
		mbl.focus();
		mbl.value="";
		}
	}
function checkEmail(obj) {
	$(obj).parent().parent().find('.style-for-warning').remove();

	if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(obj.value)){
	$(obj).parent().after('<div class="style-for-warning">Please Enter Valid E-mail</div>');
	$(obj).val('');
}
}
function refreshCaptcha(){
var theResponse = null;
	$.ajax({
	type : 'GET',
	url : "loadResource/getCaptcha" + '?key=ajax',
	dataType : "html",
	async : false,
	success : function(res) {
	var response=res.split('@@@@@@@@');
	$("#captchaid").attr('src',"data:image/png;base64,"+response[0]);
	},
	error : function() {
	alert('Error occured');
	}
});
}
function intOnly(i) {
	$(i).parent().parent().find('.style-for-warning').remove();
	if((i.value).length>0) {
	$(i).parent().after('<div class="style-for-warning">Enter only Digits</div>');
		i.value = i.value.replace(/[^\d\d.\d]+/g, '');
		}
}
function loadHoaData(obj)
{
	 $.post("Budget/HoaDesc", {hoaNo: $(obj).attr('data-id')}, 
	   function(result)
	   {
	 	$('#hoaDetailsModal').modal('show');	
	 	$('#hoaLoadModal').html(result);
       });
}
