$(document).ready(function(){
	$('#todateId').val('31/12/9999');
	$('#positionTextId').click(function(){
		$('#positionModal').modal('show');
		$('#inputValidation').on('keyup',function(){
			var value = $(this).val().toLowerCase();
			$('.list-group-flushlevel').find('span').filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			});
		});//end of keyup function
	});//end of click function
	
	$(".todateId").datepicker({
	  	/*showOtherMonths: true,
		selectOtherMonths: true,
		changeMonth: true,	
	 	changeYear: true,
		numberOfMonths: 3,
		showButtonPanel: true*/
		setDate: "31/12/9999",
		language : "ru",
		autoclose : true,
		todayHighlight : true,
		format : "dd/mm/yyyy",

		startDate : new Date('01/31/1995'),
		endDate : new Date('output')
	});
	
	$('#searchBox').on('keyup',function(){
		var value = $(this).val().toLowerCase();
		t=$('.hoaValue');
		$('.list-group-item').find('span').filter(function() {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
		});
	});//end of keyup function
	
	$('.checkAll').change(function(){
			$('.checkItem').prop('checked', $(this).prop('checked'))
	});//end of change function
	$('.checkItem').change(function(){
		if($(this).prop('checked')==false){
			$('.checkAll').prop('checked', false);
		}
		if($('.checkItem:checked').length == $('.checkItem').length){
			$('.checkAll').prop('checked', true);
		}
	});//end of change function
	
	$('.search').keyup(function(){
		
		var searchTerm=$(this).val();
		searchHoaFunction(searchTerm,function(value){
		});
			
			
		
		
		//searchList($(this).val());
	});//end Of keyup function
	
	$("#hoaModal").on('hidden.bs.modal', function(){
		var list = [];
		$('#hoaModal .checkItem:checked').each(function(){
		    list.push($(this).val());
		});
		$('#hoaId').val(list.toString());
		$('#hoaTextId').val(list.toString());
	});
	
	$("#orgUnitModal").on('hidden.bs.modal', function(){
		var list = [];
		$('#orgUnitModal .checkItem:checked').each(function(){
		    list.push($(this).val());
		});
		$('#orgId').val(list.toString());
		$('#orgTextId').val(list.toString());
		
	});
	
});//end of ready function


$(function () {
	  var dt = new Date();
		var month = dt.getMonth() + 1;
		var day = dt.getDate();
		var output = (day < 10 ? '0' : '') + day + '/' + (month < 10 ? '0' : '') + month + '/' + dt.getFullYear();
$("#fromdateId").val(output);
});

function positionModalOption(obj){
	var positionDetails = $(obj).children(".positionDetails").text().split('@');
	
	$('#positionId').val(positionDetails[0].trim());
	$('#positionTextId').val(positionDetails[0].trim()+' '+positionDetails[1].trim());
	$('#employeeId').val(positionDetails[2].trim()+' '+positionDetails[3].trim());
	
}

function searchList(value){
	$('.hoaList .hoaListItem').each(function(){
		$(this).show();
	});
	if(value.trim()!=""){
	$('.hoaList .hoaListItem').each(function(){
		var found = false;
		$(this).each(function(){
			t = $(this).text().trim();
			f = $(this).text().trim().toLowerCase().indexOf(value.toLowerCase());
			if($(this).text().trim().toLowerCase().indexOf(value.toLowerCase())>=0){
				found = true;
			}
		});//end of each function
		if(found == 'true'){
			$(this).show();
		}
		else{
			$(this).hide();
		}
	});//end of each function
	}
}

function searchHoaFunction( searchTerm, callback )
{
	$('.hoaDivclass').show();
	$('.hoaDivclass').each(function(){
		hoa=$(this).find("span").text();
		if(hoa.indexOf(searchTerm)!=-1)
		{
		$(this).show();	
		}
		else{
			$(this).hide();	
		}

	});

	return callback(true);

	}

//function checKAll(obj){
//	var parentModal =$(obj).closest('.modal');
////	$('parentModal .checkAll').change(function(){
//		$('parentModal .checkItem').prop('checked', $(this).prop('checked'))
////	});//end of change function
//	$('parentModal .checkItem').change(function(){
//		if($(this).prop('checked')==false){
//			$('parentModal .checkAll').prop('checked', false);
//		}
//		if($('parentModal .checkItem:checked').length == $('.checkItem').length){
//			$('parentModal .checkAll').prop('checked', true);
//		}
//	});//end of change function
//}
//
//function checkItem(obj){
//	debugger;
//	var parentModal =$(obj).closest('.modal');
//	var checkedElement=parentModal+' '+obj;
////	$('parentModal .checkItem').change(function(){
//		if($(this).prop('checked')==false){
//			$('parentModal .checkAll').prop('checked', false);
//		}
//		if($('parentModal .checkItem:checked').length == $('.checkItem').length){
//			$('parentModal .checkAll').prop('checked', true);
//		}
////	});//end of change function
//}
