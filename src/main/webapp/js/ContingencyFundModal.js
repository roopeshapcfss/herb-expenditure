var objectId;

$(document).ready(function() {
	$(".hodModalField").click(function() {
		
		var deptCode=$('input[name="receiverHOD"]').val().trim();
		if(!isValidData(deptCode)){
			if($(this).next('.style-for-table-warning').length==0)
				$(this).parent().append('<div class="style-for-table-warning">Please Select Department</div>');
		}
		else
		{
			$(this).next('.style-for-table-warning').remove();
			objectId=$(this).attr("id");
			
		$("#hodModal").modal("show");
		
		$("#inputValidation").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$('.list-group-flushlevel').find('a').filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			});
		});//modal
		}
	});//click
	
	$(".receiverHOAField").click(function() {
		$("#receiverHOAModal").modal("show");
		$("#inputValidation").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$('.list-group-flushlevel').find('a').filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			});
		});//modal
	});//click
}); //ready function

function modalOptionDepartment(obj){
	var depts = $(obj).text().split('-');
	$('input[name="receiverHOD"]').val(depts[0].trim());
	$("#receiverHOA_text").html(depts[1].trim());
	$('input[name="deptCode"]').val(depts[0].trim());	
	var hoaUrl="loadResource/contingencyFundloadHoaWithDept" + '?key=ajax&deptCode='+$(obj).attr("data-id");
	
	loadAjaxSingleRowValue(hoaUrl,"hoaLoadId","hoaId","id","value","modalOptionHoaD(this)");
}

$("#receiverHOAModal").on('hidden.bs.modal', function(){
	modalOptionDepartment(obj);
});

$("#hoaModal").on('hidden.bs.modal', function(){
	modalOptionHoaD(obj);
});

function loadAjaxSingleRowValue(url,target,id,value,label,childFunction){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) {
			theResponse = JSON.parse(res);

			var a = "";
			for(var i = 0;i <= theResponse.length-1;i++){
				a += "<a  class=\"list-group-item list-group-item-action hideDetailsOfHOAClass\" id=\""+theResponse[i][value]+"\" " +
						"  data-dismiss=\"modal\" style=\"border-bottom: 1px solid rgba(0,0,0,.125);\" onclick=\""+childFunction+"\"  data-id=\""+theResponse[i][value]+"\" href=\"javascript:void(0)\"> <b>"+theResponse[i][value]+"</b> </a> ";
			}
			var lableCount=target+"Label";
			$("#"+lableCount).html(theResponse.length);
			$('#'+target).html(a);
		},
		error : function() {
			alert('Error occured');
		}
	});
}
	
	function modalOptionHoaD(obj){
		/*var hoa = $(obj).text().trim();
		var deptCode=$('input[name="deptCode"]').val();
		alert(deptCode);
//		$('input[name="ContingencyFundDetailsBeanList[0].hod"]').val(hoa);
		$('#hoaID').val(hoa);*/
		
		var hoa = $(obj).text().trim();
		var deptCode=$('input[name="deptCode"]').val();
		objectId="#"+objectId;
		$(objectId).val(hoa);
		
	}