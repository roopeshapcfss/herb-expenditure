/**
 * Js For modal pop up selections 
 */

var objectId;
var estimatesId;
$(document).ready(function(){

	$("#hodId").click(function(){ 
		$("#hodModal").modal("show");
		$("#inputValidation").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$('.list-group-flushlevel').find('a').filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			});
		});
	});

	$("#hodModal").on('hidden.bs.modal', function(){
		modalOptionhod(obj);
	});
	
	$("#hoaId").click(function(){ 
		
		var hodCode=$("#hodId").val().trim();
		if(!isValidData(hodCode)){
			if($(this).parent().next('.style-for-table-warning').length==0)
				$(this).parent().append('<div class="style-for-table-warning">Please Select Department</div>');
		}
		else
		{
			objectId=$(this).attr("id");
			estimatesId=$(this).closest('tr').find('input').eq(1).attr('id');
			
			$("#hoaModal").modal("show");
			$('.hideDetailsOfHOAClass').show();
			$('#hoaTableId tbody tr').each(function() {
				$("#"+$(this).find("input").eq(0).val()).hide();
			});
			
			$("#inputValidation").on("keyup", function() {
				var value = $(this).val().toLowerCase();
				$('.list-group-flushlevel').find('a').filter(function() {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
				});
			});
		}
		
	});
	
	$("#AddhoaModal").on('hidden.bs.modal', function(){
		modalOptionHoa(obj);
	});
	
});

function modalOptionhod(obj){
	var depts = $(obj).text().split('-');
	$('input[name="hod"]').val(depts[0].trim());
	$("#hod_text").html(depts[1].trim());
	$('input[name="description"]').val(depts[1].trim());
	$('input[name="secrateriate"]').val(depts[1].trim());
	$('input[name="hodCode"]').val($(obj).attr("data-id"));
	var hoaUrl="loadResource/hodSurrenderloadHoaWithDept" + '?key=ajax&hodCode='+$(obj).attr("data-id");
	
	loadAjaxSingleRowValue(hoaUrl,"hoaLoadId","hoaId","id","value","hodsurrmodalOptionHoa(this)");
}
function hodsurrmodalOptionHoa(obj){
	var hoa = $(obj).text().trim();
	var hodCode=$('input[name="hodCode"]').val();
	objectId="#"+objectId;
	$(objectId).val(hoa);
	//var estimatesUrl="loadResource/additionloadEstimates" + '?key=ajax&hodCode='+hodCode+'&hoa='+hoa;
	//loadAjaxSetValues(estimatesUrl,estimatesId);
}

function loadAjaxSingleRowValue(url,target,id,value,label,childFunction){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) {
			theResponse = JSON.parse(res);

			var a = "";
			for(var i = 0;i <= theResponse.length-1;i++){
				a += "<a  class=\"list-group-item list-group-item-action hideDetailsOfHOAClass\" id=\""+theResponse[i][value]+"\" " +
						"  data-dismiss=\"modal\" style=\"border-bottom: 1px solid rgba(0,0,0,.125);\" onclick=\"return "+childFunction+"\"  data-id=\""+theResponse[i][value]+"\" href=\"javascript:void(0)\"> <b>"+theResponse[i][value]+"</b> </a> ";
			}
			var lableCount=target+"Label";
			$("#"+lableCount).html(theResponse.length);
			$('#'+target).html(a);
		},
		error : function() {
			alert('Error occured');
		}
	});
}
function loadAjax(url,target,id,value,label,childFunction){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) {
			theResponse = JSON.parse(res);
			var a = "";
			for(var i = 0;i <= theResponse.length-1;i++){
				a += "<a  class=\"list-group-item list-group-item-action\" id=\""+id+"\"  data-dismiss=\"modal\" style=\"border-bottom: 1px solid rgba(0,0,0,.125);\" onclick=\"return "+childFunction+"\"  data-id=\""+theResponse[i][value]+"\" href=\"javascript:void(0)\"> <b>"+theResponse[i][value]+"</b>-<br>"+theResponse[i][label]+" </a> ";
			}
			var lableCount=target+"Label";
			$("#"+lableCount).html(theResponse.length);
			$('#'+target).html(a);
		},
		error : function() {
			alert('Error occured');
		}
	});
}
function loadSingleAjaxValue(url,target,value,secondValue){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) {
			theResponse = JSON.parse(res);
			var a = "";
			for(var i = 0;i <= theResponse.length-1;i++){
				console.log(theResponse[i]);
				a += theResponse[i][value];
			}
			$('input[name='+value+']').val(a);
			$('input[name='+secondValue+']').val(a);
			$('#'+target).html(a);
		},
		error : function() {
			alert('Error occured');
		}
	});

}
function loadAjaxSetValues(url,target){
	$.ajax({
		type : 'GET',
		url : url,
		dataType : "html",
		async : false,
		success : function(res) 
		{
			theResponse = JSON.parse(res);
			console.log(theResponse[0].estimates);
			$('#'+target).val(theResponse[0].estimates);
		},
		error : function() {
			alert('Error occured');
		}
	});

}