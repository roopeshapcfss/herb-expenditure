(function(){
	var app = angular.module("initiateBroBeApp",[]);
	
	app.controller("initiateBroBeCtrl",function($scope,$http){
		$scope.hoasTableLength = [0];
		$scope.remarks = "";
		$scope.department_desc = "";
		$scope.secrateriate = "";
		$scope.Data = {
			year : "2020",
			department : "",
			funds_re_validation : "",
			reference : "",
			realease_order : "",
			
			order_desc : "In pursuance of the orders issued in references read above, the commissioner",
			issued_by : "is hereby issued a",
			bro_category : "from the BE provision 2020-21",
			order_bot_desc : "in relaxation of quarterly regulation towards meeting the expenditure under the following schemes.",
			sec_order_desc : "Shall take necessary action for issue of administrative sanction as per instructions issued in U.O Note No. 29875-A/1283/A1/BG.I/2006, Finance (BG.I) Department, dt.25.11.2006.",
			order_for : "Budget Release Order For an amount of",
			hoasData : [
				{
					hoa : "",
					withDrawlProcedure : "",
					be_amount : "",
					remarks : ""
				}
			],
			goDetails : [],
			toList : []
		};
		$scope.selectedHOAs = [];
		$scope.obj = {};
		$scope.onDeptSelect = function(obj){
			console.log(obj);
			$scope.Data.department = obj.deptcode;
			$scope.department_desc = obj.description_long
			
			$http.get("initiateBroBe/sec/"+obj.deptcode).then(function(res){
				$scope.secrateriate = res.data.description_long;
			})
		}
		
		$scope.deptData = []
		$scope.openModel = function(){
			$http.get("initiateBroBe/departmentMst").then(function(res){
				var status = res.status;
				if(status === 200){
					console.log(res.data);
					$scope.deptData = res.data; 
					
					$("#exampleModalCenter").modal("show");
					$("#inputValidation").on("keyup", function() {
						var value = $(this).val().toLowerCase();
						$('.list-group-flush').find('a').filter(function() {
							$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
						});
					});
				}else{
					swal({
		    		  title: "Invalid Request.Please try again",
		    		  icon: "warning",
		    		})
		    		.then(function(){
		    			location.reload();
		    		});
				}
				
			}).catch(function(onError) {
				if(onError.status === 403){
					swal({
		    		  title: "Session expired.Please login again",
		    		  icon: "warning",
		    		})
		    		.then(function(){
		    			location.reload("/");
		    		});  
				}
				console.log(onError.status);
			});
			
		}
		
		$scope.addRowsToHoasTable = function(){
			$scope.Data.hoasData.push({
				hoa : "",
				withDrawlProcedure : "",
				be_amount : "",
				remarks : ""
			});
		}
		
		$scope.clearRemarks = function(id){
			$scope.Data.hoasData[id].remarks ="";
		}
		
		$scope.deleteRow = function(id){
			var temp = [];
			for(var a of $scope.Data.hoasData){
				if(a != id){
					temp.push(a);
				}
			}
			$scope.Data.hoasData = temp;
		}
		$scope.HOAsList = []
		$scope.hoaIndex = 0;
		$scope.getHOAs = function(index){
			if($scope.Data.department != null && $scope.Data.department != ""){
				hoa = [];
				for(var i in $scope.Data.hoasData){
					if($scope.Data.hoasData[i].hoa !== ""){
						hoa.push($scope.Data.hoasData[i].hoa);
					}
				}
				if(hoa.length == 0){
					$http.get("initiateBroBe/hoas/"+$scope.Data.department).then(function(res){
						$scope.HOAsList = res.data; 
						$("#HOAinputValidation").on("keyup", function() {
							var value = $(this).val().toLowerCase();
							$('.list-group-flush').find('a').filter(function() {
								$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
							});
						});
					});
				}else{
					$http({
				        url: 'initiateBroBe/hoas?dept='+$scope.Data.department,
				        method: "POST",
				        data: hoa
				    }).then(function(res) {
				    	$scope.HOAsList = res.data; 
						$("#HOAinputValidation").on("keyup", function() {
							var value = $(this).val().toLowerCase();
							$('.list-group-flush').find('a').filter(function() {
								$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
							});
						});
				    })
				}
			}
			$scope.hoaIndex = index;
			$("#HOAModalCenter").modal("show");
		}
		
		
		$scope.onHOASelect = function(hoa){
			$scope.Data.hoasData[$scope.hoaIndex].hoa = hoa;
			$http.get("initiateBroBe/estimates/"+$scope.Data.department+"/"+hoa).then(function(res){
				$scope.Data.hoasData[$scope.hoaIndex].estimates = res.data.estimates
				console.log(res.data);
			})
		}
		
		$scope.initGosList = function(){
			$http.get("initiateBroBe/goRefs").then(function(res){
				$scope.Data.goDetails = res.data;
			})
		}
		
		$scope.initToList = function(){
			$http.get("initiateBroBe/toList").then(function(res){
				$scope.Data.toList = res.data;
			})
		}
		
		$scope.deleteGOsRow = function(obj){
			var temp = [];
			for(var a of $scope.Data.goDetails){
				if(a != obj){
					temp.push(a);
				}
			}
			$scope.Data.goDetails = temp;
		}
		
		$scope.addToListRows = function(){
			$scope.Data.toList.push({tolist : ""});
		}
		
		$scope.addGosListRows = function(){
			$scope.Data.goDetails.push({
				go : "",
				description : "",
				go_date : "",
			});
		}
		
		$scope.deleteTolistRow = function(obj){
			var temp = [];
			for(var a of $scope.Data.toList){
				if(a != obj){
					temp.push(a);
				}
			}
			$scope.Data.toList = temp;
		}
		
		$scope.submitData = function(){
				$http({
			        url: 'initiateBroBe',
			        method: "POST",
			        data: $scope.Data
			    }).then(function(response) {
			    	var status = response.status;
			    	if(status === 201){
			    		swal({
			    		  title: "Data Created Successfully",
			    		  text: response.data.bro,
			    		  icon: "success",
			    		})
			    		.then(function(){
			    			location.reload();
			    		});
			    	}else if(status === 500){
			    		swal({
			    		  title: "Error Creating Data.Please try Again",
			    		  icon: "danger",
			    		})
			    		.then(function(){
			    			location.reload();
			    		});
			    		
			    	}
			    }).catch(function onError(error) {
			    	swal({
		    		  title: "Error Creating Data.Please try Again",
		    		  icon: "error",
		    		})
		    		.then(function(){
		    			location.reload();
		    		});        
			    });
			  
		}
		
		$scope.validateAmount = function(id,obj){
			be_amount0
			if(document.getElementById(obj) != null && document.getElementById(obj).value != null && document.getElementById(obj).value != ""){
				var amount = document.getElementById(obj).value;
				var rgx = /^[0-9]*\.?[0-9]*$/;
				var flag = rgx.test(amount);
				if(flag == true){
					if(parseFloat(amount) > parseFloat($scope.Data.hoasData[id].estimates)){
						document.getElementById(obj).value = "";
						swal({
			    		  title: "Invalid Amount",
			    		  icon: "warning",
			    		})
			    		.then(function(){
			    			document.getElementById(obj).value = "";
			    		});
						//alert("Invalid Amount")
					}
				}else{
					document.getElementById(obj).value = "";
				}
			}
		}
		
	})
})();


