/* hoaModalField
hoaModal
fromDDO
fromDDOField
fromDDOModal */

    var objectId;
    var amountText;
    var availableAmount;
    var rfcIf;
    var descriptionId;

    $(document).ready(function() {
        $(".hoaModalField").click(function() {

            var deptCode = $('input[name="fromDDO"]').val().trim();
            if (!isValidData(deptCode)) {
                if ($(this).next('.style-for-table-warning').length == 0)
                    $(this).parent().append('<div class="style-for-table-warning">Please Select DDO</div>');
            } else {
                $(this).next('.style-for-table-warning').remove();
                objectId = $(this).attr("id");
//                availableAmount=$(this).closest('tr').find('input').eq(1).attr('id');
                amountText = $(this).closest('tr').find('#amount_text');
                index =$(this).closest('tr').index()
                availableAmount = 'ddoSurrenderDetailsBeanList['+index+'].amountAvailableForSurrender';
                rfcIf=$(this).closest('tr').find('input').eq(2).attr('id');
                descriptionId=$(this).closest('tr').find('input').eq(3).attr('id');

                $("#hoaModal").modal("show");

                $("#inputValidation").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $('.list-group-flushlevel').find('a').filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                    });
                }); //modal
            }
        }); //click

        $(".fromDDOField").click(function() {
            $("#fromDDOModal").modal("show");
            $("#inputValidation").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $('.list-group-flushlevel').find('a').filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                });
            }); //modal
        }); //click
    }); //ready function

    function modalOptionDepartment(obj) {
        var depts = $(obj).text().split('\n ');
        $('input[name="fromDDO"]').val(depts[1].trim());
        $("#fromDDO_text").html(depts[1].trim());
        $('input[name="deptCode"]').val(depts[1].trim());
        $('.hoaModalField').val('');
        var hoaUrl = "loadResource/ddoSurrenderloadHoaWithDDOCode" + '?key=ajax&ddocode=' + $(obj).attr("data-id");

        loadAjaxSingleRow(hoaUrl, "hoaLoadId", "hoaId", "hoa", "value", "modalOptionHoaD(this)");
    }

   /*  $("#fromDDOModal").on('hidden.bs.modal', function() {
        modalOptionDepartment(obj);
    });

    $("#hoaModal").on('hidden.bs.modal', function() {
        modalOptionHoaD(obj);
    }); */

    function loadAjaxSingleRow(url, target, id, value, label, childFunction) {
        $.ajax({
        	type : 'GET',
        	url : url,
        	dataType : "html",
        	async : false,
        	success : function(res) {
        		theResponse = JSON.parse(res);
        		var a = "";
        		for(var i = 0;i <= theResponse.length-1;i++){
        			a += "<a  class=\"list-group-item list-group-item-action hideDetailsOfHOAClass\" id=\""+theResponse[i][value]+"\" " +
        					"  data-dismiss=\"modal\" style=\"border-bottom: 1px solid rgba(0,0,0,.125);\" onclick=\""+childFunction+"\"  data-id=\""+theResponse[i][value]+"\" href=\"javascript:void(0)\"> <b>"+theResponse[i][value]+"</b> </a> ";
        		}
        		var lableCount=target+"Label";
        		$("#"+lableCount).html(theResponse.length);
        		$('#'+target).html(a);
        	},
        	error : function() {
        		alert('Error occured');
        	}
        });
    }

    function modalOptionHoaD(obj) {
        var hoa = $(obj).text().trim();
        var ddocode = $('input[name="fromDDO"]').val();
        objectId = "#" + objectId;
        $(objectId).val(hoa);
        
        var hoaUrl = "loadResource/ddoSurrenderloadSFCWithDDOCode" + '?key=ajax&ddocode=' + ddocode +'&hoa='+hoa;
      loadAjaxSetHOAValues(hoaUrl, availableAmount, amountText, rfcIf, descriptionId);
    }

    function loadAjaxSetHOAValues(url, id, target,target1,target2){
    	$.ajax({
    		type : 'GET',
    		url : url,
    		dataType : "html",
    		async : false,
    		success : function(res) 
    		{
    			theResponse = JSON.parse(res);
    			var str = theResponse[0].description.split('--');
    			$('input[name="'+id+'"]').val(theResponse[0].bud_dist);
    			$(target).html(theResponse[0].bud_dist);
    			$('#'+target1).val(str[1]);
    			$('#'+target2).val(str[0]);
    			
    		},
    		error : function() {
    			alert('Error occured');
    		}
    	});

    }
