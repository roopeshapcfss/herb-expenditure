 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %> --%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>HERB</title>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="template-assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="template-assets/js/bootstrap.min.js"></script>

    <!-- Bootstrap -->
   <link href="template-assets/css/bootstrap.min.css" rel="stylesheet"/>
 	<link href="template-assets/css/inner-page.css" rel="stylesheet"/>
	<link href="template-assets/css/starter.css" rel="stylesheet">
<link rel="apple-touch-icon" sizes="57x57" href="template-assets/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="template-assets/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="template-assets/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="template-assets/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="template-assets/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="template-assets/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="template-assets/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="template-assets/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="template-assets/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="template-assets/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="template-assets/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="template-assets/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="template-assets/img/favicon-16x16.png">
<link rel="manifest" href="template-assets/img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="template-assets/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="<%=basePath%>js/md5/md5.js"></script>
    <script>
    
    function CheckPassword(inputtxt) 
	{ 
	var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-z0-9!@#$%^&*]{7,15}$/;
	if(inputtxt.value.match(paswd)) 
	{
		return true;
	}
	else
	{ 
		$("#passwordckError").html("Password should be minimum of 8 Characters,one special character,one numeric digit.");
		return false;
	}
	}
    
    </script>
  </head>
  <body>
   <div class="container-fluid navbar-inverse">
  <nav class="navbar navbar-inverse container navbar-expand-lg navbar-light   navbar-fixed-top">
    <a class="navbar-brand" href="#"><img src="template-assets/img/herb.svg" width="80px" /></a>
 

  
</nav>
</div>
 

		<div class="container  login-block-setting">
		
			 	<div class="row element" >
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 bg-white left-block">
							<div class="login-form">
								<h2>Login</h2>
								<form:form id="loginUser" modelAttribute = "loginForm" action="loginCheck">
								<div style="color: red" align="center">${errorMessage}</div><br>
								  <p>
								  <form:input path="userName"  id="userNameck" placeholder="Username" />
								  <form:errors path="userName" />
								  <span id="userNameckError" style="color: red"></span>
								  </p>
								  <p>
								  <form:password path="password" id = "passwordck"  placeholder="Password"  onblur="return CheckPassword(this)"/>
								   <span id="passwordckError" style="color: red"></span>
								   <form:errors path="password" />
								  </p>
								  <p>
								  <form:hidden path="uniqueId" value="${captchaStr}"
									id="uniquecaptchaid" />
								  <img src="data:image/png;base64,${captchaCode}" id="captchaid">
								  </p>
								  <p>
								  <form:input cssClass="form-control input-sm mandatoryTableField" placeholder="Enter Captcha" path="passlineNormal" id="passlineNormal" maxlength="10" style="height: 50px;" />
								  <span id="passlineNormal" style="color: red"></span>
								  <form:errors path="passlineNormal" />
								  </p>
								  <p>
									 <input class="btn" type="button" value="SignIn" onclick = "return loginUser1()" >
								  </p>
								  <p>
									<a href="forgotPassword">Forgot password?</a>
									 
								  </p>
								</form:form>
							  </div>
					
					
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 right-block hidden-xs hidden-md">
						<div class="wrapper">
						  <h1 class="glitch">HERB</h1>
						</div>
							
					</div>
				</div>
			 
		</div>







   
	<script>
	$(document).ready(function() {
        // Transition effect for navbar 
			$(window).scroll(function() {
			  // checks if window is scrolled more than 500px, adds/removes solid class
			  if($(this).scrollTop() > 500) { 
				  $('.navbar').addClass('solid');
			  } else {
				  $('.navbar').removeClass('solid');
			  }
			});
	});
	
	
		/*************************************************/
		function loginUser1() {
			var enteredcaptcha = $("#passlineNormal").val();
			var gencaptcha = $("#uniquecaptchaid").val();
			if (enteredcaptcha != "") {
				if (enteredcaptcha.trim() != gencaptcha.trim()) {
					alert("Wrong Captcha");
					$("#passlineNormal").val("");
					$("#passlineNormal").focus();
					return false;
				}
			}
			if ($("#userNameck").val() != "" && $("#passwordck").val() != "" && enteredcaptcha.trim() == gencaptcha.trim()) {
				document.forms[0].password.value = hex_md5(document.forms[0].password.value);
				document.forms[0].action = "loginCheck";
				document.forms[0].method = "POST";
				document.forms[0].submit();
			}
		}
	</script>
  </body>
</html>